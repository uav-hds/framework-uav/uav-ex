#include <QApplication>
#include <QtGui>
#include <tclap/CmdLine.h>
#include "interface.h"

using namespace TCLAP;
using namespace std;

string address;

int nmcli_connect(QString id)
{
    QProcess p;
    printf("tentative connection nmcli %s\n",id.toStdString().c_str());
    p.start(QString("nmcli con up id %1").arg(id));
    p.waitForFinished();
    if(p.exitCode()!=0)
    {
        QString p_stderr = p.readAllStandardError();
        printf("%s\n",p_stderr.toStdString().c_str());
    }
    return p.exitCode();
}

void parseOptions(int argc, char** argv) {
	try {
        CmdLine cmd("Command description message", ' ', "0.1");

        ValueArg<string> addressArg("a","address","uav address",true,"192.168.6.1","string");
        cmd.add( addressArg );

        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        address= addressArg.getValue();

	} catch (ArgException &e) {// catch any exceptions
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }
}

int main(int argc, char *argv[])
{/*
    if(nmcli_connect("wlan_uav")!=0)
    {
        printf("connectez le wifi manuelement\n");
    }else
    {
        printf("connection établie\n");
    }
*/
    QApplication app(argc, argv);

    parseOptions(argc,argv);

    Interface fenetre(address);

    fenetre.show();

    return app.exec();
}


