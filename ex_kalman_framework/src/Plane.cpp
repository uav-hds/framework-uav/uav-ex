//  created:    2013/06/27
//  filename:   Plane.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant calculant l'etat de l'avion par un kalman
//
//
/*********************************************************************/


#include "Plane.h"
#include <cvmatrix.h>
#include <FrameworkManager.h>
#include <Tab.h>
#include <TabWidget.h>
#include <DataPlot1D.h>

//on utilise le namespace std, ce qui évite d'écrire par exemple std::string
using namespace std;
using namespace Kalman;
using namespace framework::core;
using namespace framework::gui;

namespace framework
{
namespace actuator
{
//constructeur, on hérite de IODevice
Plane::Plane(const FrameworkManager* parent,string name): IODevice(parent,name)
{
    //initialisation interface
    main_tab=new Tab(parent->GetTabWidget(),name);//onglet principal
    tabwidget=new TabWidget(main_tab->NewRow(),name);//conteneur d'onglets secondaires

    //matrice 4*1 de sortie
    cvmatrix_descriptor* desc=new cvmatrix_descriptor(4,1);
    desc->SetElementName(0,0,"x");//le nom servira pour la légende du graphe
    desc->SetElementName(1,0,"dx");
    desc->SetElementName(2,0,"y");
    desc->SetElementName(3,0,"dy");
    output=new cvmatrix(this,desc,floatType,name);


    //on spécifie les données à enregistrer (logs)
    AddDataToLog(output);

    //initialisation comme dans l'exemple de la librairie
    setDim(4, 1, 2, 2, 2);
	Period = 0.2;
	Gravity = 9.8;
	Bfriction = 0.35;
	Portance = 3.92;
	Mass = 1000;

    KVector<double, 1, true> x(4);

    static const double _P0[] = {100.0*100.0, 0.0, 0.0, 0.0,
								 0.0, 10.0*10.0, 0.0, 0.0,
								 0.0, 0.0, 25.0*25.0, 0.0,
								 0.0, 0.0, 0.0, 10.0*10.0};

	KMatrix<double, 1, true> P0(4, 4, _P0);

	x(1) = cos(3.058791)*2528.83891;
	x(2) = 60;
	x(3) = sin(3.058791)*2528.83891;
	x(4) = 0;
	output->SetValue(0,0,x(1));//x
    output->SetValue(1,0,x(2));//dx
    output->SetValue(2,0,x(3));//y
    output->SetValue(3,0,x(4));//dy

	init(x, P0);
}

Plane::~Plane()
{
    delete main_tab;
}

void Plane::UseDefaultPlot(void)
{
    Tab* plot_tab=new Tab(tabwidget,"Graphe");//onglet secondaire pour le graphe
        DataPlot1D *plot_x=new DataPlot1D(plot_tab->NewRow(),"x",-2500,3000);//le graphe
            plot_x->AddCurve(output->Element(0));//la courbe de la matrice Output(0,0)
        DataPlot1D *plot_y=new DataPlot1D(plot_tab->LastRowLastCol(),"y",0,500);//le graphe
            plot_y->AddCurve(output->Element(2));//la courbe de la matrice Output(0,1)
        DataPlot1D *plot_dx=new DataPlot1D(plot_tab->NewRow(),"dx",0,150);//le graphe
            plot_dx->AddCurve(output->Element(1));//la courbe de la matrice Output(0,2)
        DataPlot1D *plot_dy=new DataPlot1D(plot_tab->LastRowLastCol(),"dy",-50,50);//le graphe
            plot_dy->AddCurve(output->Element(3));//la courbe de la matrice Output(0,3)
}

//accesseur pour Output
//il est plus propre de faire une fonction accesseur plutot que de mettre la variable output publique
cvmatrix *Plane::Output(void) const
{
    return output;
}

//Update, c'est ici qu'on implémente le filtre
void Plane::Update(float theta,float r,float f)
{
    Vector z(2);
    z(1) = theta;
    z(2) = r;

    Vector u(1,f);

    step(u, z);

    Vector etat(4);
    etat=getX();

    output->SetValue(0,0,etat(1));//x
    output->SetValue(1,0,etat(2));//dx
    output->SetValue(2,0,etat(3));//y
    output->SetValue(3,0,etat(4));//dy

    //on place le temps corresponcant à la valeur du signal dans la matrice de sortie
    output->SetDataTime(GetTime());

    //ProcessUpdate est très important, il faut obligatoirement l'appeler après avoir mis à jour la matrice de sortie
    //cela permet:
    // d'enregistrer la matrice se sortie dans les logs
    // de mettre à jour automatiquement les filtres suivants
    ProcessUpdate(output);
}

//fonctions copiés de l'exemple de la librairie kalman
void Plane::makeBaseA()
{
	A(1,1) = 1.0;
	// A(1,2) = Period - Period*Period*Bfriction/Mass*x(2);
	A(1,3) = 0.0;
	A(1,4) = 0.0;

	A(2,1) = 0.0;
	// A(2,2) = 1 - 2*Period*Bfriction/Mass*x(2);
	A(2,3) = 0.0;
	A(2,4) = 0.0;

	A(3,1) = 0.0;
	// A(3,2) = Period*Period*Portance/Mass*x(2);
	A(3,3) = 1.0;
	A(3,4) = Period;

	A(4,1) = 0.0;
	// A(4,2) = 2*Period*Portance/Mass*x(2);
	A(4,3) = 0.0;
	A(4,4) = 1.0;
}

void Plane::makeA()
{
	// A(1,1) = 1.0;
	A(1,2) = Period - Period*Period*Bfriction/Mass*x(2);
	// A(1,3) = 0.0;
	// A(1,4) = 0.0;

	// A(2,1) = 0.0;
	A(2,2) = 1 - 2*Period*Bfriction/Mass*x(2);
	// A(2,3) = 0.0;
	// A(2,4) = 0.0;

	// A(3,1) = 0.0;
	A(3,2) = Period*Period*Portance/Mass*x(2);
	// A(3,3) = 1.0;
	// A(3,4) = Period;

	// A(4,1) = 0.0;
	A(4,2) = 2*Period*Portance/Mass*x(2);
	// A(4,3) = 0.0;
	// A(4,4) = 1.0;
}

void Plane::makeBaseW()
{
	W(1,1) = 0.0;
	W(1,2) = 0.0;
	W(2,1) = 1.0;
	W(2,2) = 0.0;
	W(3,1) = 0.0;
	W(3,2) = 0.0;
	W(4,1) = 0.0;
	W(4,2) = 1.0;
}

void Plane::makeBaseQ()
{
	Q(1,1) = 0.01*0.01;
	Q(1,2) = 0.01*0.01/10.0;
	Q(2,1) = 0.01*0.01/10.0;
	Q(2,2) = 0.01*0.01;
}

void Plane::makeBaseH()
{
	// H(1,1) = -x(3)/(x(1)*x(1)+x(3)*x(3));
	H(1,2) = 0.0;
	// H(1,3) = x(1)/(x(1)*x(1)+x(3)*x(3));
	H(1,4) = 0.0;

	// H(2,1) = x(1)/sqrt(x(1)*x(1)+x(3)*x(3));
	H(2,2) = 0.0;
	// H(2,3) = x(3)/sqrt(x(1)*x(1)+x(3)*x(3));
	H(2,4) = 0.0;
}

void Plane::makeH()
{
	H(1,1) = -x(3)/(x(1)*x(1)+x(3)*x(3));
	// H(1,2) = 0.0;
	H(1,3) = x(1)/(x(1)*x(1)+x(3)*x(3));
	// H(1,4) = 0.0;

	H(2,1) = x(1)/sqrt(x(1)*x(1)+x(3)*x(3));
	// H(2,2) = 0.0;
	H(2,3) = x(3)/sqrt(x(1)*x(1)+x(3)*x(3));
	// H(2,4) = 0.0;
}

void Plane::makeBaseV()
{
	V(1,1) = 1.0;
	V(2,2) = 1.0;
}

void Plane::makeBaseR()
{
	R(1,1) = 0.01*0.01;
	R(2,2) = 50*50;
}

void Plane::makeProcess()
{
	Vector x_(x.size());
	x_(1) = x(1) + x(2)*Period + (Period*Period)/2*(u(1)/Mass - Bfriction/Mass*x(2)*x(2));
	x_(2) = x(2) + (u(1)/Mass - Bfriction/Mass*x(2)*x(2))*Period;
	x_(3) = x(3) + x(4)*Period + (Period*Period)/2*(Portance/Mass*x(2)*x(2)-Gravity);
	x_(4) = x(4) + (Portance/Mass*x(2)*x(2)-Gravity)*Period;
	x.swap(x_);
}

void Plane::makeMeasure()
{
	z(1)=atan2(x(3), x(1));
	z(2)=sqrt(x(1)*x(1)+x(3)*x(3));
}
}//end namespace actuator
}//end namespace framework
