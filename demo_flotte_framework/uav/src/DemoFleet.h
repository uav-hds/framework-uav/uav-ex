//  created:    2015/11/05
//  filename:   DemoFleet.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo fleet
//
//
/*********************************************************************/

#ifndef DEMOFLEET_H
#define DEMOFLEET_H

#include <UavStateMachine.h>

namespace framework
{
    namespace core {
        class FrameworkManager;
        class Socket;
        class AhrsData;
    }
    namespace filter {
        class TrajectoryGenerator2DCircle;
    }
    namespace gui {
        class DoubleSpinBox;
    }
}


class DemoFleet : public framework::meta::UavStateMachine {
    public:
        DemoFleet(framework::meta::Uav* uav,std::string broadcast,uint16_t ds3port);
        ~DemoFleet();

    private:
        enum class BehaviourMode_t {
            Default,
            PositionHold1,
            Circle1,
            PositionHold2,
            PositionHold3,
            Circle2,
            PositionHold4,
        };

//        BehaviourMode_t orientation_state;
        BehaviourMode_t behaviourMode;
        bool vrpnLost;

        void VrpnPositionHold(void);//flight mode
        void StartCircle(void);
        void StopCircle(void);
        void ExtraTakeOff(void);
        void ExtraSecurityCheck(void);
        void ExtraCheckJoystick(void);
        const framework::core::AhrsData *GetOrientation(void) const;
        void AltitudeValues(float &z,float &dz) const;
        void PositionValues(framework::core::Vector2D &pos_error,framework::core::Vector2D &vel_error,float &yaw_ref);
        const framework::core::AhrsData *GetReferenceOrientation(void);
        void SignalEvent(Event_t event);
        void CheckMessages(void);

        framework::filter::Pid *u_x, *u_y;

        framework::core::Vector2D pos_hold;
        float yaw_hold;
        framework::core::Socket *message;
        framework::core::Time posWait;

        framework::filter::TrajectoryGenerator2DCircle *circle;
        framework::gui::DoubleSpinBox *xCircleCenter,*yCircleCenter,*yDisplacement;
        framework::core::AhrsData *customReferenceOrientation,*customOrientation;
};

#endif // DEMOFLEET_H
