#ifndef STRUCTDEF_H
#define STRUCTDEF_H

#ifdef __XENO__
#include <native/timer.h>
#endif

typedef struct gains_all{
        float kp_roll;
        float ki_roll;
        float kd_roll;
        float kp_pitch;
        float ki_pitch;
        float kd_pitch;
        float kd_yaw;
        float kp_yaw;
        float ki_yaw;
        float k1;
        float k2;
        float k3;
        float k4;
        float kd_of_x;
        float ki_of_x;
        float kd_of_y;
        float ki_of_y;
        float fc_alt; // cutoff frequency for altitude lowpass filter
        float fc_of; // cutoff frequency for optical flow lowpass filter
        float offset_roll;
        float offset_pitch;
        uint16 int_max_angles;
        uint16 int_max_fo;
        uint8 deb_roll; // target roll angle when input pad hits the stop
        uint8 deb_pitch; // target pitch angle when input pad hits the stop
        uint8 deb_yaw; // target yaw angle when input pad hits the stop
        uint8 deb_gaz; // target gas value when input pad hits the stop
        uint8 log_divider;
        uint8 pas_decollage;
        uint8 seuil_vitesse;
        uint8 vitesse_atterrissage;
        uint8 pwm_low;
        uint8 pwm_high;
        uint8 deb_joy_of;
        uint8 dummy8;
        float alt_dec;
        float vel_max_dec;
        float acc_dec;
        float gain;
        float exposure;
        float bright;
        float contrast;
        float hue;
        float sharpness;
        uint16 autogain;
        uint16 awb;
        float quality_level;
        float min_distance;
} gains_all;

typedef struct gains_chru{
    float ekf_mag_variance;
    float ekf_accel_variance;
    float ekf_process_variance;
} gains_chru;

typedef struct control{
    int16 roll;
    int16 pitch;
    int16 yaw;
    uint16 gaz;
} control;

typedef struct control_trim{
    signed char trim_roll;
    signed char trim_pitch;
    signed char trim_gaz;
} control_trim;

typedef struct imu_datas{
    float roll;
    float pitch;
    float yaw;
    float droll;
    float dpitch;
    float dyaw;
} imu_datas;

typedef struct angles{
    float roll;
    float pitch;
    float yaw;
} angles;

typedef struct moteurs_16bits{
    uint16 moteur[8];
} moteurs_16bits;

typedef struct moteurs_8bits{
    uint8 moteur[4];
} moteurs_8bits;

typedef struct altitude{
        float z;
        float vz;
} altitude;

typedef struct optical_flow{
        float x;
        float y;
        float fps;
} optical_flow;

#ifdef __XENO__
typedef struct optical_flow_time{
        struct optical_flow of;
        RTIME time;
} optical_flow_time;
#endif

#endif // STRUCTDEF_H
