//  created:    2012/11/27
//  filename:   Roll.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet simulateur
//
//
/*********************************************************************/

#ifndef ROLL_H
#define ROLL_H

#include <Model.h>
#include <stdint.h>

namespace framework
{
    namespace gui
    {
        class DoubleSpinBox;
    }
    namespace actuator
    {
        class SimuBldc;
    }
}

namespace framework
{
namespace simulator
{
    class Simulator;

    class Roll : public Model
    {
        public:
            Roll(Simulator* simu,std::string name);
            ~Roll();

        private:
            void CalcModel();
            gui::DoubleSpinBox *arm_length,*k_mot,*j;
            actuator::SimuBldc *motors;
    };
}// end namespace simulator
}// end namespace framework
#endif // ROLL_H
