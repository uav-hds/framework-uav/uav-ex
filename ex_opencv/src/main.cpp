#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <dspcv_gpp.h>


#define LEVEL_PYR 2
#define NB_POINTS_TO_TRACK 64
#define MAX_POINTS_TO_TRACK 256

#define ITERATIONS 100


void tracking_cv(const char* imageA,const char* imageB,double quality_level,double min_distance);
void tracking_dsp(const char* imageA,const char* imageB,double quality_level,double min_distance,CvRect roi);
void sobel_dsp(const char* imageA);
void sobel_cv(const char* imageA);
void jpg_dsp(const char* img_src);

int main(int argc, char ** argv)
{
    if (argc != 2)
    {
        printf ("Usage : %s <absolute path of DSP executable>\n", argv [0]) ;
    }
    else
    {

        initDsp(argv [1]) ;
        
        sobel_cv("imagesA.bmp");
        sobel_dsp("imagesA.bmp");

        tracking_cv("img0.bmp","img1.bmp",0.03,5);
        tracking_dsp("img0.bmp","img1.bmp",0.03,5,cvRect(0,0,320,240));
        //tracking_dsp("img0.bmp","img1.bmp",0.03,5,cvRect(160,120,160,120));
        
        jpg_dsp("jpg.bmp");
        
        //  Perform cleanup operation.
        closeDsp() ;

    }

}

void jpg_dsp(const char* img_src) {
  printf("Jpg DSP\n");

  unsigned int compressed_size;
  struct timeval tvs, tve, tvd;

  IplImage* cimg=cvLoadImage(img_src);
  IplImage* gimg=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);
  dspCvtColor(cimg,gimg,CV_BGR2GRAY);
  char* jpg_buf=shMemAlloc(gimg->imageSize);
  
  gettimeofday(&tvs, NULL);    
  compressed_size=dspConvertToJpeg(gimg,jpg_buf,gimg->imageSize);
  //if(!cvSaveImage("toto.jpg",gimg)) printf("Could not save.\n");//arm
  gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
  //printf("%i iterations dspCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);
  printf("dspConvertToJpeg, temps: %d.%06d\n",(int)tvd.tv_sec, (int)tvd.tv_usec);
  
  cvReleaseImage(&cimg);
  cvReleaseImage(&gimg);
    
  FILE* fOutFile = fopen ("jpg.jpg", "wb");;
  fwrite(jpg_buf,1,compressed_size,fOutFile);
  fclose(fOutFile);
  shMemFree(jpg_buf);
}


void sobel_dsp(const char* imageA)
{
    struct timeval tvs, tve, tvd;

    printf("Sobel DSP\n");

    IplImage* cimg=cvLoadImage(imageA);
    IplImage* gimg=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);
    IplImage* sobel=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) dspCvtColor(cimg,gimg,CV_BGR2GRAY);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) dspSobel(gimg,sobel,1,1);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspSobel, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    if(!cvSaveImage("gris_dsp.bmp",gimg)) printf("Could not save.\n");
    if(!cvSaveImage("sobel_dsp.bmp",sobel)) printf("Could not save.\n");

    printf("\n");

    cvReleaseImage(&gimg);
    cvReleaseImage(&sobel);
    cvReleaseImage(&cimg);
}

void sobel_cv(const char* imageA)
{
    struct timeval tvs, tve, tvd;

    printf("Sobel ARM\n");

    IplImage* cimg=cvLoadImage(imageA);
    IplImage* gimg=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);
    IplImage* sobel=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) cvCvtColor(cimg,gimg,CV_BGR2GRAY);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) cvSobel(gimg,sobel,1,1,3);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvSobel, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    if(!cvSaveImage("gris_arm.bmp",gimg)) printf("Could not save.\n");
    if(!cvSaveImage("sobel_arm.bmp",sobel)) printf("Could not save.\n");

    printf("\n");

    cvReleaseImage(&gimg);
    cvReleaseImage(&sobel);
    cvReleaseImage(&cimg);
}

void tracking_dsp(const char* imageA,const char* imageB,double quality_level,double min_distance,CvRect roi)
{

    struct timeval tvs, tve, tvd;
    unsigned int i;
    CvPoint *pointsA =0;
    CvPoint *pointsB = 0;
    int deplx=0,deply=0,nb_depl=0;
    char *optical_flow_found_feature=0;
    unsigned int count=NB_POINTS_TO_TRACK,nb_perdu=0;
    unsigned int *optical_flow_feature_error=0;
    bool use_roi;

    CvSize optical_flow_window = cvSize(3,3);
    CvTermCriteria optical_flow_termination_criteria = cvTermCriteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS,20, .3 );

    IplImage* cimg0=cvLoadImage(imageA);
    IplImage* cimg1=cvLoadImage(imageB);

    if(roi.x==0 && roi.y==0 && roi.width==cimg0->width && roi.height==cimg0->height)
    {
        use_roi=false;
        printf("Tracking DSP\n");
    }
    else
    {
        use_roi=true;
        printf("Tracking DSP, with ROI\n");
    }


    IplImage* gimg0=cvCreateImage(cvSize(cimg0->width,cimg0->height),IPL_DEPTH_8U,1);
    IplImage* gimg1=cvCreateImage(cvSize(cimg0->width,cimg0->height),IPL_DEPTH_8U,1);
    IplImage* pyr0=cvCreateImage(cvSize(cimg0->width/2,cimg0->height),IPL_DEPTH_8U,1);
    IplImage* pyr1=cvCreateImage(cvSize(cimg0->width/2,cimg0->height),IPL_DEPTH_8U,1);
    pointsA=(CvPoint *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(CvPoint));
    pointsB=(CvPoint *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(CvPoint));
    optical_flow_found_feature=(char *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(char));
    optical_flow_feature_error=(unsigned int *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(unsigned int));

    gettimeofday(&tvs, NULL);
    for(int j=0;j<ITERATIONS;j++)
    {
        dspCvtColor(cimg0,gimg0,CV_BGR2GRAY);
        dspCvtColor(cimg1,gimg1,CV_BGR2GRAY);
    }
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    if(use_roi==true)
    {
        cvSetImageROI(gimg0,roi);
        cvSetImageROI(gimg1,roi);
    }

    gettimeofday(&tvs, NULL);
    //select good features
    for(int j=0;j<ITERATIONS;j++) dspGoodFeaturesToTrack(gimg0,pointsA,&count,quality_level,min_distance);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspGoodFeaturesToTrack, temps: %d.%06d\n",ITERATIONS, (int)tvd.tv_sec, (int)tvd.tv_usec);

    gettimeofday(&tvs, NULL);
    for(int j=0;j<ITERATIONS;j++)
    {
        //premiere pyramide
        dspPyrDown(gimg0,pyr0,LEVEL_PYR);
        //seconde pyramide
        dspPyrDown(gimg1,pyr1,LEVEL_PYR);
    }
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspPyrDown, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    gettimeofday(&tvs, NULL);
    //lk
    for(int j=0;j<ITERATIONS;j++) dspCalcOpticalFlowPyrLK(gimg0,gimg1,pyr0,pyr1,pointsA,pointsB,count,optical_flow_window,LEVEL_PYR,optical_flow_found_feature,optical_flow_feature_error,optical_flow_termination_criteria,0) ;
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspCalcOpticalFlowPyrLK, temps: %d.%06d\n",ITERATIONS, (int)tvd.tv_sec, (int)tvd.tv_usec);

    //dessine features
    for(i=0;i<count;i++)
    {
        //cvCircle(gimg0, pointsA[i], 0, CV_RGB(255, 255, 255));
        if(optical_flow_found_feature[i]!=0)
        {
            CvPoint centre;
            centre.x=(int)(pointsA[i].x+pointsB[i].x/256);
            centre.y=(int)(pointsA[i].y+pointsB[i].y/256);
            deplx+=pointsB[i].x;
            deply+=pointsB[i].y;
            nb_depl++;

            //Sous openCv on dessine un point en traçant un cercle de diamètre 0
            cvCircle(gimg1, centre, 0, CV_RGB(255, 255, 255));

        }
        else{
         nb_perdu++;
        }
    }
    printf("total perdu %i\n",nb_perdu);
    nb_depl=nb_depl<<8;
    printf("deplacement moyen: %f %f\n",(float)deplx/nb_depl,(float)deply/nb_depl);

    for(i=0;i<count;i++)
    {
       // printf("%i %i %i %i %i\n",i,pointsA[i].x,pointsA[i].y,pointsB[i].x,pointsB[i].y);
        cvCircle(gimg0, pointsA[i], 0, CV_RGB(255, 255, 255));
    }

    if(use_roi==true)
    {
        cvResetImageROI(gimg0);
        cvResetImageROI(gimg1);
        if(!cvSaveImage("tracking_roi_dsp1.bmp",gimg0)) printf("Could not save.\n");
        if(!cvSaveImage("tracking_roi_dsp2.bmp",gimg1)) printf("Could not save.\n");
    }
    else
    {
        if(!cvSaveImage("tracking_dsp1.bmp",gimg0)) printf("Could not save.\n");
        if(!cvSaveImage("tracking_dsp2.bmp",gimg1)) printf("Could not save.\n");
    }


    printf("\n");

    cvReleaseImage(&gimg0);
    cvReleaseImage(&gimg1);
    cvReleaseImage(&pyr0);
    cvReleaseImage(&pyr1);
    cvReleaseImage(&cimg0);
    cvReleaseImage(&cimg1);
    cvFree(&pointsA);
    cvFree(&pointsB);
    cvFree(&optical_flow_found_feature);
    cvFree(&optical_flow_feature_error);

}


void tracking_cv(const char* imageA,const char* imageB,double quality_level,double min_distance)
{

    int count=NB_POINTS_TO_TRACK;
    CvSize optical_flow_window = cvSize(3,3);
    CvTermCriteria optical_flow_termination_criteria = cvTermCriteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, .3 );
    struct timeval tvs, tve, tvd;
    unsigned int i;
    float deplx=0,deply=0;
    int nb_depl=0;
    int nb_perdu=0;

    printf("Tracking CV\n");

    IplImage* cimg0=cvLoadImage(imageA);
    IplImage* cimg1=cvLoadImage(imageB);

    IplImage* gimg0=cvCreateImage(cvSize(cimg0->width,cimg0->height),IPL_DEPTH_8U,1);
    IplImage* gimg1=cvCreateImage(cvSize(cimg1->width,cimg1->height),IPL_DEPTH_8U,1);
    IplImage* pyr0=cvCreateImage( cvSize(cimg0->width,cimg0->height), IPL_DEPTH_8U, 1 );
    IplImage* pyr1=cvCreateImage( cvSize(cimg1->width,cimg1->height), IPL_DEPTH_8U, 1 );

     //déclaration et allocation des images temporarires
    IplImage *eig_image, *temp_image;
    eig_image = cvCreateImage(cvGetSize(cimg0), IPL_DEPTH_32F, 1);
    temp_image = cvCloneImage(eig_image);

    //déclaration et allocation du vecteur qui contiendra les points d'interêt
    CvPoint2D32f *pointsA = new CvPoint2D32f[MAX_POINTS_TO_TRACK];
    CvPoint2D32f pointsB[MAX_POINTS_TO_TRACK];

    char optical_flow_found_feature[MAX_POINTS_TO_TRACK];
    float optical_flow_feature_error[MAX_POINTS_TO_TRACK];

    gettimeofday(&tvs, NULL);
    for(int j=0;j<ITERATIONS;j++)
    {
        cvCvtColor(cimg0,gimg0,CV_BGR2GRAY);
        cvCvtColor(cimg1,gimg1,CV_BGR2GRAY);
    }
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    //select good features
    for(int j=0;j<ITERATIONS;j++) cvGoodFeaturesToTrack(gimg0,eig_image, temp_image,pointsA,&count,quality_level,min_distance);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvGoodFeaturesToTrack, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

     for(i=0;i<count;i++)
    {
       // printf("%i %i %i %i %i\n",i,pointsA[i].x,pointsA[i].y,pointsB[i].x,pointsB[i].y);
        CvPoint centre = cvPointFrom32f(pointsA[i]);
        cvCircle(gimg0, centre, 0, CV_RGB(255, 255, 255));
    }

    if(!cvSaveImage("tracking_cv1.bmp",gimg0)) printf("Could not save.\n");

    gettimeofday(&tvs, NULL);
    //lk
    for(int j=0;j<ITERATIONS;j++) cvCalcOpticalFlowPyrLK(gimg0,gimg1,pyr0,pyr1,pointsA,pointsB,count,optical_flow_window,LEVEL_PYR,optical_flow_found_feature,optical_flow_feature_error,optical_flow_termination_criteria,0) ;
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvCalcOpticalFlowPyrLK, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);


    //dessine features
    for(i=0;i<count;i++)
    {
        //cvCircle(gimg0, pointsA[i], 0, CV_RGB(255, 255, 255));
        if(optical_flow_found_feature[i]!=0)
        {
            CvPoint centre = cvPointFrom32f(pointsB[i]);
            deplx+=pointsB[i].x-pointsA[i].x;
            deply+=pointsB[i].y-pointsA[i].y;
            nb_depl++;

            //Sous openCv on dessine un point en traçant un cercle de diamètre 0
            cvCircle(gimg1, centre, 0, CV_RGB(255, 255, 255));

        }
        else{
         nb_perdu++;
        }
    }
    printf("total perdu %i\n",nb_perdu);
    printf("deplacement moyen: %f %f\n",deplx/nb_depl,deply/nb_depl);


    if(!cvSaveImage("tracking_cv2.bmp",gimg1)) printf("Could not save.\n");

    printf("\n");

    cvReleaseImage(&cimg0);
    cvReleaseImage(&cimg1);
    cvReleaseImage(&gimg0);
    cvReleaseImage(&gimg1);
    cvReleaseImage(&pyr0);
    cvReleaseImage(&pyr1);
    cvReleaseImage(&eig_image);
    cvReleaseImage(&temp_image);
    delete pointsA;

}


