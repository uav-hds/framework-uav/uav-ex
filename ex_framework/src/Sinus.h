//  created:    2013/06/26
//  filename:   Sinus.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant la generation d'un sinus
//
//
/*********************************************************************/

#ifndef SINUS_H
#define SINUS_H

//on met les include nécessaires, ceux dont dérive la classe Sinus
#include <IODevice.h>
#include <Thread.h>

//les autres classe peuvent simplement être déclarées (forward declaration)
namespace framework
{
    namespace core
    {
        class FrameworkManager;
        class cvmatrix;
    }
    namespace gui
    {
        class Tab;
        class TabWidget;
        class GridLayout;
        class DoubleSpinBox;
        class SpinBox;
        class DataPlot1D;
    }
}

//Sinus est une classe générant un signal sinusoidal
//dans cette exemple, elle simule une donnée capteur
//on étend donc le namespace sensor
//elle dérive de:
// IODevice: car elle possède une sortie (le signal sinusoidal)
// Thread: cette classe est un thread, car elle simule un capteur
namespace framework
{
namespace sensor
{
    class Sinus : public core::IODevice, public core::Thread
    {
        public://fonctions publiques visibles par tous
            /*!
            * \brief Constructeur
            *
            * Construit un générateur de Sinus
            *
            * \param parent le FrameworkManager à utiliser
            * \param name nom de l'objet
            * \param priority priorité du Thread, par défaut 50 (1:mini, 99:maxi)
            */
            Sinus(const core::FrameworkManager* parent,std::string name,int priority=50);

            /*!
            * \brief Déstructeur
            */
            ~Sinus();

            /*!
            * \brief Matrice de sortie
            *
            * permet d'accéder à la matrice de sortie;
            * par exemple pour récupérer la valeur du signal (voir aussi la fonction Value),
            * ou pour afficher un graphe (voir aussi la fonction UseDefaultPlot)
            *
            * \return un pointeur vers la matrice de sortie
            */
            core::cvmatrix *Output(void) const;

            /*!
            * \brief Valeur
            *
            * cette fonction est équivalente à Output()->Value(0,0)
            *
            * \return valeur actuelle du signal
            */
            float Value(void) const;

            /*!
            * \brief Utilise le graphe par défaut
            *
            * cette fonction placera un graphe du signal dans un onglet Graphe
            *
            */
            void UseDefaultPlot(void);

            /*!
            * \brief SetupLayout
            *
            * cette fonction permet d'ajouter d'autres Widget dans l'onglet du Sinus
            * par exmple si on ajoute un filtre, ses réglages appraitront à la suite des
            * réglages du Sinus
            *
            * \return le GridLayout
            */
            gui::GridLayout* SetupLayout(void) const;

            /*!
            * \brief Plot
            *
            * cette fonction permet d'ajouter d'autres courbes dans le graphe par défaut
            * par exmple si on ajoute un filtre, on pourra avoir sur le même graphe la valeur
            * du signal et sa valeur filtrée
            *
            * \return le DataPlot1D
            */
            gui::DataPlot1D* Plot(void) const;

        private://fonctions et variables privées, utilisables uniquement par cette classe

            //fonction UpdateFrom de la classe IODevice
            //Sinus s'apparente à un capteur, cette fonction n'est donc pas implémentée ici
            //en effet Sinus n'a pas d'entrée, mais seulement une sortie
            void UpdateFrom(const core::io_data *data){};

            //fonction Run de la classe Thread, c'est la boucle de traitement
            void Run(void);

            //variables:
            core::cvmatrix *output;//matrice de sortie
            gui::Tab* main_tab;//onglet principal du Sinus
            gui::TabWidget* tabwidget;//conteneur d'onglets secondaires
            gui::DataPlot1D* plot;//graphe
            gui::DoubleSpinBox *frequency,*amplitude,*offset;//réglages du signal
            gui::SpinBox *period;//réglafe du Thread
            gui::GridLayout* setup_layout;//GridLayout pour ajouter des filtres par exemple
    };
}// end namespace sensor
}// end namespace framework
#endif // SINUS_H
