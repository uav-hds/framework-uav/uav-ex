//  created:    2013/06/26
//  filename:   Sinus.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant la generation d'un sinus
//
//
/*********************************************************************/

//on inclut le header de la classe
#include "Sinus.h"
//on inclut les headers des classes seulement déclarées dans Sinus.h (forward declaration)
#include <FrameworkManager.h>
#include <TabWidget.h>
#include <Tab.h>
#include <GridLayout.h>
#include <GroupBox.h>
#include <DoubleSpinBox.h>
#include <SpinBox.h>
#include <DataPlot1D.h>
#include <cvmatrix.h>
#include <math.h>

//définition du nombre Pi
#define PI ((float)3.14159265358979323846)

//on utilise le namespace std, ce qui évite d'écrire par exemple std::string
using namespace std;
using namespace framework::core;
using namespace framework::gui;

namespace framework
{
namespace sensor
{
//constructeur, on hérite de IODevice et Thread
Sinus::Sinus(const FrameworkManager* parent,string name,int priority) : IODevice(parent,name),Thread(parent,name,priority)
{
    //initialisation de quelques variables
    plot=NULL;

    //matrice 1*1 de sortie
    //Sinus dérive de Thread et IODevice, tous les deux dérivant de Object
    //on explicite que l'Object parent de la cvmatrix et l'IODevice du Sinus
    cvmatrix_descriptor* desc=new cvmatrix_descriptor(1,1);
    desc->SetElementName(0,0,"value");//le nom servira pour la légende du graphe et les logs
    output=new cvmatrix((IODevice*)this,desc,floatType,name);
    output->SetValue(0,0,0);

    //on spécifie les données à enregistrer (logs)
    AddDataToLog(output);

    //initialisation interface
    main_tab=new Tab(parent->GetTabWidget(),name);//onglet principal

    tabwidget=new TabWidget(main_tab->NewRow(),name);//conteneur d'onglets secondaires

	Tab* reglages_tab=new Tab(tabwidget,"Reglages");//onglet secondaire des réglages
    GroupBox* sinus_groupbox=new GroupBox(reglages_tab->NewRow(),name);//conteneur de boites pour les réglages du signal
        frequency=new DoubleSpinBox(sinus_groupbox->NewRow(),"frequence:"," Hz",0,100,1);//fréquence du signal
        amplitude=new DoubleSpinBox(sinus_groupbox->LastRowLastCol(),"amplitude:",0,10,1);//amplitude du signal
        offset=new DoubleSpinBox(sinus_groupbox->LastRowLastCol(),"offset:",0,10,1);//offset du signal
    period=new SpinBox(reglages_tab->NewRow(),"period thread:"," ms",1,1000,1);//période du Thread

    setup_layout=new GridLayout(reglages_tab->NewRow(),"setup");//layout sui servira à placer des filtres par exemple
}

//destructeur
Sinus::~Sinus()
{
    //on demande l'arret du thread
    SafeStop();

    //on attend que le Thread soit fini, c'est à dire que l'on quitte la fonction Run
    Join();

    //l'ensemble des objet créés et ayant pour parent cette classe seront automatiquement détruits
    //main_tab has the FrameworkManager as parent; it will be destroyed when FrameworkManager is destroyed
    //it is cleaner to delete it manually, because main_tab is unnecessary when Sinus is deleted
    delete main_tab;
}

void Sinus::UseDefaultPlot(void)
{
    Tab* plot_tab=new Tab(tabwidget,"Graphe");//onglet secondaire pour le graphe
        plot=new DataPlot1D(plot_tab->NewRow(),"Sinus",-10,10);//le graphe
            plot->AddCurve(output->Element(0),DataPlot::Red);//la courbe de la matrice Output(0,0)
}

//accesseur pour setup_layout
//il est plus propre de faire une fonction accesseur plutot que de mettre la variable setup_layout publique
GridLayout* Sinus::SetupLayout(void) const
{
    return setup_layout;
}

DataPlot1D* Sinus::Plot(void) const
{
    //attention, si l'on a pas appellé UseDefaultPlot avant, plot est inutilisable
    if(plot==NULL) Printf("Sinus::Plot, attention plot non défini, voir UseDefaultPlot\n");
    return plot;
}

//accesseur pour Output
//il est plus propre de faire une fonction accesseur plutot que de mettre la variable output publique
cvmatrix *Sinus::Output(void) const
{
    return output;
}

float Sinus::Value(void) const
{
   return output->Value(0,0);
}

//la boucle du Thread
void Sinus::Run(void)
{
    //on fixe la période du Thread
    SetPeriodMS(period->Value());

    //permet d'avertir si l'on fait des changements du mode temps réél au mode normal
    WarnUponSwitches(true);

    //on prend le temps de départ
    Time init_time=GetTime();

    //boucle tant que l'on a pas appele SafeStop
    while(!ToBeStopped())
    {
        //on synchronise avec la période
        WaitPeriod();

        //si la valeur de la box a changé, on fixe la période du Thread
        if(period->ValueChanged()==true) SetPeriodMS(period->Value());

        //on prend le temps actuel
        Time time=GetTime();
        //calcul de la valeur du sinus on fonction de tous les paramétres
        float value=offset->Value()+amplitude->Value()*sinf(2*PI*frequency->Value()*(time-init_time)/1000000000.);//les temps sont en nanosecondes

        //on place la valeur du signal dans la matrice de sortie
        output->SetValue(0,0,value);
        //on place le temps corresponcant à la valeur du signal dans la matrice de sortie
        output->SetDataTime(time);

        //ProcessUpdate est très important, il faut obligatoirement l'appeler après avoir mis à jour la matrice de sortie
        //cela permet:
        // d'enregistrer la matrice se sortie dans les logs
        // de mettre à jour automatiquement les filtres suivants (dans notre cas le passe bas et le moyenneur)
        ProcessUpdate(output);
    }
}
}// end namespace sensor
}// end namespace framework
