//  created:    2012/11/27
//  filename:   Roll.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet simulateur
//
//
/*********************************************************************/

#include "Roll.h"
#include <Simulator.h>
#include <SimuBldc.h>
#include <TabWidget.h>
#include <Tab.h>
#include <DoubleSpinBox.h>
#include <GroupBox.h>
#include <math.h>

#define PI ((float)3.14159265358979323846)

using namespace std;
using namespace framework::gui;
using namespace framework::actuator;

namespace framework
{
namespace simulator
{
Roll::Roll(Simulator* simu,std::string name): Model(simu,name)
{
    Tab *sensor_tab=new Tab(GetTabWidget(),"model");
        arm_length=new DoubleSpinBox(sensor_tab->NewRow(),"arm length (m):",0,2,0.1);
        k_mot=new DoubleSpinBox(sensor_tab->NewRow(),"k_mot",0,1,0.001,3);// vitesse rotation² (unité arbitraire) -> force (N)

        j=new DoubleSpinBox(sensor_tab->NewRow(),"j:",0.001,100,0.001,3); //moment d'inertie d'un axe (N.m.s²/rad)

	motors=new SimuBldc(this,name,2,0);
}

Roll::~Roll()
{
}

void Roll::CalcModel()
{
	float l_speed,r_speed,u_roll;
    float motor_speed[2];

    motors->GetSpeeds(motor_speed);

	l_speed=motor_speed[0];
	r_speed=motor_speed[1];

    //calcul du modele
	u_roll=arm_length->Value()*k_mot->Value()*(l_speed*l_speed-r_speed*r_speed)*sqrtf(2)/2;


	state[0].W.x=(dT()/j->Value())*u_roll+state[-1].W.x;

	// compute quaternion from W
    // Quaternion derivative: dQ = 0.5*(Q*Qw)
    core::Quaternion dQ=state[-1].Quat.GetDerivative(state[0].W);

    // Quaternion integration
    state[0].Quat = state[-1].Quat +dQ*dT();
    state[0].Quat.Normalize();
}
}// end namespace simulator
}// end namespace framework
