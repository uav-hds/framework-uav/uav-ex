//  created:    2013/06/26
//  filename:   Loop.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//
//
/*********************************************************************/

#ifndef LOOP_H
#define LOOP_H

//on met les include nécessaires, ceux dont dérive la classe Loop
#include <Thread.h>

//les autres classe peuvent simplement être déclarées (forward declaration)
namespace framework
{
    namespace core
    {
       class FrameworkManager;
    }
    namespace gui
    {
        class PushButton;
        class SpinBox;
    }
    namespace sensor
    {
        class Sinus;
    }
    namespace filter
    {
        class LowPassFilter;
        class ButterworthLowPass;
        class Moyenneur;
    }
}


//Loop est la classe principale de cet example
//elle permet de créer l'interface et tous les objets
//elle dérive de Thread
class Loop : public framework::core::Thread
{
    public://fonctions publiques visibles par tous
        /*!
        * \brief Constructeur
        *
        * Construit la boucle principale
        *
        * \param parent le FrameworkManager à utiliser
        * \param name nom de l'objet
        * \param priority priorité du Thread, par défaut 51 (1:mini, 99:maxi)
        */
        Loop(framework::core::FrameworkManager* parent,std::string name,int priority=51);

        /*!
        * \brief Déstructeur
        */
        ~Loop();

    private://fonctions et variables privées, utilisables uniquement par cette classe

        //fonction Run de la classe Thread, c'est la boucle de traitement
        void Run(void);

        //variables:
        framework::sensor::Sinus *sinus;//le sinus
        framework::filter::LowPassFilter *pbas;//le filtre passe bas
        framework::filter::ButterworthLowPass *pbas_butt;//le filtre passe bas
        framework::filter::Moyenneur *moyenne;//la moyenne du signal brut
        framework::filter::Moyenneur *moyenne_pbas;//la moyenne du signal filtré par le passe bas
        framework::gui::PushButton *button_start_log,*button_stop_log,*button_kill;//les boutons
        framework::gui::SpinBox *period;//période du Thread
};

#endif // LOOP_H
