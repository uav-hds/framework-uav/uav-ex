/*!
 * \file X2Multiplex.h
 * \brief Class defining X2 multiplexing
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/04/15
 * \version 4.0
 */

#ifndef X2MULTIPLEX_H
#define X2MULTIPLEX_H

#include <UavMultiplex.h>

namespace framework
{
    namespace core
    {
        class cvmatrix;
        class io_data;
    }
    namespace gui
    {
        class DataPlot1D;
    }
}

namespace framework
{
namespace filter
{
    /*! \class X2Multiplex
    *
    * \brief Class defining X4 and X8 multiplexing
    *
    */
    class X2Multiplex : public UavMultiplex
    {
        public:
            /*!
            * \brief Constructor
            *
            * Construct a X2 multiplexing
            *
            * \param parent parent
            * \param name name
            */
            X2Multiplex(const core::FrameworkManager* parent,std::string name);

            /*!
            * \brief Destructor
            *
            */
            ~X2Multiplex();

            /*!
            * \brief Use default plot
            *
            * Plot the output values.
            *
            */
            void UseDefaultPlot(void);

            /*!
            * \brief Motors count
            *
            * Reimplemented from UavMultiplex.
            *
            * \return motors count
            */
            uint8_t MotorsCount(void) const;

        private:
            /*!
            * \brief Update using provided datas
            *
            * Reimplemented from IODevice.
            *
            * \param data data from the parent to process
            */
            void UpdateFrom(const core::io_data *data);

            float Set(float u);
            core::cvmatrix* output;
            gui::DataPlot1D *plot_left,*plot_right;
    };
} // end namespace filter
} // end namespace framework
#endif // X2MULTIPLEX_H
