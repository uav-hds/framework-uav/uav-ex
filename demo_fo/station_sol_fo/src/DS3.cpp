#include "DS3.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define L2CAP_PSM_HIDP_CTRL 0x11
#define L2CAP_PSM_HIDP_INTR 0x13

#define HIDP_TRANS_GET_REPORT    0x40
#define HIDP_TRANS_SET_REPORT    0x50
#define HIDP_DATA_RTYPE_INPUT    0x01
#define HIDP_DATA_RTYPE_OUTPUT   0x02
#define HIDP_DATA_RTYPE_FEATURE  0x03

#define IR0 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x00, 0x41
#define IR1 0x40, 0x00

#define BIT1 2

#define SIXAXIS 1
#define DS_3 2

#define DEAD_ZONE 10

bdaddr_t bdaddr_any={{0,0,0,0,0,0}};

DS3::DS3()
{
    devs = NULL;
    next_devindex = 0;
    force_sixaxis = 0;
    force_ds3 = 0;
    bdaddr_t ba;/*
    struct motion_dev *dev = connect_device(&ba);
    dev->index = next_devindex++;
    dev->next = devs;
    devs = dev;
    setup_device(dev);
    //usb_scan();
*/
    csk = l2cap_listen(&bdaddr_any, L2CAP_PSM_HIDP_CTRL);
    isk = l2cap_listen(&bdaddr_any, L2CAP_PSM_HIDP_INTR);
}

DS3::~DS3()
{
    is_running=false;
    if(wait(10000)==false)
    {
        printf("joy terminate\n");
        terminate();
    }

    for ( struct motion_dev *dev=devs; dev; dev=dev->next )
    {
        close(dev->csk); close(dev->isk);
    }
    free(devs);
}


void DS3::run()
{
    //int csk = l2cap_listen(BDADDR_ANY, L2CAP_PSM_HIDP_CTRL);
    //int isk = l2cap_listen(BDADDR_ANY, L2CAP_PSM_HIDP_INTR);
    struct timeval timeout;

    if ( csk>=0 && isk>=0 )
        emit_log("Waiting for Bluetooth connections.");
    else
        emit_log( "Unable to listen on HID PSMs.");

    is_running=true;

    while (is_running==true )
    {
      fd_set fds; FD_ZERO(&fds);
      int fdmax = 0;
      if ( csk >= 0 ) FD_SET(csk, &fds);
      if ( isk >= 0 ) FD_SET(isk, &fds);
      if ( csk > fdmax ) fdmax = csk;
      if ( isk > fdmax ) fdmax = isk;

      for ( struct motion_dev *dev=devs; dev; dev=dev->next ) {
        FD_SET(dev->csk, &fds); if ( dev->csk > fdmax ) fdmax = dev->csk;
        FD_SET(dev->isk, &fds); if ( dev->isk > fdmax ) fdmax = dev->isk;
      }
      timeout.tv_sec=0;
      timeout.tv_usec=20000;

      if ( select(fdmax+1,&fds,NULL,NULL,&timeout) < 0 ) emit_log("prob select");
      // Incoming connection ?

      if ( csk>=0 && FD_ISSET(csk,&fds) ) {

        struct motion_dev *dev = accept_device(csk, isk);
        dev->index = next_devindex++;
        dev->next = devs;
        devs = dev;
        setup_device(dev);
      }

      // Incoming input report ?
      for ( struct motion_dev *dev=devs; dev; dev=dev->next )
        if ( FD_ISSET(dev->isk, &fds) ) {
          unsigned char report[256];
          int nr = recv(dev->isk, report, sizeof(report), 0);

          if ( nr <= 0 ) {
            emit_log(QString("%1 disconnected").arg(dev->index));
            close(dev->csk); close(dev->isk);
            struct motion_dev **pdev;
            for ( pdev=&devs; *pdev!=dev; pdev=&(*pdev)->next ) ;
            *pdev = dev->next;
            free(dev);
          } else {
            if ( report[0] == 0xa1 ) {
                parse_report(dev, report+1, nr-1);
            }
          }
        }
    }
}


/**********************************************************************/
// Bluetooth HID devices

// Incoming connections.
int DS3::l2cap_listen(const bdaddr_t *bdaddr, unsigned short psm)
{
  int sk = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
  if ( sk < 0 ) emit_log("prob socket");

  struct sockaddr_l2 addr;
    addr.l2_family = AF_BLUETOOTH;
    //addr.l2_bdaddr = *BDADDR_ANY;
    addr.l2_bdaddr = *bdaddr;
    addr.l2_psm = htobs(psm);
    addr.l2_cid = 0;

  if ( bind(sk, (struct sockaddr *)&addr, sizeof(addr)) < 0 ) {
    emit_log("prob bind");
    close(sk);
    return -1;
  }

  if ( listen(sk, 5) < 0 ) emit_log("prob listen");
  return sk;
}

struct DS3::motion_dev* DS3::accept_device(int csk, int isk)
{
  emit_log("Incoming connection...");
  struct motion_dev *dev =(motion_dev*)malloc(sizeof(struct motion_dev));
  if ( ! dev ) emit_log("prob malloc");

  dev->csk = accept(csk, NULL, NULL);
  if ( dev->csk < 0 ) emit_log("prob accept(CTRL)");
  dev->isk = accept(isk, NULL, NULL);
  if ( dev->isk < 0 ) emit_log("prob accept(INTR)");

  struct sockaddr_l2 addr;
  socklen_t addrlen = sizeof(addr);
  if ( getpeername(dev->isk, (struct sockaddr *)&addr, &addrlen) < 0 )
    emit_log("prob getpeername");
  dev->addr = addr.l2_bdaddr;

  {
    // Distinguish SIXAXIS / DS3
    unsigned char resp[64];
    char get03f2[] = { HIDP_TRANS_GET_REPORT | HIDP_DATA_RTYPE_FEATURE | 8,
                       0xf2, sizeof(resp), sizeof(resp)>>8 };
    send(dev->csk, get03f2, sizeof(get03f2), 0);  // 0301 is interesting too.
    int nr = recv(dev->csk, resp, sizeof(resp), 0);
    if ( force_sixaxis ) dev->type = SIXAXIS;
    else if ( force_ds3 ) dev->type = DS_3;
    else dev->type = (resp[13]==0x40) ? SIXAXIS : DS_3;  // My guess.
  }

  return dev;
}

// Outgoing connections.

int DS3::l2cap_connect(bdaddr_t *ba, unsigned short psm)
{
  int sk = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
  if ( sk < 0 ) emit_log("prob socket");

  struct sockaddr_l2 daddr;
    daddr.l2_family = AF_BLUETOOTH;
    daddr.l2_bdaddr = *ba;
    daddr.l2_psm = htobs(psm);
    daddr.l2_cid = 0;

    if ( ::connect(sk, (struct sockaddr *)&daddr, sizeof(daddr)) < 0 ) emit_log("prob connect");

  return sk;
}

struct DS3::motion_dev *DS3::connect_device(bdaddr_t *ba) {
  emit_log(QString("Connecting to %1\n").arg(myba2str(ba)));

  struct motion_dev *dev = (motion_dev*)malloc(sizeof(struct motion_dev));
  if ( ! dev ) printf("prob malloc");
  dev->addr = *ba;

  dev->csk = l2cap_connect(ba, L2CAP_PSM_HIDP_CTRL);
  dev->isk = l2cap_connect(ba, L2CAP_PSM_HIDP_INTR);
  dev->type = DS_3;
  return dev;
}

/**********************************************************************/
// Device setup



void DS3::hidp_trans(int csk, char *buf, int len) {
  if ( send(csk, buf, len, 0) != len ) emit_log("prob send(CTRL)");
  char ack;
  int nr = recv(csk, &ack, sizeof(ack), 0);
  if ( nr!=1 || ack!=0 ) emit_log("prob ack");
}


void DS3::setup_device(struct motion_dev *dev) {

  switch ( dev->type )
  {
      case SIXAXIS:
      case DS_3: {
        // Enable reporting
        char set03f4[] = { HIDP_TRANS_SET_REPORT | HIDP_DATA_RTYPE_FEATURE, 0xf4,
                           0x42, 0x03, 0x00, 0x00 };
        hidp_trans(dev->csk, set03f4, sizeof(set03f4));
        // Leds: Display 1+index in additive format.
        static const char ledmask[10] = { 1, 2, 4, 8, 6, 7, 11, 13, 14, 15 };
        #define LED_PERMANENT 0xff, 0x27, 0x00, 0x00, 0x32
        char set0201[] = {
          HIDP_TRANS_SET_REPORT | HIDP_DATA_RTYPE_OUTPUT, 0x01,
          0x00, 0x00, 0x00, 0,0, 0x00, 0x00, 0x00,
          0x00, ledmask[dev->index%10]<<1,
          LED_PERMANENT,
          LED_PERMANENT,
          LED_PERMANENT,
          LED_PERMANENT,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        };
        if ( dev->type == SIXAXIS ) {
          set0201[5] = 0xff;  // Enable gyro
          set0201[6] = 0x78;  // Constant bias (should adjust periodically ?)
        } else {
          set0201[5] = 20;     // Set to e.g. 20 to test rumble on startup
          set0201[6] = 0x70;  // Weak rumble
        }
        hidp_trans(dev->csk, set0201, sizeof(set0201));
        break;
      }
  }
}


void DS3::rumble(unsigned char value)
{

    static const char ledmask[10] = { 1, 2, 4, 8, 6, 7, 11, 13, 14, 15 };
    #define LED_PERMANENT 0xff, 0x27, 0x00, 0x00, 0x32
    char set0201[] = {
      HIDP_TRANS_SET_REPORT | HIDP_DATA_RTYPE_OUTPUT, 0x01,
      0x00, 0x00, 0x00, 0,0, 0x00, 0x00, 0x00,
      0x00, ledmask[devs->index%10]<<1,
      LED_PERMANENT,
      LED_PERMANENT,
      LED_PERMANENT,
      LED_PERMANENT,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };

    set0201[5] = 20;     // left timeout
    set0201[6] = value;  // left force
    set0201[3] = 20;     // right timeout
    set0201[4] = value;  // right force

    hidp_trans(devs->csk, set0201, sizeof(set0201));

}

/**********************************************************************/
// Reports
unsigned char DS3::BatterieLevel()
{
    return batterie;
}

void DS3::parse_report_sixaxis_ds3(unsigned char *r, int len)
{
    if ( r[0]==0x01 && len>=49 )
    {
        /*
        int aX = r[41]*256 + r[42] - 512;
        int aY = r[43]*256 + r[44] - 512;
        int aZ = r[45]*256 + r[46] - 512;
        int gZ = r[47]*256 + r[48] - 512;
        int X = r[49]*256 + r[50] ;
        int Y = r[37]*256 + r[38] ;
        int Z = r[35]*256 + r[36] ;
        printf(" aX=%-4d aY=%-4d aZ=%-4d gZ=%-4d\n", aX,aY,aZ, gZ);
        printf(" X=%-4d Y=%-4d Z=%-4d\n", X,Y,Z);*/
        //for(int i=0;i<20;i++) printf("%i ",r[i]);
        //emit_log(QString("%1").arg(aX));
        batterie=r[30];

        if((r[3]&16)==16 && (last_data[3]&16)==0) emit buttonValueChanged(2,true);
        if((r[3]&16)==0 && (last_data[3]&16)==16) emit buttonValueChanged(2,false);

        if((r[3]&64)==64 && (last_data[3]&64)==0) emit buttonValueChanged(1,true);
        if((r[3]&64)==0 && (last_data[3]&64)==64) emit buttonValueChanged(1,false);

        if((r[3]&32)==32 && (last_data[3]&32)==0) emit buttonValueChanged(4,true);
        if((r[3]&32)==0 && (last_data[3]&32)==32) emit buttonValueChanged(4,false);

        if((r[3]&128)==128 && (last_data[3]&128)==0) emit buttonValueChanged(3,true);
        if((r[3]&128)==0 && (last_data[3]&128)==128) emit buttonValueChanged(3,false);

        if((r[3]&8)==8 && (last_data[3]&8)==0) emit buttonValueChanged(13,true);
        if((r[3]&8)==0 && (last_data[3]&8)==8) emit buttonValueChanged(13,false);

        if((r[3]&4)==4 && (last_data[3]&4)==0) emit buttonValueChanged(14,true);
        if((r[3]&4)==0 && (last_data[3]&4)==4) emit buttonValueChanged(14,false);

        if((r[2]&1)==1 && (last_data[2]&1)==0) emit buttonValueChanged(11,true);
        if((r[2]&1)==0 && (last_data[2]&1)==1) emit buttonValueChanged(11,false);

        if((r[2]&8)==8 && (last_data[2]&8)==0) emit buttonValueChanged(12,true);
        if((r[2]&8)==0 && (last_data[2]&8)==8) emit buttonValueChanged(12,false);

        if((r[2]&16)==16 && (last_data[2]&16)==0) emit buttonValueChanged(15,true);//up
        if((r[2]&16)==0 && (last_data[2]&16)==16) emit buttonValueChanged(15,false);

        if((r[2]&64)==64 && (last_data[2]&64)==0) emit buttonValueChanged(16,true);//down
        if((r[2]&64)==0 && (last_data[2]&64)==64) emit buttonValueChanged(16,false);

        compute_dead_zone(&r[6]);
        compute_dead_zone(&r[7]);
        compute_dead_zone(&r[8]);
        compute_dead_zone(&r[9]);
        if(r[6]!=last_data[6] || r[7]!=last_data[7] || r[8]!=last_data[8] || r[9]!=last_data[9]) emit axisValueChanged(r[6]*256,r[7]*256,r[9]*256,r[8]*256);

        memcpy(last_data,r,49);
    }

}

void DS3::compute_dead_zone(unsigned char *value)
{
    if(*value>128+DEAD_ZONE)
    {
        *value=*value-DEAD_ZONE-128;
    }
    else if(*value<128-DEAD_ZONE)
    {
        *value=*value+DEAD_ZONE-128;
    }
    else
    {
        *value=0;
    }

}


void DS3::parse_report(struct motion_dev *dev, unsigned char *r, int len) {
  switch ( dev->type ) {
  case SIXAXIS:
    parse_report_sixaxis_ds3(r, len);
    break;
  case DS_3:
    parse_report_sixaxis_ds3(r, len);
    break;
  }
  fflush(stdout);
}


// ----------------------------------------------------------------------
// Replacement for libbluetooth

int DS3::mystr2ba(const char *s, bdaddr_t *ba) {
  if ( strlen(s) != 17 ) return 1;
  for ( int i=0; i<6; ++i ) {
    int d = strtol(s+15-3*i, NULL, 16);
    if ( d<0 || d>255 ) return 1;
    ba->b[i] = d;
  }
  return 0;
}

char *DS3::myba2str(const bdaddr_t *ba) {
  static char buf[2][18];  // Static buffer valid for two invocations.
  static int index = 0;
  index = (index+1)%2;
  sprintf(buf[index], "%02x:%02x:%02x:%02x:%02x:%02x",
          ba->b[5], ba->b[4], ba->b[3], ba->b[2], ba->b[1], ba->b[0]);
  return buf[index];
}


