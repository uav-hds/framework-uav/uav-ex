//  created:    2013/06/26
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//
//
/*********************************************************************/

#include "Loop.h"
#include <FrameworkManager.h>
#include <tclap/CmdLine.h>
#include <stdio.h>

//on utilise le namespace std, ce qui évite d'écrire par exemple std::string
using namespace std;
//idem
using namespace TCLAP;
//idem
using namespace framework::core;

//variables globales pour les arguments de la ligne de commande
string log_path;//chemin pour sotcker les logs
string address;//adresse pour la connexion avec la station sol
int port;//port pour la connexion avec la station sol
string xml_file;//fichier de réglages
string name;//nom du FrameworkManager

//declaration de la fonction
void parseOptions(int argc, char** argv);

//fonction main
int main(int argc, char* argv[])
{
    //on récupére les arguments de la ligne de commande (name,port,xml_file,log_path)
    parseOptions(argc,argv);

    //creation du FrameworkManager
    FrameworkManager *manager= new FrameworkManager(name);
    manager->SetupConnection(address,port);
    manager->SetupUserInterface(xml_file);
    manager->SetupLogger(log_path);

    //creation de la boucle
    Loop* exemple=new Loop(manager,"Loop");

    //on lance la boucle
    exemple->Start();
    //on attend qu'elle se finisse (par le bouton kill)
    exemple->Join();

    delete manager;
}


void parseOptions(int argc, char** argv)
{
	try
	{
        CmdLine cmd("Command description message", ' ', "0.1");

        //nom du FrameworkManager, par défaut ex_framework
        ValueArg<string> nameArg("n","name","prog name",false,"ex_framework","string");
        cmd.add( nameArg );

        //fichier de réglages, par défaut ./setup.xml
        ValueArg<string> xmlArg("x","xml","fichier xml",false,"./setup.xml","string");
        cmd.add( xmlArg );

        //chemin des logs, par défaut ./
        ValueArg<string> logsArg("l","logs","repertoire des logs",false,"./","string");
        cmd.add( logsArg );

        //adresse pour la station sol, par défaut 9000
        ValueArg<string> addressArg("a","address","addresse station sol",false,"127.0.0.1","string");
        cmd.add( addressArg );

        //port pour la station sol, par défaut 9000
        ValueArg<int> portArg("p","port","port pour station sol",false,9000,"int");
        cmd.add( portArg );


        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        log_path = logsArg.getValue();
        port=portArg.getValue();
        xml_file = xmlArg.getValue();
        name=nameArg.getValue();
        address=addressArg.getValue();

	} catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
}
