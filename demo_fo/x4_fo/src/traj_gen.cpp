//  created:    2011/05/01
//  filename:   traj_gen.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    objet permettant la generation d'une trajectoire
//
//
/*********************************************************************/

#include "traj_gen.h"
#include <stdio.h>
#include <cmath>

trajectory_generator::trajectory_generator()
{
    first_update=true;
    is_started=false;

    T=0;//period auto
    max_veloctity=0;
    acceleration=0;

    pos=0;
    v=0;

}

trajectory_generator::~trajectory_generator()
{

}

void trajectory_generator::start(float start_pos,float end_pos,float max_vel,float accel)
{
    is_started=true;
    is_finished=false;
    max_veloctity=max_vel;
    acceleration=accel;


    //configure trajectory
    end_position=end_pos;
    pos=start_pos;
    acc=acceleration;
    v=0;
    if(end_position<start_pos)
    {
        acc=-acc;
        //max_veloctity=-max_veloctity;
    }
}

//revoir l'interet du stop?
void trajectory_generator::stop(void)
{
    is_started=false;
    v=0;
}

float trajectory_generator::Position()
{
   return pos;
}

float trajectory_generator::Speed()
{
   return v;
}

void trajectory_generator::Update(RTIME time)
{
    float delta_t;

    if(T==0)
    {
        if(first_update==true)
        {
            delta_t=0;
            first_update=false;
        }
        else
        {
            delta_t=(float)(time-previous_time)/1000000000.;
        }
    }
    else
    {
        delta_t=T;
    }
    previous_time=time;


    if(is_started==true)
    {
        if(is_finished==false)
        {

            v+=acc*delta_t;
            if(fabs(v)>fabs(max_veloctity))
            {
                if(v>0)
                    v=max_veloctity;
                else
                    v=-max_veloctity;
            }
            pos+=v*delta_t;
            if(end_position-v*v/(2*acc)<=pos && v>=0) acc=-acc;
            if(end_position-v*v/(2*acc)>=pos && v<0) acc=-acc;
            if(pos>=end_position && v>=0) is_finished=true;
            if(pos<=end_position && v<0) is_finished=true;
        }
        //not a else since is_finished migth change in previous block
        if(is_finished==true)
        {
            v=0;
            pos=end_position;
        }

    }
}
