//  created:    2015/10/19
//  filename:   DemoFollowLine.cpp
//
//  author:     Gildas Bayard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo follow line
//
//
/*********************************************************************/

#include "DemoFollowLine.h"
#include <Uav.h>
#include <Camera.h>
#include <GroupBox.h>
#include <DoubleSpinBox.h>
#include <cvmatrix.h>
#include <MetaDualShock3.h>
#include <LowPassFilter.h>
#include <Pid.h>
#include <Sobel.h>
#include <ImgThreshold.h>
#include <Tab.h>
#include <GridLayout.h>
#include <CvtColor.h>
#include <Picture.h>
#include <HoughLines.h>
#include <DataPlot1D.h>
#include <Ahrs.h>
#include <AhrsData.h>

using namespace std;
using namespace framework::core;
using namespace framework::sensor;
using namespace framework::gui;
using namespace framework::filter;
using namespace framework::meta;

DemoFollowLine::DemoFollowLine(Uav* uav): DemoOpticalFlow(uav) {
    //We disabled the X and Y optical flow max speed parameters
    maxXSpeed->setEnabled(false);
    maxYSpeed->setEnabled(false);
    //We add a speed along the line step parameter
    speedStep=new DoubleSpinBox(opticalFlowGroupBox->NewRow(),"speed step"," m/s",-5,5,0.1,1);
    opticalFlowReference->SetValue(0,0,0); //we start with a zero speed along line
    opticalFlowReference->SetValue(1,0,0); //we don't want any side movement, we set speed along Y axis to zero once and for all

    Sobel *sobelAlongX=new Sobel(greyCameraImage,uav->GetVerticalCamera()->GetLayout()->NewRow(),"sobel along x");
    ImgThreshold *threshold=new ImgThreshold(sobelAlongX,uav->GetVerticalCamera()->GetLayout()->NewRow(),"threshold");
    houghLines=new HoughLines(threshold,uav->GetVerticalCamera()->GetLayout()->NewRow(),"houghlines");

    Picture* sobelPlot=new Picture(uav->GetVerticalCamera()->GetPlotTab()->LastRowLastCol(),sobelAlongX->ObjectName(),sobelAlongX->Output());
    Picture* threshPlot=new Picture(uav->GetVerticalCamera()->GetPlotTab()->LastRowLastCol(),threshold->ObjectName(),threshold->Output());

    DataPlot1D *plotDistance=new DataPlot1D(uav->GetVerticalCamera()->GetPlotTab()->NewRow(),"distance",-20,20);
            plotDistance->AddCurve(houghLines->Output()->Element(0));
    DataPlot1D *plotOrientation=new DataPlot1D(uav->GetVerticalCamera()->GetPlotTab()->LastRowLastCol(),"orientation",-90,90);
            plotOrientation->AddCurve(houghLines->Output()->Element(2));
    DataPlot1D *plotLineDetected=new DataPlot1D(uav->GetVerticalCamera()->GetPlotTab()->LastRowLastCol(),"line detected",0,1.1);
            plotLineDetected->AddCurve(houghLines->Output()->Element(3));

    wasLineDetected=false;


    customOrientation=new AhrsData(this,"orientation");
}

void DemoFollowLine::SignalEvent(Event_t event) {
    UavStateMachine::SignalEvent(event);
    switch(event) {
    case Event_t::EnteringFailSafeMode:
        //We shall reset speed along line value
        opticalFlowReference->SetValue(0,0,0);
        //enable speedStep
        speedStep->setEnabled(true);
        break;
    }
}

void DemoFollowLine::ExtraCheckJoystick(void) {
    DemoOpticalFlow::ExtraCheckJoystick();

    static bool wasFollowLineModeButtonPressed=false;
    static bool wasIncreaseSpeedButtonPressed=false;
    static bool wasDecreaseSpeedButtonPressed=false;
    // controller button R1 enters follow the line mode

    if(GetJoystick()->IsButtonPressed(9)) { // R1
        if (!wasFollowLineModeButtonPressed) {
            wasFollowLineModeButtonPressed=true;
            if (SetOrientationMode(OrientationMode_t::Custom)) {
                Thread::Info("(Re)entering follow line mode\n");
                //disable speedStep to have consistent increment/decrement of opticalFlowReference
                speedStep->setEnabled(false);
            } else {
                Thread::Warn("Could not enter follow line mode\n");
            }
        }
    } else {
        wasFollowLineModeButtonPressed=false;
    }

    // if we are in follow the line mode, we care about up and down buttons
    if (GetOrientationMode()==OrientationMode_t::Custom) {
        // controller button up increases speed along line
        if(GetJoystick()->IsButtonPressed(12)) { // UP
            if (!wasIncreaseSpeedButtonPressed) {
                wasIncreaseSpeedButtonPressed=true;
                opticalFlowReference->SetValue(0,0,opticalFlowReference->Value(0,0)+speedStep->Value());
                Thread::Info("Increasing speed along line. Speed is now %f\n", opticalFlowReference->Value(0,0));
            }
        } else {
            wasIncreaseSpeedButtonPressed=false;
        }

        // controller button down decreases speed along line
        if(GetJoystick()->IsButtonPressed(13)) { // DOWN
            if (!wasDecreaseSpeedButtonPressed) {
                wasDecreaseSpeedButtonPressed=true;
                opticalFlowReference->SetValue(0,0,opticalFlowReference->Value(0,0)-speedStep->Value());
                Thread::Info("Decreasing speed along line. Speed is now %f\n", opticalFlowReference->Value(0,0));
            }
        } else {
            wasDecreaseSpeedButtonPressed=false;
        }
    }
}

const AhrsData *DemoFollowLine::GetReferenceOrientation(void) {
    //ne pas mettre en static?
    //pour pouvoir maitriser le reset quand on passe en suivi de ligne
    static float error_lateralPosition;
    Euler refAngles;

    //a mettre autre part car ne concerne pas le GetReferenceOrientation
    //par exemple dans l'evenemnt entering control loop, si on est en custom
    //basculement ligne -> pas ligne
    if (!houghLines->isLineDetected() && wasLineDetected) {
        wasLineDetected=false;
        //GetJoystick()->SetYawRef(GetUav()->GetAhrs()->GetDatas()->GetQuaternion());//inutile??
        Printf("ligne -> pas ligne\n");
    }

    //basculement pas ligne -> ligne
    if(houghLines->isLineDetected() && !wasLineDetected) {
        wasLineDetected=true;
        Printf("pas ligne -> ligne\n");
    }

    //Yaw angle
    Quaternion currentQuaternion=GetCurrentQuaternion();
    Euler currentAngles;//in vrpn frame
    currentQuaternion.ToEuler(currentAngles);
    if (houghLines->isLineDetected()) {
        refAngles.yaw=0;
    } else {
        refAngles.yaw=currentAngles.yaw;
    }

    // Forward speed (in pixel/s)
    if (houghLines->isLineDetected()) {
        float error_speedAlongLine=opticalFlowSpeedFiltered->Output(0,0)-opticalFlowReference->Value(0,0);
        float errorVariation_speedAlongLine=0; //there we should use a derivation filter
        // Indeed naive derivation fails because this method is called at a higher frequency than the optical flow calculation frequency (camera frequency)
        u_x->SetValues(error_speedAlongLine, errorVariation_speedAlongLine);
        u_x->Update(GetTime());
        refAngles.pitch=u_x->Output();
    } else {
        refAngles.pitch=0;
    }

    // lateral distance to the line
    if (houghLines->isLineDetected()) {
        error_lateralPosition=-houghLines->GetDistance(); //distance to the line SIGN? and unit?
    } else {
        error_lateralPosition+=opticalFlowSpeedFiltered->Output(1,0)/271.76;
    }
    float errorVariation_lateralPosition=opticalFlowSpeedFiltered->Output(1,0);
    //u_y->SetValues(error_lateralPosition, errorVariation_lateralPosition);
    u_y->SetValues(errorVariation_lateralPosition, 0);
    u_y->Update(GetTime());
    refAngles.roll=-u_y->Output();

    customReferenceOrientation->SetQuaternionAndAngularRates(refAngles.ToQuaternion(),Vector3D(0,0,0));

    return customReferenceOrientation;
}

const AhrsData *DemoFollowLine::GetOrientation(void) const {
    Quaternion ahrsQuaternion;
    Vector3D ahrsAngularSpeed;
    GetDefaultOrientation()->GetQuaternionAndAngularRates(ahrsQuaternion, ahrsAngularSpeed);

    //yaw from line if available
    if (houghLines->isLineDetected()) {
        Euler ahrsEuler=ahrsQuaternion.ToEuler();
        ahrsEuler.yaw=houghLines->GetOrientation();
        Quaternion mixQuaternion=ahrsEuler.ToQuaternion();

        customOrientation->SetQuaternionAndAngularRates(mixQuaternion,ahrsAngularSpeed);
    } else {
        customOrientation->SetQuaternionAndAngularRates(ahrsQuaternion,ahrsAngularSpeed);
    }

    return customOrientation;
}

DemoFollowLine::~DemoFollowLine() {
}
