#ifndef TRAMES_H_INCLUDED
#define TRAMES_H_INCLUDED

#include <typedef.h>


#define RX_PACKET 0x01 //0x81 rendu identique si on passe pas par le xbee
#define NO_TRAME_AVAILABLE 0x50
#define WRONG_CHECKSUM 0x51


//entetes des trames de communication
#define ID_TRAME_LOG 1
#define ID_TRAME_POS_EXT 2
#define ID_TRAME_JOYSTICK 3
#define ID_TRAME_JOYSTICK_TRIM 4
#define ID_TRAME_JOYSTICK_TRIM_ROLL 5
#define ID_TRAME_JOYSTICK_TRIM_PITCH 6
#define ID_TRAME_JOYSTICK_TRIM_GAZ 7

#define FLAG_START_OF 24
#define FLAG_STOP_OF 25
#define ID_TRAME_OF 26
//#define ID_TRAME_MASK 27
#define FLAG_SEND_GAIN 28
//#define FLAG_SAVE_GYRO_BIAIS 29
//#define FLAG_SET_GYRO_BIAIS 30
#define FLAG_ATTERRISSAGE 38
#define FLAG_DECOLLAGE 39
#define FLAG_STOP_Z 40
#define FLAG_WRITE_GAIN 43
#define FLAG_START 44
#define FLAG_STOP 45
#define FLAG_START_Z 48
//#define ID_TRAME_GAINS_CHRU 249
//#define FLAG_RESET_EKF 250
#define ID_TRAME_GAINS 51
//#define FLAG_ZERO_MAG 252
//#define FLAG_ZERO_ACC 253
#define FLAG_KILL 54
#define ID_TRAME_DEBUG 55


struct extract_result{
    uint8 api_id;
	  uint8 trame_id;
	  char debug[512];
	  uint16 log_time;
	};



void extract_buf(uint8* dest,int32 size,uint8* src,int32* pt_rd);

struct extract_result extract_trame_sci(uint8* buf,int32 size);
int32 calc_long_log(void);

#endif // TRAMES_H_INCLUDED


