//  created:    2011/05/01
//  filename:   traj_gen.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    objet permettant la generation d'une trajectoire
//
//
/*********************************************************************/

#ifndef TRAJ_GEN_H
#define TRAJ_GEN_H

#include <native/timer.h>


class trajectory_generator
{

    public:
        trajectory_generator();
        ~trajectory_generator();
        void Update(RTIME time);
        void start(float start_pos,float end_pos,float max_vel,float accel);
        void stop(void);
        float Position();
        float Speed();


    private:
        float end_position;
        float pos,v,acc;
        bool is_finished,is_started;
        RTIME previous_time;
        bool first_update;
        float T,max_veloctity,acceleration;
};

#endif // TRAJ_GEN_H
