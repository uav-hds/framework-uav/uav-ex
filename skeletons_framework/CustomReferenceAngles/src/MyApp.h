//  created:    2015/10/27
//  filename:   MyApp.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    skeleton to use UavStateMachine with custom reference angles only
//
//
/*********************************************************************/

#ifndef MYAPP_H
#define MYAPP_H

#include <UavStateMachine.h>

namespace framework {
    namespace core {
        class AhrsData;
    }
    namespace gui {
        class PushButton;
    }
}

class MyApp : public framework::meta::UavStateMachine {
    public:
        MyApp(framework::meta::Uav* uav);
        ~MyApp();

    private:
        enum class BehaviourMode_t {
            Default,
            CustomReferenceAngles
        };

        BehaviourMode_t behaviourMode;

        void StartCustomAngles(void);
        void StopCustomAngles(void);
        void ExtraCheckPushButton(void);
        void ExtraCheckJoystick(void);
        framework::core::AhrsData *GetReferenceOrientation(void);
        void SignalEvent(Event_t event);

        framework::gui::PushButton *start_CustomAngles,*stop_CustomAngles;
        framework::core::AhrsData *customReferenceOrientation;
};

#endif // MYAPP_H
