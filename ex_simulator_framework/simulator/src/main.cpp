//  created:    2012/11/27
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    main simulateur
//
//
/*********************************************************************/

#include <stdio.h>
#include "Roll.h"
#include <Simulator.h>
#include <SimuImu.h>
#include <tclap/CmdLine.h>

using namespace TCLAP;
using namespace std;
using namespace framework::simulator;
using namespace framework::sensor;

int port;
string address;
string xml_file;

void parseOptions(int argc, char** argv);


int main(int argc, char* argv[])
{
    Simulator *simu;
    Roll* roll;
    SimuImu* imu;

    parseOptions(argc,argv);

    simu= new Simulator("simulateur");
    simu->SetupConnection(address,port);
    simu->SetupUserInterface(xml_file);

    roll=new Roll(simu,"simu roll");
    imu=new SimuImu(roll,"imu",0);

    simu->RunSimu();

    delete simu;

	return 0;

}


void parseOptions(int argc, char** argv)
{
	try
	{
        CmdLine cmd("Command description message", ' ', "0.1");

        ValueArg<string> xmlArg("x","xml","fichier xml",true,"./reglages.xml","string");
        cmd.add( xmlArg );

        ValueArg<string> addressArg("a","address","addresse station sol",true,"127.0.0.1","string");
        cmd.add( addressArg );

        ValueArg<int> portArg("p","port","port pour station sol",true,9002,"int");
        cmd.add( portArg );


        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        port=portArg.getValue();
        xml_file = xmlArg.getValue();
        address=addressArg.getValue();

	} catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
}
