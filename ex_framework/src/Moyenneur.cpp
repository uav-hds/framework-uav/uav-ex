//  created:    2013/06/27
//  filename:   Moyenneur.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant le calcul d'une moyenne
//
//
/*********************************************************************/

#include "Moyenneur.h"
#include <cvmatrix.h>
#include <LayoutPosition.h>
#include <GroupBox.h>
#include <SpinBox.h>

//on utilise le namespace std, ce qui évite d'écrire par exemple std::string
using namespace std;
using namespace framework::core;
using namespace framework::gui;

namespace framework
{
namespace filter
{
//constructeur, on hérite de IODevice
Moyenneur::Moyenneur(const IODevice* parent,const LayoutPosition* position,string name): IODevice(parent,name)
{
    //initialisation interface
    groupbox=new GroupBox(position,name);//conteneur de boites pour les réglages du signal
    nb=new SpinBox(groupbox->NewRow(),"nb:",1,MAX_NB,1);//nombre d'éléments pour la moyenne, saturé à MAX_NB

    //initialisation variables
    for(int i=0;i<MAX_NB;i++) previous_values[i]=0;

    //matrice 1*1 de sortie
    cvmatrix_descriptor* desc=new cvmatrix_descriptor(1,1);
    desc->SetElementName(0,0,"moyenne");//le nom servira pour la légende du graphe et les logs
    output=new cvmatrix(this,desc,floatType,name);

    //on spécifie les données à enregistrer (logs)
    AddDataToLog(output);

}

Moyenneur::~Moyenneur()
{
}

//accesseur pour Output
//il est plus propre de faire une fonction accesseur plutot que de mettre la variable output publique
cvmatrix *Moyenneur::Matrix() const
{
    return output;
}

float Moyenneur::Value(void) const
{
   return output->Value(0,0);
}

//UpdateFrom, c'est ici qu'on implémente le filtre
//cette fonction est appellée automatiquement lorsque l'IODevice parent (défini dans le constructeur) appelle la fonction ProcessUpdate
//dans notre cas c'est le Sinus ou le filtre passe bas
//(voir dans la fonction Sinus::Run, l'appel à ProcessUpdate)
void Moyenneur::UpdateFrom(const io_data *data)
{
    //variable contenant la moyenne
    float result=0;
    //on récupére l'argument d'entrée sous forme de cvmatrix
    cvmatrix *input=(cvmatrix*)data;

    //la valeur d'entrée est mise à la fin du buffer contenant les anciennes valeurs
    previous_values[nb->Value()-1]=input->Value(0,0);

    //on fait la sommme de toutes les valeurs
    for(int i=0;i<nb->Value();i++) result+=previous_values[i];

    //on décalle les anciennes valeurs; la plus anciennes est donc perdue
    //Nb: cela serait optimisable pour ne pas faire de décallage (avec des index de début et de fin)
    //mais ici on reste simple
    for(int i=1;i<nb->Value();i++) previous_values[i-1]=previous_values[i];

    //on place la moyenne dans la matrice de sortie
    output->SetValue(0,0,result/nb->Value());
    //on place le temps corresponcant à la valeur du signal dans la matrice de sortie
    output->SetDataTime(data->DataTime());

    //ProcessUpdate est très important, il faut obligatoirement l'appeler après avoir mis à jour la matrice de sortie
    //cela permet:
    // d'enregistrer la matrice se sortie dans les logs
    // de mettre à jour automatiquement les filtres suivants
    ProcessUpdate(output);
}
}// end namespace filter
}// end namespace framework
