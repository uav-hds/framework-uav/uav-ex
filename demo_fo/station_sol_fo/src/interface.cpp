#include "interface.h"
#include <QDate>
#include "trames.h"
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>

using namespace std;

#define TAU 0.01 //periode d'echantillonage x4


struct gains_all gains;
struct gains_chru chru;
struct control joy_data;
struct control_trim joy_trim;
struct imu_datas orientation;
struct angles orientation_cons;
struct moteurs_8bits pwm_8;
struct altitude alt_fil,alt_cons;
struct optical_flow of;


Interface::Interface(string address,QWidget *parent) : QWidget(parent)
{
    setupUi(this); // A faire en premier

    //create dialog box
    //remote_ip= new address_dialog();
    //connect(remote_ip,SIGNAL(address(QString)),this,SLOT(address(QString)));
    //remote_ip->show();
    startup(QString(address.c_str()));
    //address("192.168.7.2");

}


void Interface::startup(QString ip)
{
    int i;
    joypad=NULL;

    setWindowTitle(QString("X4: %1").arg(ip));
    //delete remote_ip;

    //quelques verifications:
    //la taille est mise dans un uint8 (trames_sci.cpp)
    if(sizeof(gains_all)>205) log_text->append("<strong>Struct gains_all trop grande!!!</strong>");

    //lut image
    for(int i=0;i<256;i++)
    {
        color_table.append(qRgb(i,i,i));
    }


    //joystick
    log_text->append("<strong>tentative ouverture dual shcok 3</strong>");

    //DS3
    joypad=new DS3;
    connect(joypad, SIGNAL(emit_log(QString)),this, SLOT(print_log(QString)));
    connect(this, SIGNAL(ask_rumble(unsigned char)),joypad, SLOT(rumble(unsigned char)));
    connect(joypad, SIGNAL(axisValueChanged(int , int ,int,int)), this, SLOT(joyaxisValueChanged(int , int ,int,int)));
    connect(joypad, SIGNAL(buttonValueChanged(int , bool )), this, SLOT(joybuttonValueChanged(int , bool )));
    joypad->start();


    //sockets
    socket_fd= new udt_socket(ip,9000,65535);//65535 is the max message size
    connect(socket_fd, SIGNAL(emit_log(QString)),this, SLOT(print_log(QString)));
    connect(socket_fd, SIGNAL(data_ready()),this, SLOT(receive()));

    socket_cam= new udt_socket(ip,9001,320*240+2);//65535 is the max message size
    connect(socket_cam, SIGNAL(emit_log(QString)),this, SLOT(print_log(QString)));
    connect(socket_cam, SIGNAL(data_ready()),this, SLOT(receive_cam()));

    //port serie
    //enumerations des ports series dispos
    com_xbee=NULL;
    ports = QextSerialEnumerator::getPorts();
    for ( i = 0; i < ports.size(); i++) combo_xbee->addItem(ports.at(i).physName);

    //graphes
    roll_imu_graphe = new DataPlot("Roll",-20, 20,0.04);
    roll_imu_graphe->addCurve(QPen(Qt::red),"mesure");
    roll_imu_graphe->addCurve(QPen(Qt::blue),"consigne");
    IMU_Layout->addWidget(roll_imu_graphe,0,0);
    pitch_imu_graphe = new DataPlot("Pitch",-20, 20,0.04);
    pitch_imu_graphe->addCurve(QPen(Qt::red),"mesure");
    pitch_imu_graphe->addCurve(QPen(Qt::blue),"consigne");
    IMU_Layout->addWidget(pitch_imu_graphe,0,1);
    yaw_imu_graphe = new DataPlot("Yaw",-180, 180,0.04);
    yaw_imu_graphe->addCurve(QPen(Qt::red),"mesure");
    yaw_imu_graphe->addCurve(QPen(Qt::blue),"consigne");
    IMU_Layout->addWidget(yaw_imu_graphe,0,2);
    droll_imu_graphe = new DataPlot("Gyro roll",-10, 10,0.04);
    droll_imu_graphe->addCurve(QPen(Qt::red),"mesure");
    IMU_Layout->addWidget(droll_imu_graphe,1,0);
    dpitch_imu_graphe = new DataPlot("Gyro pitch",-10, 10,0.04);
    dpitch_imu_graphe->addCurve(QPen(Qt::red),"mesure");
    IMU_Layout->addWidget(dpitch_imu_graphe,1,1);
    dyaw_imu_graphe = new DataPlot("Gyro yaw",-10, 10,0.04);
    dyaw_imu_graphe->addCurve(QPen(Qt::red),"mesure");
    IMU_Layout->addWidget(dyaw_imu_graphe,1,2);


    roll_joy_graphe = new DataPlot("Roll", -32768, 32768,0.04);
    roll_joy_graphe->addCurve(QPen(Qt::red),"drone");
    roll_joy_graphe->addCurve(QPen(Qt::blue),"PC");
    joy_Layout->addWidget(roll_joy_graphe,0,0);
    pitch_joy_graphe = new DataPlot("Pitch", -32768, 32768,0.04);
    pitch_joy_graphe->addCurve(QPen(Qt::red),"drone");
    pitch_joy_graphe->addCurve(QPen(Qt::blue),"PC");
    joy_Layout->addWidget(pitch_joy_graphe,0,1);
    yaw_joy_graphe = new DataPlot("Yaw", -32768, 32768,0.04);
    yaw_joy_graphe->addCurve(QPen(Qt::red),"drone");
    yaw_joy_graphe->addCurve(QPen(Qt::blue),"PC");
    joy_Layout->addWidget(yaw_joy_graphe,1,0);
    gaz_joy_graphe = new DataPlot("Gaz", 0, 65535,0.04);
    gaz_joy_graphe->addCurve(QPen(Qt::red),"drone");
    gaz_joy_graphe->addCurve(QPen(Qt::blue),"PC");
    joy_Layout->addWidget(gaz_joy_graphe,1,1);

    z_graphe = new DataPlot("z", 0,2,0.04);
    z_graphe->addCurve(QPen(Qt::red),"mesure");
    z_graphe->addCurve(QPen(Qt::blue),"consigne");
    v_alt_Layout->addWidget(z_graphe,1,0);

    vz_graphe = new DataPlot("Vz", -1.5,1.5,0.04);
    vz_graphe->addCurve(QPen(Qt::red),"mesure");
    vz_graphe->addCurve(QPen(Qt::blue),"consigne");
    v_alt_Layout->addWidget(vz_graphe,1,1);

    pwm1_graphe = new DataPlot("PWM 1",0,255,0.04);
    pwm1_graphe->addCurve(QPen(Qt::red),"mesure");
    PWM_Layout->addWidget(pwm1_graphe,0,0);
    pwm2_graphe = new DataPlot("PWM 2",0,255,0.04);
    pwm2_graphe->addCurve(QPen(Qt::red),"mesure");
    PWM_Layout->addWidget(pwm2_graphe,0,1);
    pwm3_graphe = new DataPlot("PWM 3",0,255,0.04);
    pwm3_graphe->addCurve(QPen(Qt::red),"mesure");
    PWM_Layout->addWidget(pwm3_graphe,1,0);
    pwm4_graphe = new DataPlot("PWM 4",0,255,0.04);
    pwm4_graphe->addCurve(QPen(Qt::red),"mesure");
    PWM_Layout->addWidget(pwm4_graphe,1,1);

    of_x_graphe=new DataPlot("Vx",-10,10,0.04);
    of_x_graphe->addCurve(QPen(Qt::red),"mesure");
    of_Layout->addWidget(of_x_graphe,0,0);
    of_y_graphe=new DataPlot("Vy",-10,10,0.04);
    of_y_graphe->addCurve(QPen(Qt::red),"mesure");
    of_Layout->addWidget(of_y_graphe,0,1);

    log_time_label->setText(QString("Non connect�"));
    is_connected=false;
    total_bytesRead=0;

    on_ResetRollButton_clicked();
    on_ResetGazButton_clicked();
    on_ResetPitchButton_clicked();

    x4_flying=false;
    save_picture_flag=false;

    startTimer(40.0);

}

Interface::~Interface()
{
    killTimer(0);

    delete joypad;
    delete socket_fd;
    delete socket_cam;

    printf("reset bluetooth\n");
    QProcess p;
    p.start("/etc/init.d/bluetooth restart");
    p.waitForFinished();
}

void Interface::on_connect_xbee_clicked()
{
    delete socket_fd;
    delete socket_cam;
    socket_fd=NULL;
    socket_cam=NULL;
    connect_xbee->setEnabled(false);
    combo_xbee->setEnabled(false);

    log_text->append(QString("<strong>Ouverture du port :</strong> %1").arg(ports.at(combo_xbee->currentIndex()).portName.toLocal8Bit().constData()));
    com_xbee= new xbee(ports.at(combo_xbee->currentIndex()).portName.toLocal8Bit().constData());
    connect(com_xbee, SIGNAL(emit_log(QString)),this, SLOT(print_log(QString)));
    connect(com_xbee, SIGNAL(data_ready()),this, SLOT(receive()));

    char data[255];
    for(int i=0;i<255;i++) data[i]=i;

//com_xbee->SendTrame(data,15,2);
}
/*
** ===================================================================
**     envoit sur socket
**
** ===================================================================
*/

void Interface::send_sock(uint8* buf,int32 size,uint8 id_trame)
{

    if(com_xbee==NULL)
    {
        socket_fd->SendTrame(buf,size,id_trame);
    }
    else
    {
        com_xbee->SendTrame(buf,size,id_trame);
    }
}

uint8 Interface::calc_checksum(uint8* buf,int32 size)
{
    uint8 checksum=0;
    for(int32 i=0;i<size;i++) checksum+=buf[i];

    return (uint8)(255-checksum);
}

void Interface::print_log(QString message)
{
   log_text->append(message);
}

void Interface::on_ClearLogButton_clicked()
{
    log_text->clear();
}

void Interface::on_KillButton_clicked()
{
    log_text->append("envoi kill");

    send_sock(NULL,0,FLAG_KILL);
}

void Interface::on_main_tab_currentChanged (int index)
{
    if(index==main_tab->indexOf(tab_gains))
    {
       RequestGains();
    }
}

void Interface::RequestGains()
{
    log_text->append("envoy� demande gains");

    send_sock(NULL,0,FLAG_SEND_GAIN);

}

void Interface::on_SendButton_clicked()
{

    spinbox_to_gains();
    pwm1_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
    pwm2_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
    pwm3_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
    pwm4_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);

    log_text->append("envoi gains");
    send_sock((uint8*)(&gains),sizeof(gains_all),ID_TRAME_GAINS);

}

void Interface::spinbox_to_gains()
{
    gains.kp_roll=(kp_roll->value());
    gains.kd_roll=kd_roll->value();
    gains.ki_roll=ki_roll->value();
    gains.kp_pitch=kp_pitch->value();
    gains.kd_pitch=kd_pitch->value();
    gains.ki_pitch=ki_pitch->value();
    gains.kp_yaw=kp_yaw->value();
    gains.kd_yaw=kd_yaw->value();
    gains.ki_yaw=ki_yaw->value();
    gains.k1=k1->value();
    gains.k2=k2->value();
    gains.k3=k3->value();
    gains.k4=k4->value();
    gains.offset_roll=offset_roll->value()*PI/(float)180.0;
    gains.offset_pitch=offset_pitch->value()*PI/(float)180.0;
    gains.deb_roll=deb_roll->value();
    gains.deb_pitch=deb_pitch->value();
    gains.deb_yaw=deb_yaw->value();
    gains.deb_gaz=deb_gaz->value();
    gains.int_max_angles=int_max_angles->value();
    gains.int_max_fo=int_max_fo->value();
    gains.pas_decollage=pas_decollage->value();
    gains.seuil_vitesse=seuil_vitesse->value();
    gains.log_divider=log_divider->value();
    gains.pwm_high=pwm_high->value();
    gains.pwm_low=pwm_low->value();
    gains.kd_of_x=kd_of_x->value();
    gains.kd_of_y=kd_of_y->value();
    gains.ki_of_x=ki_of_x->value();
    gains.ki_of_y=ki_of_y->value();
    gains.fc_alt=fc_alt->value();
    gains.fc_of=fc_of->value();
    gains.vitesse_atterrissage=vitesse_atterrissage->value();
    gains.deb_joy_of=deb_joy_of->value();
    gains.alt_dec=alt_dec->value();
    gains.vel_max_dec=vel_max_dec->value();
    gains.acc_dec=acc_dec->value();
    gains.gain=gain->value();
    gains.exposure=exposure->value();
    gains.bright=bright->value();
    gains.contrast=contrast->value();
    gains.hue=hue->value();
    gains.sharpness=sharpness->value();
    gains.autogain=autogain->value();
    gains.awb=awb->value();
    gains.quality_level=quality_level->value();
    gains.min_distance=min_distance->value();

}

void Interface::gains_to_spinbox()
{
    kp_roll->setValue(gains.kp_roll);
    kd_roll->setValue(gains.kd_roll);
    ki_roll->setValue(gains.ki_roll);
    kp_pitch->setValue(gains.kp_pitch);
    kd_pitch->setValue(gains.kd_pitch);
    ki_pitch->setValue(gains.ki_pitch);
    kp_yaw->setValue(gains.kp_yaw);
    kd_yaw->setValue(gains.kd_yaw);
    ki_yaw->setValue(gains.ki_yaw);
    k1->setValue(gains.k1);
    k2->setValue(gains.k2);
    k3->setValue(gains.k3);
    k4->setValue(gains.k4);
    offset_roll->setValue(gains.offset_roll*(float)180.0/PI);
    offset_pitch->setValue(gains.offset_pitch*(float)180.0/PI);
    deb_roll->setValue(gains.deb_roll);
    deb_pitch->setValue(gains.deb_pitch);
    deb_yaw->setValue(gains.deb_yaw);
    deb_gaz->setValue(gains.deb_gaz);
    int_max_angles->setValue(gains.int_max_angles);
    int_max_fo->setValue(gains.int_max_fo);
    pas_decollage->setValue(gains.pas_decollage);
    seuil_vitesse->setValue(gains.seuil_vitesse);
    log_divider->setValue(gains.log_divider);
    pwm_low->setValue(gains.pwm_low);
    pwm_high->setValue(gains.pwm_high);
    kd_of_x->setValue(gains.kd_of_x);
    kd_of_y->setValue(gains.kd_of_y);
    ki_of_x->setValue(gains.ki_of_x);
    ki_of_y->setValue(gains.ki_of_y);
    fc_alt->setValue(gains.fc_alt);
    fc_of->setValue(gains.fc_of);
    vitesse_atterrissage->setValue(gains.vitesse_atterrissage);
    deb_joy_of->setValue(gains.deb_joy_of);
    alt_dec->setValue(gains.alt_dec);
    vel_max_dec->setValue(gains.vel_max_dec);
    acc_dec->setValue(gains.acc_dec);
    gain->setValue(gains.gain);
    exposure->setValue(gains.exposure);
    bright->setValue(gains.bright);
    contrast->setValue(gains.contrast);
    hue->setValue(gains.hue);
    sharpness->setValue(gains.sharpness);
    autogain->setValue(gains.autogain);
    awb->setValue(gains.awb);
    quality_level->setValue(gains.quality_level);
    min_distance->setValue(gains.min_distance);
}


void Interface::on_StartButton_clicked()
{
    if(joy_local.gaz!=0)
    {
        QMessageBox::critical(this, "Molette des gaz", "Mettre les gaz � 0");
    }
    else
    {
        log_text->append("envoi start");
        send_sock(NULL,0,FLAG_START);
        x4_flying=true;
    }

}

void Interface::on_DecollageButton_clicked()
{
    log_text->append("envoi d�collage auto");
    send_sock(NULL,0,FLAG_DECOLLAGE);
    x4_flying=true;
}

void Interface::on_StopButton_clicked()
{
    log_text->append("envoi stop");
    send_sock(NULL,0,FLAG_STOP);
    x4_flying=false;
}

void Interface::on_AtterrissageButton_clicked()
{
    log_text->append("envoi atterrissage");
    send_sock(NULL,0,FLAG_ATTERRISSAGE);
    x4_flying=false;
}


void Interface::on_WriteButton_clicked()
{
    log_text->append("envoi write gains");
    send_sock(NULL,0,FLAG_WRITE_GAIN);
}

void Interface::on_LoadButton_clicked()
{

    QString fichier= QFileDialog::getOpenFileName(this, "Ouvrir un fichier", "./conf", "Fichier conf (*.hex)");
    QByteArray ba = fichier.toLatin1();


    ifstream fd((char*)ba.data(), ios::in | ios::binary);

    fd.read((char*)&gains, sizeof(gains_all));
    fd.close();

    gains_to_spinbox();
}

void Interface::on_SaveButton_clicked()
{
    spinbox_to_gains();

    QString fichier = QFileDialog::getSaveFileName(this, "Enregistrer un fichier", "./conf", "Fichier conf (*.hex)");

    QByteArray ba = fichier.toLatin1();

    ofstream fd((char*)ba.data(), ios::out | ios::trunc | ios::binary);

    fd.write((char*)&gains, sizeof(gains_all));

    fd.close();
}

void Interface::on_StartOFButton_clicked()
{
    log_text->append("envoi start flux optique");
    send_sock(NULL,0,FLAG_START_OF);
}

void Interface::on_StopOFButton_clicked()
{
    log_text->append("envoi stop flux optique");
    send_sock(NULL,0,FLAG_STOP_OF);
}

void Interface::on_save_picture_clicked()
{
    save_picture_flag=true;
}

void Interface::receive_cam()
{
    int32 bytesRead=-1;
    uint8 buf[320*240];
    static int cpt=0;

    while(bytesRead!=0)
    {
        bytesRead=socket_cam->readData((char*)buf, 320*240);
        if(bytesRead==-1) bytesRead=0;

        if(bytesRead!=0)
        {

            QImage image= QImage((const uchar*)buf,320,240,QImage::Format_Indexed8);//Format_RGB888);
            image.setColorTable(color_table);
            if(save_picture_flag==true)
            {
                save_picture_flag=false;
                image.save(QString("optical_flow_%1.bmp").arg(cpt));
                cpt++;
            }

            cam_label->setPixmap(QPixmap::fromImage(image));
        }
    }
}

void Interface::receive()
{
    struct extract_result result;
    result.api_id=RX_PACKET;
    uint8 buf[65535];
    int32 bytesRead=-1;

    while(bytesRead!=0)
    {

        if(socket_fd!=NULL)
        {
            bytesRead=socket_fd->readData((char*)buf, 65535);
        }
        else
        {
            bytesRead=com_xbee->readData((char*)buf, 65535);
        }
        if(bytesRead==-1) bytesRead=0;

        //check connection
        total_bytesRead+=bytesRead;
        if(is_connected==false && bytesRead!=0)
        {
            is_connected=true;
            RequestGains();
            //log_text->append("<strong>Connect�</strong>");

            //get current date and time
            //QDateTime dateTime = QDateTime::currentDateTime();
            //QString dateTimeString = dateTime.toString("./log/yyyyMMdd_hhmm");
        }

        if(bytesRead!=0)
        {
            result=extract_trame_sci(buf,bytesRead);
            //log_text->append(QString("trame %1 %2").arg(result.api_id).arg(result.trame_id));
            switch(result.api_id)
            {
                case WRONG_CHECKSUM:
                    log_text->append("<strong>Wrong checksum</strong>");
                    break;
                case RX_PACKET:
                    if(result.trame_id==ID_TRAME_LOG)
                    {
                        log_time_label->setText(QString("Temps: %L1   DSP: %2 fps").arg((double)(result.log_time*TAU*log_divider->value()),0,'f',2).arg((int)(of.fps)));
                        orientation.droll=orientation.droll*(float)180.0/PI;
                        orientation.dpitch=orientation.dpitch*(float)180.0/PI;
                        orientation.dyaw=orientation.dyaw*(float)180.0/PI;
                        orientation.roll=orientation.roll*(float)180.0/PI;
                        orientation.pitch=orientation.pitch*(float)180.0/PI;
                        orientation.yaw=orientation.yaw*(float)180.0/PI;
                        orientation_cons.roll=orientation_cons.roll*(float)180.0/PI;
                        orientation_cons.pitch=orientation_cons.pitch*(float)180.0/PI;
                        orientation_cons.yaw=orientation_cons.yaw*(float)180.0/PI;
                    }
                    //reception de la trame de gains
                    if(result.trame_id==ID_TRAME_GAINS)
                    {
                        log_text->append("re�u gains");
                        gains_to_spinbox();
                        pwm1_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
                        pwm2_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
                        pwm3_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
                        pwm4_graphe->setYAxisScale((float)gains.pwm_low,(float)gains.pwm_high);
                    }
                    if(result.trame_id==ID_TRAME_JOYSTICK_TRIM)
                    {
                        pitch_joy_graphe->setTitle(QString("Pitch (trim: %L1)").arg((double)joy_trim.trim_pitch,0,'f',0));
                        roll_joy_graphe->setTitle(QString("Roll (trim: %L1)").arg((double)joy_trim.trim_roll,0,'f',0));
                        gaz_joy_graphe->setTitle(QString("Gaz (trim: %L1)").arg((double)joy_trim.trim_gaz,0,'f',0));
                    }
                    if(result.trame_id==ID_TRAME_DEBUG)
                        log_text->append(QString::fromAscii(result.debug, bytesRead-2));
                    break;
            }
        }
    }
}


void Interface::timerEvent(QTimerEvent *)
{
    static uint8 divider=0;

    //check connection
    divider++;
    if(divider==25)
    {
        divider=0;
        if(total_bytesRead==0)
        {
            if(is_connected==true)
            {
                is_connected=false;
                log_time_label->setText(QString("Non connect�"));
            }
        }
        total_bytesRead=0;
    }

    main_tab->setTabText(main_tab->indexOf(tab_joy),QString("Joystick %1/5").arg(joypad->BatterieLevel()));

    //on met a jour les valeurs que si connect�
    if(is_connected==true)
    {
        //joy graphe
        roll_joy_graphe->SetData(joy_data.roll,0);
        pitch_joy_graphe->SetData(joy_data.pitch,0);
        yaw_joy_graphe->SetData(joy_data.yaw,0);
        gaz_joy_graphe->SetData(joy_data.gaz,0);
        roll_joy_graphe->SetData(joy_local.roll,1);
        pitch_joy_graphe->SetData(joy_local.pitch,1);
        yaw_joy_graphe->SetData(joy_local.yaw,1);
        gaz_joy_graphe->SetData(joy_local.gaz,1);


        //imu graphe
        roll_imu_graphe->SetData(orientation.roll,0);
        pitch_imu_graphe->SetData(orientation.pitch,0);
        yaw_imu_graphe->SetData(orientation.yaw,0);
        roll_imu_graphe->SetData(orientation_cons.roll,1);
        pitch_imu_graphe->SetData(orientation_cons.pitch,1);
        yaw_imu_graphe->SetData(orientation_cons.yaw,1);
        droll_imu_graphe->SetData(orientation.droll,0);
        dpitch_imu_graphe->SetData(orientation.dpitch,0);
        dyaw_imu_graphe->SetData(orientation.dyaw,0);


        //pwm graphe
        pwm1_graphe->SetData(pwm_8.moteur[0],0);
        pwm2_graphe->SetData(pwm_8.moteur[1],0);
        pwm3_graphe->SetData(pwm_8.moteur[2],0);
        pwm4_graphe->SetData(pwm_8.moteur[3],0);

        //alt graphes
        z_graphe->SetData(alt_fil.z,0);
        vz_graphe->SetData(alt_fil.vz,0);
        z_graphe->setTitle(QString("z: %L1m").arg((double)alt_fil.z,0,'f',2));
        vz_graphe->setTitle(QString("Vz: %1m/s").arg((double)alt_fil.vz,0,'f',2));

        z_graphe->SetData(alt_cons.z,1);
        vz_graphe->SetData(alt_cons.vz,1);
        //z_graphe->SetData(orientation.az,2);
        //z_graphe->SetData((float)joy_local.gaz/32768,1);

        //of graphe
        of_x_graphe->SetData(of.x,0);
        of_y_graphe->SetData(of.y,0);

    }

    //if(main_tab->indexOf(tab_joy)==main_tab->currentIndex())
    {
        roll_joy_graphe->Draw();
        pitch_joy_graphe->Draw();
        yaw_joy_graphe->Draw();
        gaz_joy_graphe->Draw();
    }

    //if(main_tab->indexOf(tab_imu)==main_tab->currentIndex())
    {
        roll_imu_graphe->setTitle(QString("Roll: %L1�").arg((double)orientation.roll,0,'f',1));
        pitch_imu_graphe->setTitle(QString("Pitch: %L1�").arg((double)orientation.pitch,0,'f',1));
        yaw_imu_graphe->setTitle(QString("Yaw: %L1�").arg((double)orientation.yaw,0,'f',1));
        roll_imu_graphe->Draw();
        pitch_imu_graphe->Draw();
        yaw_imu_graphe->Draw();
        droll_imu_graphe->Draw();
        dpitch_imu_graphe->Draw();
        dyaw_imu_graphe->Draw();
    }
    //if(main_tab->indexOf(tab_pwm)==main_tab->currentIndex())
    {
        pwm1_graphe->Draw();
        pwm2_graphe->Draw();
        pwm3_graphe->Draw();
        pwm4_graphe->Draw();
    }
    //if(main_tab->indexOf(tab_vgps)==main_tab->currentIndex())
    {
        vz_graphe->Draw();
        z_graphe->Draw();
    }
    //if(main_tab->indexOf(tab_of)==main_tab->currentIndex())
    {
        of_x_graphe->Draw();
        of_y_graphe->Draw();
    }

}


void Interface::joyaxisValueChanged(int value0,int value1,int value2,int value3)
{
//printf("%i %i\n",axis,value);
    joy_local.roll=value0;
    joy_local.pitch=value1;
    joy_local.yaw=value3;
    joy_local.gaz=32767-value2;

    send_sock((uint8*)(&joy_local),sizeof(control),ID_TRAME_JOYSTICK);
}

void Interface::joybuttonValueChanged(int button, bool value)
{
    signed char trim_value;
    if(button==7 && value==1) joy_local.yaw=-32767;
    if(button==8 && value==1) joy_local.yaw=32767;
    if((button==7 || button==8) && value==0) joy_local.yaw=0;

    if(button==7 || button==8)
    {
        send_sock((uint8*)(&joy_local),sizeof(control),ID_TRAME_JOYSTICK);//pas d'ack
    }

    if(button==0 && value==1) on_StopButton_clicked();

    //trims
    if(button==1 && value==1)
    {
        trim_value=-1;
        send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_PITCH);
    }
    if(button==2 && value==1)
    {
        trim_value=1;
        send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_PITCH);
    }
    if(button==3 && value==1)
    {
        trim_value=1;
        send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_ROLL);
    }
    if(button==4 && value==1)
    {
        trim_value=-1;
        send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_ROLL);
    }
    if(button==10 && value==1)
    {
        trim_value=1;
        send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_GAZ);
    }
    if(button==9 && value==1)
    {
        trim_value=-1;
        send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_GAZ);
    }

    //decollage/atterrissage auto
    if(button==12 && value==1)
    {
        emit ask_rumble(0x70);
        if(x4_flying==1)
            on_AtterrissageButton_clicked();
        else
            on_DecollageButton_clicked();

    }
    if(button==11 && value==1)
    {
        emit ask_rumble(0xff);
        on_StopButton_clicked();
    }

    //flux optique
    if(button==13 && value==1)
    {
        emit ask_rumble(0x70);
        on_StartOFButton_clicked();
    }
    if(button==14 && value==1)
    {
        emit ask_rumble(0xff);
        on_StopOFButton_clicked();
    }


    //stabilisation Z
    if(button==5 && value==1)
    {
        log_text->append("envoi consigne Z");
        send_sock(NULL,0,FLAG_START_Z);
    }

    if(button==6 && value==1)
    {
        log_text->append("stop consigne Z");
        send_sock(NULL,0,FLAG_STOP_Z);
    }

}

void Interface::on_ResetRollButton_clicked()
{
    signed char trim_value=0;
    send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_ROLL);
    log_text->append("Reset trim roll");
}

void Interface::on_ResetPitchButton_clicked()
{
    signed char trim_value=0;
    send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_PITCH);
    log_text->append("Reset trim pitch");
}

void Interface::on_ResetGazButton_clicked()
{
    signed char trim_value=0;
    send_sock((uint8*)(&trim_value),sizeof(signed char),ID_TRAME_JOYSTICK_TRIM_GAZ);
    log_text->append("Reset trim gaz");
}
