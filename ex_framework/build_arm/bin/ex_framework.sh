#! /bin/bash

# $1 is the ground station ip
if [ "$#" -ne 1 ] ; then
  echo "Usage: $0 ground_station_ip_address"
  exit 1
fi

if [ -f /proc/xenomai/version ];then
	EXEC=./ex_framework_rt
else
	EXEC=./ex_framework_nrt
fi

$EXEC -a ${1}

