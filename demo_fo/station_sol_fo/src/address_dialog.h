#ifndef ADDRESS_DIALOG_H
#define ADDRESS_DIALOG_H


#include <QtGui>


class address_dialog : public QDialog //QWidget
{

    Q_OBJECT

    public:
        address_dialog();
        ~address_dialog();

    private:
        QLineEdit *add_text;
        QPushButton *ok_button;
        QSettings *settings;

    private slots:
        void ok();

    signals:
        void address(QString);

    protected:

    public slots:

};

#endif // ADDRESS_DIALOG_H
