#include "DataPlot.h"
#include <stdlib.h>
#include <qwt_painter.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_scale_widget.h>
#include <qwt_legend_label.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_panner.h>
#include <QMouseEvent>
#include <QMenu>
#include <QInputDialog>
//#include <stdio.h>


DataPlot::DataPlot(QString title,float ymin, float ymax,float period):
    QwtPlot()
{

//printf("a revoir qd on arrive a la fin du PLOT_SIZE, il faudrait faire un realloc pour rien perdre\n");

    view_size=20;//default 20s
    refresh_rate=0.1;//default 100ms
/*
     // Disable polygon clipping
    QwtPainter::setDeviceClipping(false);

    // We don't need the cache here
    canvas()->setPaintAttribute(QwtPlotCanvas::PaintCached, false);
    canvas()->setPaintAttribute(QwtPlotCanvas::PaintPacked, false);
*/
    //scroll bar
    sb=new ScrollBar(Qt::Horizontal,canvas());
    connect(sb,SIGNAL(valueChanged(Qt::Orientation, double, double)),SLOT(scrollBarMoved(Qt::Orientation, double, double)));
    scrolling=true;


#if QT_VERSION >= 0x040000
#ifdef Q_WS_X11
    /*
       Qt::WA_PaintOnScreen is only supported for X11, but leads
       to substantial bugs with Qt 4.2.x/Windows
     */
    canvas()->setAttribute(Qt::WA_PaintOnScreen, true);
#endif
#endif

    //minimum size; sinon widget trop grand
    setMinimumHeight(1);
    setMinimumWidth(1);

    alignScales();
    d_y=new QList<double*>;
    datas=new QList<QwtPlotCurve*>;
    datas_type=new QList<int>;

    //  Initialize data
    for (int i = 0; i< view_size/refresh_rate; i++) d_x[i] = i*refresh_rate;
    data_index=0;
    temps=0;
    min_data_index=0;
    max_data_index=0;
    refresh_rate=period;

    // Assign a title
    setTitle(title);

    // Axis
    setAxisTitle(QwtPlot::xBottom, "Sample Time");
    setAxisScale(QwtPlot::xBottom,0, view_size);

    setAxisTitle(QwtPlot::yLeft, "Values");
    setYAxisScale(ymin,ymax);

    // grid
    QwtPlotGrid *grid = new QwtPlotGrid;
    //grid->enableXMin(false);
    grid->setPen(QPen(Qt::black, 0, Qt::DotLine));
    //grid->setMinPen(QPen(Qt::gray, 0 , Qt::DotLine));
    grid->attach(this);

    //zoomer
    QwtPlotMagnifier * zoomer = new QwtPlotMagnifier(this->canvas());
    zoomer->setMouseButton(Qt::RightButton,Qt::ControlModifier);
    zoomer->setAxisEnabled(xBottom,0);

    //scroller
    QwtPlotPanner *scroller =new QwtPlotPanner(this->canvas());
    scroller->setAxisEnabled(xBottom,0);

    //legend
    QwtLegend* new_legend=new QwtLegend();
    new_legend->setDefaultItemMode(QwtLegendData::Checkable);
    insertLegend(new_legend, QwtPlot::BottomLegend);

    connect( new_legend, SIGNAL( checked( const QVariant &, bool, int ) ), SLOT( legendChecked( const QVariant &, bool ) ) );

    canvas()->installEventFilter(this);

}

DataPlot::~DataPlot()
{
    for(int i=0;i<d_y->count();i++)
    {
        free(d_y->at(i));
    }
    delete d_y;

    delete datas;
    delete datas_type;
}

bool DataPlot::eventFilter(QObject *o, QEvent *e)
{
    if (  o == canvas() )
    {
        switch(e->type())
        {
            case QEvent::Resize:
            {
                const int fw = ((QwtPlotCanvas *)canvas())->frameWidth();

                QRect rect;
                rect.setSize(((QResizeEvent *)e)->size());
                rect.setRect(rect.x() + fw, rect.y() + fw,
                    rect.width() - 2 * fw, rect.height() - 2 * fw);

                sb->setGeometry(0, 0, rect.width(), 10);

                break;
            }

            default:
                break;
        }
    }
    return QwtPlot::eventFilter(o, e);
}

void DataPlot::legendChecked( const QVariant &itemInfo, bool on )
{
    QwtPlotItem *plotItem = infoToItem( itemInfo );
    if ( plotItem )
        showCurve( plotItem, on );
}

void DataPlot::showCurve(QwtPlotItem *item, bool on)
{
    item->setVisible(on);

    QwtLegend *lgd = qobject_cast<QwtLegend *>( legend() );

    QList<QWidget *> legendWidgets =
        lgd->legendWidgets( itemToInfo( item ) );

    if ( legendWidgets.size() == 1 )
    {
        QwtLegendLabel *legendLabel =
            qobject_cast<QwtLegendLabel *>( legendWidgets[0] );

        if ( legendLabel )
            legendLabel->setChecked( on );
    }
    if(scrolling==false)
    {
        replot();
    }

}

void DataPlot::addCurve(QPen pen,QString legend)
{
    double* new_dy;
    new_dy=(double*)malloc(PLOT_SIZE*sizeof(double));
    d_y->append(new_dy);

    //  Initialize data
    for (int i = 0; i< PLOT_SIZE; i++) new_dy[i] = 0;

    // Insert new curve
    QwtPlotCurve* new_data;

    new_data = new QwtPlotCurve(legend);
    new_data->attach(this);
    new_data->setPen(pen);
    datas->append(new_data);

    showCurve(new_data, true);

}


void DataPlot::setYAxisScale(float ymin,float ymax)
{
    ymin_orig=ymin;
    ymax_orig=ymax;
    setAxisScale(QwtPlot::yLeft, ymin_orig, ymax_orig);

}

//
//  Set a plain canvas frame and align the scales to it
//
void DataPlot::alignScales()
{
    // The code below shows how to align the scales to
    // the canvas frame, but is also a good example demonstrating
    // why the spreaded API needs polishing.
/*
    canvas()->setFrameStyle(QFrame::Box | QFrame::Plain );
    canvas()->setLineWidth(1);
*/
    for ( int i = 0; i < QwtPlot::axisCnt; i++ )
    {
        QwtScaleWidget *scaleWidget = (QwtScaleWidget *)axisWidget(i);
        if ( scaleWidget )
            scaleWidget->setMargin(0);

        QwtScaleDraw *scaleDraw = (QwtScaleDraw *)axisScaleDraw(i);
        if ( scaleDraw )
            scaleDraw->enableComponent(QwtAbstractScaleDraw::Backbone, false);
    }
}

void DataPlot::Draw(void)
{
    data_index++;
    if(data_index==PLOT_SIZE)
    {
        //printf("a revoir qd on arrive a la fin, il faudrait faire un realloc pour rien perdre\n");
        //attention le setrawdata s'attend a ce que l'adresse change pas, ce qui n'est pas le cas avec lerealloc
        //il faudra refaire un setrawdata ici
        data_index=0;
		min_data_index=0;
        scrolling=true;
    }

    temps+=refresh_rate;
    d_x[data_index]=temps;

    //determine les index pour la visualisation
    if(scrolling==true)
    {
        max_data_index=data_index;
        if(d_x[min_data_index]<temps-view_size)
        {
            while(d_x[min_data_index]<temps-view_size) min_data_index++;
        }
        else
        {
            while(d_x[min_data_index]>temps-view_size && min_data_index!=0) min_data_index--;
        }
    }


    sb->setBase(0,temps);
    if(scrolling==true)
    {
        sb->moveSlider(temps-view_size,temps);

        if(temps<view_size)
        {
            setAxisScale(QwtPlot::xBottom,0, view_size);
        }
        else
        {
            setAxisScale(QwtPlot::xBottom,temps-view_size, temps);
        }
        replot();
    }
    else
    {
        sb->moveSlider(min_scroll,max_scroll);
    }
}

void DataPlot::SetData(double data,int index)
{
    d_y->at(index)[data_index] = data;

    if(scrolling==true)
    {
        datas->at(index)->setRawSamples(&d_x[min_data_index], &d_y->at(index)[min_data_index], max_data_index-min_data_index);
    }

}

void DataPlot::scrollBarMoved(Qt::Orientation o, double min, double max)
{
    min_scroll=min;
    max_scroll=max;

    if(max==sb->maxBaseValue())
    {
        scrolling=true;
    }
    else
    {
        scrolling=false;
        setAxisScale(QwtPlot::xBottom,min,max);

        //determine les index pour la visualisation
        if(d_x[min_data_index]<min)
        {
            while(d_x[min_data_index]<min) min_data_index++;
        }
        else
        {
            while(d_x[min_data_index]>min && min_data_index!=0) min_data_index--;
        }

        if(d_x[max_data_index]<max)
        {
            while(d_x[max_data_index]<max && max_data_index!=data_index) max_data_index++;
        }
        else
        {
            while(d_x[max_data_index]>max) max_data_index--;
        }

        for(int i=0;i<datas->count();i++) datas->at(i)->setRawSamples(&d_x[min_data_index], &d_y->at(i)[min_data_index], max_data_index-min_data_index);
        replot();
    }

}


//context menu
void DataPlot::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {

        QMenu * menu = new QMenu("nom", this);
        // ajout des actions
        QAction *a,*z,*d;

        a=menu->addAction("reset y zoom");
        menu->addSeparator();

        d=menu->addAction(QString("set view size (%1s)").arg(view_size));

        z=menu->exec(event->globalPos());

        if(z==a)
        {
            //zoom to original size
            setAxisScale(QwtPlot::yLeft, ymin_orig, ymax_orig);
            if(scrolling==false) replot();
        }

        if(z==d)
        {
            bool ok;
            float time = QInputDialog::getInt(this, QString("Set view size (%1)").arg(title().text()),tr("Value (s):"), view_size, 1, 65535, 10, &ok);
            if(ok==true)
            {
                view_size=time;
                if(scrolling==false)
                {
                    //4 cas: on utilise le temps au milieu de la vue actuelle

                    //le temps total est plus petit que le view_size, on affiche tout:
                    if(temps<view_size)
                    {
                        scrolling=true;
                    }
                    else
                    {
                        double min=(min_scroll+max_scroll)/2-view_size/2;
                        double max=(min_scroll+max_scroll)/2+view_size/2;
                        //on va du debut jusqu'a view size
                        if(min<0)
                        {
                            min=0;
                            max=view_size;

                        }
                        //on va de fin-viewsize jusqu'a la fin
                        if(max>temps)
                        {
                            min=temps-view_size;
                            max=temps;
                        }
                        sb->moveSlider(min,max);//move slider coupe le signal, on fait aussi le scrollbar
                        scrollBarMoved(Qt::Horizontal,min,max);
                    }

                }
            }
        }

    }
}


