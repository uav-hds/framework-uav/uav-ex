//  created:    2014/04/15
//  filename:   X2Multiplex.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining X2 multiplexing
//
//
/*********************************************************************/

#include "X2Multiplex.h"
#include <cvmatrix.h>
#include <DataPlot1D.h>
#include <Tab.h>
#include <math.h>

using namespace framework::core;
using namespace framework::gui;

namespace framework
{
namespace filter
{

X2Multiplex::X2Multiplex(const FrameworkManager* parent,std::string name) : UavMultiplex(parent,name)
{
    cvmatrix_descriptor* desc=new cvmatrix_descriptor(2,1);
    desc->SetElementName(0,0,"left motor");
    desc->SetElementName(1,0,"right motor");

    output=new cvmatrix(this,desc,floatType);

    AddDataToLog(output);
}

X2Multiplex::~X2Multiplex(void)
{

}

uint8_t X2Multiplex::MotorsCount(void) const
{
    return 2;
}

void X2Multiplex::UseDefaultPlot(void)
{
    Tab *plot_tab=new Tab(GetTabWidget(),"Values");
    plot_left=new DataPlot1D(plot_tab->NewRow(),"motor left",0,1);
    plot_left->AddCurve(output->Element(0));
    plot_right=new DataPlot1D(plot_tab->LastRowLastCol(),"motor right",0,1);
    plot_right->AddCurve(output->Element(1));
}

void X2Multiplex::UpdateFrom(const io_data *data)
{
    float u_roll;
    float u_thrust=0.5;
    float value[2];

    cvmatrix* input=(cvmatrix*)data;

    u_roll=input->Value(0,0);

    //keft
    value[0]=Set(u_thrust+u_roll);

    //(right
    value[1]=Set(u_thrust-u_roll);


    //on prend une fois pour toute le mutex et on fait des accès directs
    output->GetMutex();
    for(int i=0;i<2;i++) output->SetValueNoMutex(i,0,value[i]);
    output->ReleaseMutex();

    output->SetDataTime(data->DataTime());

    ProcessUpdate(output);
}

float X2Multiplex::Set(float u)
{
    float value=0;

    if(u>0)
    {
        value=sqrtf(u);
    }

    return value;
}

} // end namespace filter
} // end namespace framework
