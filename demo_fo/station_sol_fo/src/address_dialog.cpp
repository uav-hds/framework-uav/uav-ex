#include "address_dialog.h"
#include <stdio.h>



address_dialog::address_dialog()
{
    //create dialog box
    setWindowTitle(tr("enter IP address"));
    QGridLayout *main_layout = new QGridLayout();
    ok_button= new QPushButton("Ok");
    add_text=new QLineEdit();
    setLayout(main_layout);

    settings=new QSettings("Heudiasyc", "Station sol");
    settings->beginGroup("remote");
    if(settings->value("ip").toString()=="")
    {
        add_text->setText("192.168.6.1");
    }
    else
    {
        add_text->setText(settings->value("ip").toString());
    }
    settings->endGroup();


    main_layout->addWidget(add_text,0,0);


    main_layout->addWidget(ok_button,1,0);

    connect(ok_button, SIGNAL(clicked(bool)),this, SLOT(ok()));

}

address_dialog::~address_dialog()
{
    delete settings;
}

void address_dialog::ok()
{
    settings->beginGroup("remote");
    settings->setValue("ip",add_text->text());
    settings->endGroup();
    emit address(add_text->text());

}
