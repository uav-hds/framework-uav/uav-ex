#include <stdio.h>
#include <native/timer.h>
#include <fcntl.h>
#include <typedef.h>
#include "structdef.h"
#include "of_thread.h"
#include <socket.h>

#include <unistd.h>
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <dspcv_gpp.h>

#include "kernel/io_hdfile.h"
#include "kernel/err.h"

#define LEVEL_PYR 2
#define NB_POINTS_TO_TRACK 64
#define MAX_POINTS_TO_TRACK 256

#define ARROW_FACTOR 6

using namespace std;

extern struct gains_all gains;
extern bool continuer;
extern udt_socket *sock_cam;
extern bool gains_changed;
extern string machine;

static const float pi = 3.14159265358979323846;

inline static float square(int a)
{

	return a * a;

}

void* main_of(void* argv)
{
    RTIME now, previous;
    unsigned int fps_counter;
    int deplx,deply,nb_depl;
    IplImage* img = 0;
    CvPoint *pointsA =0;
    CvPoint *pointsB =0;
    char *optical_flow_found_feature=0;
    unsigned int count;
    unsigned int *optical_flow_feature_error=0;
    CvSize optical_flow_window = cvSize(3,3);
    CvTermCriteria optical_flow_termination_criteria = cvTermCriteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, .3 );
    float of_x_ant=0;
    float of_y_ant=0;
    struct optical_flow_time of;
    of.of.fps=80;
    int pipe_fd=-1;

    printf("Openning dsp file: %s\n",(char*)argv);
    init_dsp((char*)argv);

    CvCapture* capture = cvCaptureFromCAM(0);//a mettre apres l'init dsp

    pointsA=(CvPoint *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(CvPoint));
    pointsB=(CvPoint *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(CvPoint));
    optical_flow_found_feature=(char *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(char));
    optical_flow_feature_error=(unsigned int *)cvAlloc(MAX_POINTS_TO_TRACK*sizeof(unsigned int));
    // gimg = grey image
    IplImage* gimg0=cvCreateImage(cvSize(320,240),IPL_DEPTH_8U,1);
    IplImage* gimg1=cvCreateImage(cvSize(320,240),IPL_DEPTH_8U,1);
    // pyr = pyramid
    IplImage* pyr0=cvCreateImage(cvSize(320,240),IPL_DEPTH_8U,1);
    IplImage* pyr1=cvCreateImage(cvSize(320,240),IPL_DEPTH_8U,1);
    IplImage* pyr=pyr0;
    IplImage* pyr_old=pyr1;
    IplImage* pyr_tmp;
    IplImage* gimg=gimg0;
    IplImage* gimg_old=gimg1;
    IplImage* gimg_tmp;

    //init pipe
    while(pipe_fd<0)
    {
        pipe_fd = open("/proc/xenomai/registry/native/pipes/of_pipe" , O_WRONLY);
        if (pipe_fd < 0 && errno!=ENOENT) printf("err open write pipe_fd: %i\n",errno);
        usleep(1000);
    }

    //init image old
    if(!cvGrabFrame(capture))
    {
      printf("Could not grab a frame\n");
    }
    img=cvRetrieveRawFrame(capture);

    if(!cvGrabFrame(capture))
    {
      printf("Could not grab a frame\n");
    }
    dspCvtColor(img,gimg_old,DSP_YUYV2GRAY);

    dspPyrDown(gimg_old,pyr_old,LEVEL_PYR);

    previous = rt_timer_read();
    fps_counter=0;

    while(continuer==true)
    {
        if(gains_changed==true)
        {
            cvSetCaptureProperty(capture,CV_CAP_PROP_GAIN,gains.gain);
            cvSetCaptureProperty(capture,CV_CAP_PROP_EXPOSURE,gains.exposure);
            cvSetCaptureProperty(capture,CV_CAP_PROP_BRIGHTNESS,gains.bright);
            cvSetCaptureProperty(capture,CV_CAP_PROP_CONTRAST,gains.contrast);
            cvSetCaptureProperty(capture,CV_CAP_PROP_HUE,gains.hue);
            cvSetCaptureProperty(capture,CV_CAP_PROP_SHARPNESS,gains.sharpness);
            cvSetCaptureProperty(capture,CV_CAP_PROP_AUTOGAIN,gains.autogain);
            cvSetCaptureProperty(capture,CV_CAP_PROP_AWB,gains.awb);

            gains_changed=false;
        }
        if(fps_counter==1)
        {
            //dessine features, inspiré de:
            // http://robots.stanford.edu/cs223b05/notes/optical_flow_demo.cpp
            for(unsigned int i=0;i<count;i++)
            {
                if(optical_flow_found_feature[i]!=0)
                {
                    CvPoint p,q;
                    p.x = pointsA[i].x;
                    p.y = pointsA[i].y;
                    q.x = (int) (pointsA[i].x+(float)pointsB[i].x/256.);
                    q.y = (int) (pointsA[i].y+(float)pointsB[i].y/256.);

                    float angle;
                    angle = atan2( (float) q.y - p.y, (float) q.x - p.x );
                    float hypotenuse;
                    hypotenuse = sqrtf( square(q.y - p.y) + square(q.x - p.x) );

                    // Here we lengthen the arrow by a factor of three
                    q.x = (int) (p.x + ARROW_FACTOR * hypotenuse * cosf(angle));
                    q.y = (int) (p.y + ARROW_FACTOR * hypotenuse * sinf(angle));

                    cvLine( gimg_old, p, q, CV_RGB(0,0,0), 1);//, CV_AA, 0 );

                    p.x = (int) (q.x + ARROW_FACTOR * hypotenuse/2.5 * cosf(angle + 3*pi / 4));
                    p.y = (int) (q.y + ARROW_FACTOR * hypotenuse/2.5 * sinf(angle + 3*pi / 4));
                    cvLine( gimg_old, p, q, CV_RGB(0,0,0), 1);//, CV_AA, 0 );

                    p.x = (int) (q.x + ARROW_FACTOR * hypotenuse/2.5 * cosf(angle - 3*pi / 4));
                    p.y = (int) (q.y + ARROW_FACTOR * hypotenuse/2.5 * sinf(angle - 3*pi / 4));
                    cvLine( gimg_old, p, q, CV_RGB(0,0,0), 1);//, CV_AA, 0 );

                }
            }
            if(sock_cam!=NULL) sock_cam->send_sock((uint8*)(gimg_old->imageData),320*240);
        }

        if(fps_counter==50)
        {
            now = rt_timer_read();
            of.of.fps=fps_counter/((float)(now - previous)/1000000000.);
            fps_counter=0;
            previous=now;
            //printf("%f\n",of.of.fps);
        }

        //select good features
        count=NB_POINTS_TO_TRACK;
        dspGoodFeaturesToTrack(gimg_old,pointsA,&count,gains.quality_level,gains.min_distance);//0.08,5);

        img=cvRetrieveRawFrame(capture);
        if(!cvGrabFrame(capture))
        {
          printf("Could not grab a frame\n");
        }

        dspCvtColor(img,gimg,DSP_YUYV2GRAY);

        //pyramide
        dspPyrDown(gimg,pyr,LEVEL_PYR);

        //lk
        dspCalcOpticalFlowPyrLK(gimg_old,gimg,pyr_old,pyr,pointsA,pointsB,count,optical_flow_window,LEVEL_PYR,optical_flow_found_feature,optical_flow_feature_error,optical_flow_termination_criteria,0) ;

        deplx=0;
        deply=0;
        nb_depl=0;
        for(unsigned int i=0;i<count;i++)
        {
            if(optical_flow_found_feature[i]!=0)
            {
                //the data contained in the x and y is not an integer, but a fixed point data (int part on 8 bits, decimal part on 8 bits)
                deplx+=pointsB[i].x;
                deply+=pointsB[i].y;
                nb_depl++;
            }
        }

        nb_depl=nb_depl<<8; // to rescale from fixed point to int
        if(nb_depl==0)
        {
            //printf("prob\n");
        }
        else
        {
            //on adapte pour se placer dans le repere de la centrale et on fait la moyenne
            of.of.y=(float)deplx/nb_depl;
            of.of.x=(float)deply/nb_depl;

            //xair has a 180 degre rotation of the camera
            if(machine=="xair")
            {
                of.of.x=-of.of.x;
                of.of.y=-of.of.y;
            }

            // we replace the displacement data with lowpass filtered speed data
            of.of.x=(1-2*PI*gains.fc_of/of.of.fps)*of_x_ant+2*PI*gains.fc_of*of.of.x/of.of.fps;
            of.of.y=(1-2*PI*gains.fc_of/of.of.fps)*of_y_ant+2*PI*gains.fc_of*of.of.y/of.of.fps;

            of_x_ant=of.of.x;
            of_y_ant=of.of.y;

            write(pipe_fd,&of,sizeof(struct optical_flow_time));
        }

        //rotation
        pyr_tmp=pyr;
        pyr=pyr_old;
        pyr_old=pyr_tmp;

        gimg_tmp=gimg;
        gimg=gimg_old;
        gimg_old=gimg_tmp;

        fps_counter++;

    }

    cvReleaseCapture(&capture);
    cvReleaseImage(&gimg0);
    cvReleaseImage(&gimg1);
    cvReleaseImage(&pyr0);
    cvReleaseImage(&pyr1);
    cvFree(&pointsA);
    cvFree(&pointsB);
    cvFree(&optical_flow_found_feature);
    cvFree(&optical_flow_feature_error);

    close_dsp();
    close(pipe_fd);

//cette desalloc plante...?
    //cvReleaseImage(&img);

    printf("fin du thread of\n");
    pthread_exit(0);

}


