//  created:    2011/05/01
//  filename:   DemoOpticalFlow.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo optical flow
//
//
/*********************************************************************/

#ifndef DEMOOPTICALFLOW_H
#define DEMOOPTICALFLOW_H

#include <UavStateMachine.h>

namespace framework {
    namespace core {
        class FrameworkManager;
        class cvmatrix;
        class AhrsData;
    }
    namespace gui {
        class GroupBox;
        class DoubleSpinBox;
    }
    namespace filter {
        class OpticalFlow;
        class OpticalFlowSpeed;
        class LowPassFilter;
        class CvtColor;
    }
}

class DemoOpticalFlow : public framework::meta::UavStateMachine {

    public:
        DemoOpticalFlow(framework::meta::Uav* uav);
        ~DemoOpticalFlow();

    protected:
        void SignalEvent(Event_t event);
        void ExtraCheckJoystick(void);
        const framework::core::AhrsData *GetReferenceOrientation(void);

        framework::gui::GroupBox* opticalFlowGroupBox;
        framework::gui::DoubleSpinBox *maxXSpeed,*maxYSpeed;
        framework::core::cvmatrix *opticalFlowReference;
        framework::filter::Pid *u_x, *u_y;
        framework::filter::LowPassFilter* opticalFlowSpeedFiltered;
        framework::filter::CvtColor* greyCameraImage;
        framework::core::AhrsData *customReferenceOrientation;

    private:
        framework::filter::OpticalFlow *opticalFlow;
        framework::filter::OpticalFlowSpeed *opticalFlowSpeed;
};

#endif // DEMOOPTICALFLOW_H
