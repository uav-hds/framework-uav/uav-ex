#ifndef DS3_H
#define DS3_H

#include <QThread>

#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>

class DS3 : public QThread
{
    Q_OBJECT

    struct motion_dev {
      int index;
      bdaddr_t addr;
      char type;
      int csk;
      int isk;
      struct motion_dev *next;
    };

public:
    DS3();
    ~DS3();
    unsigned char BatterieLevel();

private:

    int force_sixaxis;
    int force_ds3;
    struct motion_dev *devs;
    int next_devindex;
    bool is_running;
    unsigned char last_data[49];
    int l2cap_listen(const bdaddr_t *bdaddr, unsigned short psm);
    struct motion_dev *accept_device(int csk, int isk);
    int l2cap_connect(bdaddr_t *ba, unsigned short psm);
    struct motion_dev *connect_device(bdaddr_t *ba);
    void hidp_trans(int csk, char *buf, int len);
    void setup_device(struct motion_dev *dev);
    void parse_report_sixaxis_ds3(unsigned char *r, int len);
    void parse_report(struct motion_dev *dev, unsigned char *r, int len);
    int mystr2ba(const char *s, bdaddr_t *ba);
    char *myba2str(const bdaddr_t *ba);
    void compute_dead_zone(unsigned char *value);
    unsigned char batterie;
    int csk,isk;

signals:
    void emit_log(QString);
    void buttonValueChanged(int button, bool value);
    void axisValueChanged(int value0,int value1,int value2,int value3);

protected:
    void run();

 private slots:
    void rumble(unsigned char value);

};

#endif // DS3_H
