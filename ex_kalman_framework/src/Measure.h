//  created:    2013/06/26
//  filename:   Measure.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant la generation de mesures
//
//
/*********************************************************************/

#ifndef MEASURE_H
#define MEASURE_H

//on met les include nécessaires, ceux dont dérive la classe Sinus
#include <IODevice.h>
#include <Thread.h>
#include <kalman/ekfilter.hpp>

//définition du nombre de measures
#define NTRY 500

//les autres classe peuvent simplement être déclarées (forward declaration)
namespace framework
{
    namespace core
    {
        class FrameworkManager;
        class cvmatrix;
    }
    namespace gui
    {
        class Tab;
        class TabWidget;
        class GridLayout;
        class GroupBox;
        class DoubleSpinBox;
        class SpinBox;
        class DataPlot1D;
    }
}

namespace framework
{
namespace core
{
    class Measure : public IODevice, public Thread
    {
        typedef Kalman::KVector<double, 1, true> Vector;  //!< Vector type.
        typedef Kalman::KMatrix<double, 1, true> Matrix;  //!< Matrix type.

        public://fonctions publiques visibles par tous
            /*!
            * \brief Constructeur
            *
            * Construit un générateur de mesures
            *
            * \param parent le FrameworkManager à utiliser
            * \param name nom de l'objet
            * \param priority priorité du Thread, par défaut 50 (1:mini, 99:maxi)
            */
            Measure(const FrameworkManager* parent,std::string name,int priority=50);

            /*!
            * \brief Déstructeur
            */
            ~Measure();

            /*!
            * \brief Matrice de sortie
            *
            * permet d'accéder à la matrice de sortie;
            * par exemple pour récupérer la valeur du signal (voir aussi la fonction Value),
            * ou pour afficher un graphe (voir aussi la fonction UseDefaultPlot)
            *
            * \return un pointeur vers la matrice de sortie
            */
            cvmatrix *Output(void) const;

            /*!
            * \brief R
            *
            * cette fonction est équivalente à Output()->Value(0,1)
            *
            * \return valeur actuelle du signal
            */
            float R(void) const;


            /*!
            * \brief Theta
            *
            * cette fonction est équivalente à Output()->Value(0,0)
            *
            * \return valeur actuelle du signal
            */
            float Theta(void) const;

            /*!
            * \brief Utilise le graphe par défaut
            *
            * cette fonction placera un graphe du signal dans un onglet Graphe
            *
            */
            void UseDefaultPlot(void);

            /*!
            * \brief SetupLayout
            *
            * cette fonction permet d'ajouter d'autres Widget dans l'onglet du Sinus
            * par exmple si on ajoute un filtre, ses réglages appraitront à la suite des
            * réglages du Sinus
            *
            * \return le GridLayout
            */
            gui::GridLayout* SetupLayout(void) const;

            /*!
            * \brief PlotR
            *
            * cette fonction permet d'ajouter d'autres courbes dans le graphe par défaut
            * par exmple si on ajoute un filtre, on pourra avoir sur le même graphe la valeur
            * du signal et sa valeur filtrée
            *
            * \return le DataPlot1D
            */
            gui::DataPlot1D* PlotR(void) const;

            /*!
            * \brief PlotTheta
            *
            * cette fonction permet d'ajouter d'autres courbes dans le graphe par défaut
            * par exmple si on ajoute un filtre, on pourra avoir sur le même graphe la valeur
            * du signal et sa valeur filtrée
            *
            * \return le DataPlot1D
            */
            gui::DataPlot1D* PlotTheta(void) const;

        private://fonctions et variables privées, utilisables uniquement par cette classe

            //fonction UpdateFrom de la classe IODevice
            //measure s'apparente à un capteur, cette fonction n'est donc pas implémentée ici
            //en effet measure n'a pas d'entrée, mais seulement une sortie
            void UpdateFrom(const io_data *data){};

            //fonction Run de la classe Thread, c'est la boucle de traitement
            void Run(void);

            //variables:
            bool is_started;//pour savoir si le thread est en fonctionnement
            cvmatrix *output;//matrice de sortie
            gui::Tab* main_tab;//onglet principal
            gui::TabWidget* tabwidget;//conteneur d'onglets secondaires
            gui::DataPlot1D *plot_theta,*plot_r;//graphes
            gui::SpinBox *period;//réglage du Thread
            gui::GridLayout* setup_layout;//GridLayout pour ajouter des filtres par exemple
            int index;
            Kalman::KMatrix<double, 1, true> *measure;
    };
}//end namespace core
}//end namespace framework
#endif // MEASURE_H
