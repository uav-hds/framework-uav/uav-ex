#include "main_thread.h"
#include "trames.h"
#include "structdef.h"
#include "traj_gen.h"
#include <io.h>
#include <3dmgx3.h>
#include "socket.h"
#include <dbt.h>
#include <xbee.h>

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <string>
#include <fstream>
#include <stdarg.h>
#include <math.h>

#include "kernel/io_hdfile.h"
#include "kernel/err.h"

#include <native/task.h>
#include <native/timer.h>


//configuration X8
#define HAVG 4
#define HAVD 3
#define HARG 0
#define HARD 5
#define BAVG 7
#define BAVD 2
#define BARG 1
#define BARD 6
#define PAS_HAVG 1

//test moteur
//ne fonctionne pas avec les drivers xair
//-> il faut un certaint temps a 0 pour que les drivers accrochent?
#define TEST_MOTOR 0
#define TESTED_MOTOR BARD
#define TEST_VALUE 100

//navigation states (for navigation_state variable)
#define MANUAL 0
#define OF 1
#define ARRET 255

//altitude states (for z_state variable)
#define Z_STAB 1
#define AUGMENTATION_GAZ_AUTO 2
#define Z_ASSERV 3
#define AUGMENTATION_GAZ_MANUAL 4
#define ATTERRISSAGE 5
#define DECOLLAGE 6

//periode echantillonage en ms -> a passer dans les gains
//if the control loop frequency is 100Hz, then TAU is 10ms
#define TAU 10

//gains pour les trims plus il est grand plus les trims sont fins
#define TRIM_GAIN 4

using namespace std;


/*
** ===================================================================
**     variables globales
**
** ===================================================================
*/

//parametres loi de commande
//mettre des valeurs par defaut dans la fonction read flash??
//(zeros dans tous les octets de la structure)
//sauf pour les mask et divider!!
struct gains_all gains;
float pas_gaz,pas_roll,pas_pitch,pas_yaw;

//moteurs
struct moteurs_8bits pwm_8;
struct moteurs_16bits pwm_16;
int nb_mot;

//imu
extern gx3* imu; // orientation angles follow north, east, down scheme
struct imu_datas orientation; // oriented imu information (in case imu is not facing north)
struct angles orientation_cons;

//joystick
struct control joy_data;
struct control_trim joy_trim;

//loi de commande
uint8 z_state=ARRET;
uint8 navigation_state=ARRET;

//divers
uint8 nb_loop=0;

//altitude
struct altitude alt_raw; // raw value from sensors (ultra sound)
struct altitude alt_fil; // filtered value
struct altitude alt_cons; // target value
trajectory_generator* alt_ref;

//of
struct optical_flow of,position;
extern RT_PIPE of_pipe;
extern bool gains_changed;

extern bool continuer;

extern dbt *us_dbtFile,*imu_dbtFile,*states_dbtFile,*of_dbtFile;

//socket
extern udt_socket *sock;
extern xbee* modem;

typedef struct tolog{
    float uroll;
    float upitch;
    float uyaw;
    float ugaz;
} tolog;

// low level attitude control
void main_thread(void * arg)
{
	rt_task_set_periodic(NULL, TM_NOW, TAU* 1000000);

	continuer=true;

    //debattement joystick
    pas_gaz=(float)gains.deb_gaz/65535; //consigne force
    pas_roll=(float)gains.deb_roll*PI/((float)180.0*32767); //consignes angles
    pas_pitch=(float)gains.deb_pitch*PI/((float)180.0*32767);
    pas_yaw=(float)gains.deb_yaw*PI/((float)180.0*32767);

    //trims
    joy_trim.trim_roll=0;
    joy_trim.trim_pitch=0;
    joy_trim.trim_gaz=0;


    // init the dbt file
    us_dbtFile = NULL;
    imu_dbtFile = NULL;
    states_dbtFile = NULL;
    of_dbtFile=NULL;

    init_i2c((char*)arg);

    nb_mot=nb_motors();
    if(nb_mot==4)
    {
        printf("configuration X4\n");
    }
    else
    if(nb_mot==8)
    {
        printf("configuration modulo-X\n");
    }
    else
    {
        printf("erreur nb moteurs %i\n",nb_mot);
        continuer=false;
    }
    alt_ref=new trajectory_generator();

    //us
    //range 46*43+43=2021mm max (2m)
    //7:gain=118
    init_us(46,7);
    start_capture_us();

    // When the drone lies on the ground, the z sensor (ultra sound) is 8cm above the ground
    // (reasonable guess but obviously depends on the exact drone model)
    alt_fil.z=0.08;

  	while(continuer==true)
  	{
		rt_task_wait_period(NULL);
		control_loop();
  	}

    if(sock!=NULL) sock->debug("fin du thread controle");
    delete alt_ref;
    close_i2c();

  	printf("fin du thread controle\n");
}



void control_loop(void)
{
    uint8 i;
    int32 size=1; // size of incoming data
    uint8 buf[256];
    float z_tmp;

    float pwm_float[8];

    float roll,pitch,yaw;
    float vx_cons,vy_cons;
    float u_roll,u_pitch,u_yaw; // correction applied
    static float i_roll=0,i_pitch=0,i_yaw=0;
    static float i_roll_of=0,i_pitch_of=0;
    float rot1,rot2;

    float gaz;
    static float i_gaz=0;
    static float offset_gaz=0; // gas throttle value needed for the drone to lift itself

    struct extract_result result;
    static uint8 loop_cpt=0;
    struct tolog log;

    struct optical_flow_time of_tmp;

/*
** ===================================================================
**     gestion des messages sur socket
**
** ===================================================================
*/

    while(size>0)
    {
        if(sock!=NULL)
        {
            size=sock->read_sock(buf,256);
        }
        else
        {
            size=modem->read((char*)buf,256);
        }
        result.api_id=NO_TRAME_AVAILABLE;
        if(size>0) result=extract_trame_sci(buf,size);
        switch(result.api_id)
        {
            case NO_TRAME_AVAILABLE:
                break;
            case WRONG_CHECKSUM:
                if(sock!=NULL) sock->debug("recv socket checksum wrong");
                break;
            case RX_PACKET:
                if(result.trame_id==FLAG_KILL)
                {
                    if(navigation_state==ARRET)
                    {
                        continuer=false;
                    }
                    else
                        if(sock!=NULL) sock->debug("kill impossible en vol");
                }
                if(result.trame_id==ID_TRAME_JOYSTICK_TRIM_ROLL || result.trame_id==ID_TRAME_JOYSTICK_TRIM_PITCH || result.trame_id==ID_TRAME_JOYSTICK_TRIM_GAZ)
                {
                    if(sock!=NULL)
                    {
                        buf[0]=ID_TRAME_JOYSTICK_TRIM;
                        memcpy(&buf[1],(char*)(&joy_trim),sizeof(control_trim));
                        sock->send_sock(buf,sizeof(control_trim)+1);
                    }
                    if(modem!=NULL) modem->write((char*)&joy_trim,sizeof(control_trim),ID_TRAME_JOYSTICK_TRIM);
                }

                if(result.trame_id==FLAG_SEND_GAIN)
                {
                    if(sock!=NULL)
                    {
                        buf[0]=ID_TRAME_GAINS;
                        memcpy(&buf[1],(char*)(&gains),sizeof(gains_all));
                        sock->send_sock(buf,sizeof(gains_all)+1);
                    }
                    if(modem!=NULL) modem->write((char*)&gains,sizeof(gains_all),ID_TRAME_GAINS);
                }

                if(result.trame_id==FLAG_START || result.trame_id==FLAG_DECOLLAGE)
                {
                    if(navigation_state==ARRET)
                    {
                        // create the dbt files
                        us_dbtFile = new dbt("us_dbt",TRAME_US,sizeof(altitude));
                        imu_dbtFile =  new dbt("imu_dbt",TRAME_IMU,sizeof(imu_datas));
                        states_dbtFile =  new dbt("states_dbt",TRAME_STATE,sizeof(tolog));
                        of_dbtFile =  new dbt("of_dbt",TRAME_CAM,sizeof(optical_flow));

                        if(result.trame_id==FLAG_START)
                        {
                            z_state=AUGMENTATION_GAZ_MANUAL;
                            navigation_state=MANUAL;
                            if(sock!=NULL) sock->debug("z state: start PWM");
                        }
                        if(result.trame_id==FLAG_DECOLLAGE)
                        {
                            z_state=AUGMENTATION_GAZ_AUTO;
                            navigation_state=MANUAL;
                            alt_cons.z=alt_fil.z;
                            if(sock!=NULL) sock->debug("z state: start PWM decollage auto");
                        }
                    }
                    else
                        if(sock!=NULL) sock->debug("deja en marche");
                }

                if(result.trame_id==FLAG_ATTERRISSAGE)
                {
                    if(navigation_state!=ARRET)
                    {
                        alt_ref->stop();
                        alt_ref->start(alt_fil.z,0,gains.vel_max_dec,gains.acc_dec);
                        z_state=ATTERRISSAGE;
                        if(sock!=NULL) sock->debug("z state: atterrissage");
                    }
                    else
                        if(sock!=NULL) sock->debug("deja a l'arret");
                }

                if(result.trame_id==FLAG_STOP)
                {
                    if(navigation_state!=ARRET)
                        stop_moteurs();
                    else
                        if(sock!=NULL) sock->debug("deja a l'arret");
                }

                if(result.trame_id==FLAG_START_Z)
                {
                    if(z_state!=MANUAL || navigation_state==ARRET)
                    {
                        if(sock!=NULL) sock->debug("deja en mode z stab");
                    }
                    else
                    {
                        alt_cons.z=alt_fil.z;
                        //alt_cons.vz=0;
                        z_state=Z_STAB;
                        if(sock!=NULL) sock->debug("z state: z stab");
                    }
                }

                if(result.trame_id==FLAG_STOP_Z)
                {
                    if(z_state!=Z_STAB || navigation_state==ARRET)
                    {
                        if(sock!=NULL) sock->debug("pas en mode z stab");
                    }
                    else
                    {
                        alt_cons.z=0;
                        //alt_cons.vz=0;
                        z_state=MANUAL;
                        if(sock!=NULL) sock->debug("z state: z manuel");
                    }
                }

                if(result.trame_id==FLAG_WRITE_GAIN)
                {
                    if(write_gains_memory()==0)
                        if(sock!=NULL) sock->debug("erreur ecriture fichier");
                }

                if(result.trame_id==FLAG_START_OF)
                {
                    if(navigation_state==MANUAL)
                    {
                        navigation_state=OF;
                        if(sock!=NULL) sock->debug("passage en mode flux optique");
                    }
                    else
                        if(sock!=NULL) sock->debug("mode flux optique pas possible");
                }

                if(result.trame_id==FLAG_STOP_OF)
                {
                    if(navigation_state==OF)
                    {
                        navigation_state=MANUAL;
                        if(sock!=NULL) sock->debug("retour en mode normal");
                    }
                    else
                        if(sock!=NULL) sock->debug("pas en mode flux optique");
                }

                if(result.trame_id==ID_TRAME_GAINS)
                {
                    pas_gaz=(float)gains.deb_gaz/65535; //consigne force
                    pas_roll=(float)gains.deb_roll*PI/((float)180.0*32767); //consignes angles
                    pas_pitch=(float)gains.deb_pitch*PI/((float)180.0*32767);
                    pas_yaw=(float)gains.deb_yaw*PI/((float)180.0*32767);
                    nb_loop=0;
                    gains_changed=true;
                }
                break;
            default:
                if(sock!=NULL) sock->debug("trame inconnue");
        }
    }

/*
** ===================================================================
**     lecture centrale
**
** ===================================================================
*/

    //hack modulo x
    if(nb_mot==8)
    {
        orientation.roll=imu->pitch();
        orientation.pitch=-imu->roll();
        orientation.yaw=imu->yaw();
        orientation.droll=imu->dpitch();
        orientation.dpitch=-imu->droll();
        orientation.dyaw=imu->dyaw();
    }
    else
    {
        orientation.roll=imu->roll();
        orientation.pitch=imu->pitch();
        orientation.yaw=imu->yaw();
        orientation.droll=imu->droll();
        orientation.dpitch=imu->dpitch();
        orientation.dyaw=imu->dyaw();
    }

    roll=orientation.roll+gains.offset_roll;
    pitch=orientation.pitch+gains.offset_pitch;
    yaw=orientation.yaw;

    if(imu_dbtFile!=NULL)
    {
        imu_dbtFile->write((char*)(&orientation),imu->time());
    }

/*
** ===================================================================
**     lecture ultra son
**
** ===================================================================
*/

    // the control loops is running at 100Hz but ultrasound acquisition maximum frequency is 50Hz.
    // AAC
    loop_cpt++;
    if(loop_cpt>=2)
    {

        z_tmp=get_value_us();

        if(z_tmp==-1)
        {
            if(sock!=NULL) sock->debug("erreur ecriture I2C US");
        }
        else
        {
            start_capture_us();
            // if altitude variation is more than 5 m/s we discard the value which is an outlier
            if(fabs(z_tmp-alt_fil.z)/(loop_cpt*TAU*0.001)>5)
            {
                //printf("avant %f %f %f %f\n",z_tmp,alt_raw.z,alt_raw.vz,alt_fil.vz);
                z_tmp=alt_fil.z;
            }

            // first order lowpass filter
            alt_fil.z=(1-2*PI*gains.fc_alt*loop_cpt*TAU*0.001)*alt_fil.z+2*PI*gains.fc_alt*loop_cpt*TAU*0.001*z_tmp;
            //alt_raw.vz=(z_tmp-alt_raw.z)/(loop_cpt*TAU*0.001);
            alt_raw.vz=(alt_fil.z-alt_raw.z)/(loop_cpt*TAU*0.001);

            alt_raw.z=alt_fil.z;

            //alt_fil.z=alt_raw.z;

            alt_fil.vz=(1-2*PI*gains.fc_alt*loop_cpt*TAU*0.001)*alt_fil.vz+2*PI*gains.fc_alt*loop_cpt*TAU*0.001*alt_raw.vz;
            if(alt_fil.vz>1 || alt_fil.vz<-1)
            {
                //printf("apres %f %f %f %f\n",z_tmp,alt_raw.z,alt_raw.vz,alt_fil.vz);
            }
            if(us_dbtFile!=NULL)
            {
                us_dbtFile->write((char*)(&alt_fil),rt_timer_read());
            }


        }
        loop_cpt=0;
    }

/*
** ===================================================================
**     lecture flux optique
**
** ===================================================================
*/

    int nb_read=rt_pipe_read(&of_pipe,&of_tmp,sizeof(struct optical_flow_time),TM_NONBLOCK);
    if(nb_read==sizeof(struct optical_flow_time))
    {
        memcpy(&of,&of_tmp,sizeof(struct optical_flow));//copie dans la struct qui n'a pas le temps
        //scaling
        of.y=alt_fil.z*of.y;
        of.x=alt_fil.z*of.x;

        //uncomment this to stick to m/s
        //of.x    = of.x*100/270.87;
        //of.y    = of.y*100/271.76;

        if(of_dbtFile!=NULL)
        {
            of_dbtFile->write((char*)(&of),of_tmp.time);
        }
    }

/*
** ===================================================================
**     securite angles
**
** ===================================================================
*/

    if((roll>PI/3 || roll<-PI/3 || pitch>PI/3 || pitch<-PI/3) && navigation_state!=ARRET)
    {
      stop_moteurs();
  	  if(sock!=NULL) sock->debug("erreur angles");
    }

/*
** ===================================================================
**     calculs de navigation
**
** ===================================================================
*/
    //reference yaw
    if(navigation_state==ARRET)
    {
      orientation_cons.yaw=yaw;
    }

	//rot1 et rot2 sont les angles de rotation en valeur absolue
	//rot1: rotation +
	//rot2: rotation -
	if(orientation_cons.yaw > yaw )
	{
		rot1=orientation_cons.yaw-yaw;
		rot2=2*PI-orientation_cons.yaw+yaw;
	} else
	{
		rot1=2*PI+orientation_cons.yaw-yaw;
		rot2=yaw-orientation_cons.yaw;
	}
	//from now on, rot1 is the final signed value
	if(rot2<rot1) rot1=-rot2;
	rot1=-rot1;//pour avoir rot1=yaw-orientation_cons.yaw


/*
** ===================================================================
**     stabilisation altitude
**		certains changements d'etats se font a la reception
**		des trames
**
** ===================================================================
*/
    alt_ref->Update(rt_timer_read());
    switch(z_state)
	{
        case ATTERRISSAGE://diminution de l'altitude de consigne
            if(i_gaz==gains.k4)
			{
			  //stop_moteurs();
			}
			// we use the target altitude to be independent of the drone geometry
			// Indeed the real altitude will never reach 0 since the sensor is a little bit above the ground.
			// Here we consider that the real altitude follow closely the target so when the target altitude
			// reaches zero the drone is already on the ground
			if(alt_cons.z==0) stop_moteurs();
			//pas de break ici, on continue sur le z stab
		case Z_STAB:
            alt_cons.z=alt_ref->Position();
            alt_cons.vz=alt_ref->Speed();
            //rt_printf("%f %f\n",alt_cons.z,alt_cons.vz);
			i_gaz+=(alt_fil.z-alt_cons.z);
			if(i_gaz>gains.k4) i_gaz=gains.k4;
			if(i_gaz<-gains.k4) i_gaz=-gains.k4;
			gaz=offset_gaz-gains.k1*(alt_fil.z-alt_cons.z)-gains.k2*i_gaz-gains.k3*(alt_fil.vz-alt_cons.vz);
			break;
		case AUGMENTATION_GAZ_AUTO://augmentation des gaz pour décollage auto
			if(alt_fil.z<(alt_cons.z+0.03))//on attend d'avoir décollé de 3cm avant d'augmenter la consigne
			{
                offset_gaz+=gains.pas_decollage*TAU*0.001;      //auto-calibration (auto-détection) de m*g
                gaz=offset_gaz;
			}
			else
			{
                z_state=Z_STAB;//après avoir détecté m*g
                alt_ref->start(alt_fil.z,gains.alt_dec,gains.vel_max_dec,gains.acc_dec);
                if(sock!=NULL) sock->debug("z state: z asserv\n");
                i_gaz=0;
                alt_cons.z=alt_fil.z;
			}
			break;
	}

	log.ugaz=gaz;


/*
** ===================================================================
**     lois de commande
**
** ===================================================================
*/

	switch(navigation_state)
	{
		case ARRET:
            //la consigne colle bêtement à l'état courant pour éviter une discontinuité au redémarrage
			i_roll=0;
			i_pitch=0;
			i_yaw=0;
			i_roll_of=0;
			i_pitch_of=0;
			offset_gaz=gains.seuil_vitesse;
			orientation_cons.pitch=-joy_data.pitch*pas_pitch;
			orientation_cons.roll=-joy_data.roll*pas_roll;
			orientation_cons.yaw+=joy_data.yaw*pas_yaw*TAU*0.001;
			if(orientation_cons.yaw<-PI) orientation_cons.yaw+=2*PI;
			if(orientation_cons.yaw>PI) orientation_cons.yaw-=2*PI;
			break;
		case MANUAL:
            //qd on arrete le flux optique on repasse par le mode normal a mettre dans le chg d'etat? (lecture trame)
            i_roll_of=0;
			i_pitch_of=0;

			orientation_cons.pitch=-joy_data.pitch*pas_pitch;
			orientation_cons.roll=-joy_data.roll*pas_roll;
			orientation_cons.yaw+=joy_data.yaw*pas_yaw*TAU*0.001;
			if(orientation_cons.yaw<-PI) orientation_cons.yaw+=2*PI;
			if(orientation_cons.yaw>PI) orientation_cons.yaw-=2*PI;

			i_roll+=roll-orientation_cons.roll;
			i_pitch+=pitch-orientation_cons.pitch;
			i_yaw+=rot1;

			if((int16)i_roll>gains.int_max_angles) i_roll=gains.int_max_angles;
			if(i_roll<-gains.int_max_angles) i_roll=(float)(-gains.int_max_angles);
			if((int16)i_pitch>gains.int_max_angles) i_pitch=gains.int_max_angles;
			if(i_pitch<-gains.int_max_angles) i_pitch=(float)(-gains.int_max_angles);
			if((int16)i_yaw>gains.int_max_angles) i_yaw=gains.int_max_angles;
			if(i_yaw<-gains.int_max_angles) i_yaw=(float)(-gains.int_max_angles);

			u_roll=-orientation.droll*gains.kd_roll-(roll-orientation_cons.roll)*gains.kp_roll-i_roll*gains.ki_roll;
			u_pitch=-orientation.dpitch*gains.kd_pitch-(pitch-orientation_cons.pitch)*gains.kp_pitch-i_pitch*gains.ki_pitch;
			u_yaw=-orientation.dyaw*gains.kd_yaw-rot1*gains.kp_yaw-i_yaw*gains.ki_yaw;

			//rajouter saturation sur le uyaw
			break;

	  case OF:
			orientation_cons.pitch=0;
			orientation_cons.roll=0;
			vx_cons=-joy_data.pitch*(float)gains.deb_joy_of/32767;
			vy_cons=-joy_data.roll*(float)gains.deb_joy_of/32767;

			orientation_cons.yaw+=joy_data.yaw*pas_yaw*TAU*0.001;
			if(orientation_cons.yaw<-PI) orientation_cons.yaw+=2*PI;
			if(orientation_cons.yaw>PI) orientation_cons.yaw-=2*PI;

			i_roll+=roll-orientation_cons.roll;
			i_pitch+=pitch-orientation_cons.pitch;
			i_yaw+=rot1;

			if((int16)i_roll>gains.int_max_angles) i_roll=gains.int_max_angles;
			if(i_roll<-gains.int_max_angles) i_roll=(float)(-gains.int_max_angles);
			if((int16)i_pitch>gains.int_max_angles) i_pitch=gains.int_max_angles;
			if(i_pitch<-gains.int_max_angles) i_pitch=(float)(-gains.int_max_angles);
			if((int16)i_yaw>gains.int_max_angles) i_yaw=gains.int_max_angles;
			if(i_yaw<-gains.int_max_angles) i_yaw=(float)(-gains.int_max_angles);

            i_roll_of+=of.y-vy_cons;
            i_pitch_of+=of.x-vx_cons;

			if((int16)i_roll_of>gains.int_max_fo) i_roll_of=gains.int_max_fo;
			if(i_roll_of<-gains.int_max_fo) i_roll_of=(float)(-gains.int_max_fo);
			if((int16)i_pitch_of>gains.int_max_fo) i_pitch_of=gains.int_max_fo;
			if(i_pitch_of<-gains.int_max_fo) i_pitch_of=(float)(-gains.int_max_fo);

			u_roll=-orientation.droll*gains.kd_roll-(roll-orientation_cons.roll)*gains.kp_roll-i_roll*gains.ki_roll-(of.y-vy_cons)*gains.kd_of_y-i_roll_of*gains.ki_of_y;
			u_pitch=-orientation.dpitch*gains.kd_pitch-(pitch-orientation_cons.pitch)*gains.kp_pitch-i_pitch*gains.ki_pitch-(of.x-vx_cons)*gains.kd_of_x-i_pitch_of*gains.ki_of_x;
			u_yaw=-orientation.dyaw*gains.kd_yaw-rot1*gains.kp_yaw-i_yaw*gains.ki_yaw;
            break;

	}



/*
** ===================================================================
**     moteurs
**      repartition des entrees de commande
**
** ===================================================================
*/
    gaz=(gaz*gaz)/(cosf(roll)*cosf(pitch));//a changer ou mettre ailleurs? voir si les cos doivent pas etre au carre

    if(nb_mot==4)
    {
        //avant gauche
        pwm_float[0]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN;
        if(gaz-u_pitch-u_roll+u_yaw>0)
        {
          pwm_float[0]+=sqrtf(gaz-u_pitch-u_roll+u_yaw);
        }

        //arriere gauche
        if(gaz+u_pitch-u_roll-u_yaw>0)
        {
          pwm_float[2]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz+u_pitch-u_roll-u_yaw);
        }else
        {
          pwm_float[2]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //arriere droit
        if(gaz+u_pitch+u_roll+u_yaw>0)
        {
          pwm_float[1]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz+u_pitch+u_roll+u_yaw);
        }else
        {
          pwm_float[1]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //avant droit
        if(gaz-u_pitch+u_roll-u_yaw>0)
        {
          pwm_float[3]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz-u_pitch+u_roll-u_yaw);
        }else
        {
          pwm_float[3]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN;
        }
    }
    else if(nb_mot==8)
    {
        u_yaw=PAS_HAVG*u_yaw;
        //haut avant gauche
        if(gaz-u_pitch-u_roll+u_yaw>0)
        {
          pwm_float[HAVG]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz-u_pitch-u_roll+u_yaw);
        }else
        {
          pwm_float[HAVG]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN;
        }
        //bas avant gauche
        if(gaz-u_pitch-u_roll-u_yaw>0)
        {
          pwm_float[BAVG]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz-u_pitch-u_roll-u_yaw);
        }else
        {
          pwm_float[BAVG]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //haut arriere gauche
        if(gaz+u_pitch-u_roll-u_yaw>0)
        {
          pwm_float[HARG]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz+u_pitch-u_roll-u_yaw);
        }else
        {
          pwm_float[HARG]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //bas arriere gauche
        if(gaz+u_pitch-u_roll+u_yaw>0)
        {
          pwm_float[BARG]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz+u_pitch-u_roll+u_yaw);
        }else
        {
          pwm_float[BARG]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN-(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //haut arriere droit
        if(gaz+u_pitch+u_roll+u_yaw>0)
        {
          pwm_float[HARD]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz+u_pitch+u_roll+u_yaw);
        }else
        {
          pwm_float[HARD]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //bas arriere droit
        if(gaz+u_pitch+u_roll-u_yaw>0)
        {
          pwm_float[BARD]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz+u_pitch+u_roll-u_yaw);
        }else
        {
          pwm_float[BARD]=(float)joy_trim.trim_gaz/8+(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //haut avant droit
        if(gaz-u_pitch+u_roll-u_yaw>0)
        {
          pwm_float[HAVD]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz-u_pitch+u_roll-u_yaw);
        }else
        {
          pwm_float[HAVD]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN;
        }

        //bas avant droit
        if(gaz-u_pitch+u_roll+u_yaw>0)
        {
          pwm_float[BAVD]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN+sqrtf(gaz-u_pitch+u_roll+u_yaw);
        }else
        {
          pwm_float[BAVD]=(float)joy_trim.trim_gaz/8-(float)joy_trim.trim_pitch/TRIM_GAIN+(float)joy_trim.trim_roll/TRIM_GAIN;
        }
    }
    else
    {
        printf("Probleme nb moteurs %i\n",nb_mot);
    }
    //pour envoie compatible ssol: a revoir pour la gestion 4/8 moteurs sur station sol
    for(i=0;i<4;i++) pwm_8.moteur[i]=(uint8)sat_pwm(pwm_float[i],gains.pwm_low,gains.pwm_high);
    //blctrlv2, on multiplie tout par 8
    for(i=0;i<nb_mot;i++) pwm_16.moteur[i]=sat_pwm(8*pwm_float[i],8*gains.pwm_low,8*(gains.pwm_high+1)-1);

    switch(z_state)
    {
      case ARRET:
        stop_pwms();
        for(i=0;i<nb_mot;i++) pwm_16.moteur[i]=0;
        if(TEST_MOTOR==1) pwm_16.moteur[TESTED_MOTOR]=TEST_VALUE;
        set_motors(pwm_16.moteur,nb_mot);
        break;
      default:
        set_motors(pwm_16.moteur,nb_mot);
        break;

    }

/*
** ===================================================================
**     envoi des logs et consignes
**
** ===================================================================
*/
	nb_loop++;
	if(nb_loop==gains.log_divider)
	{
		nb_loop=0;
		send_logs();
	}

	if(states_dbtFile!=NULL)
    {

        log.uroll=u_roll;
        log.upitch=u_pitch;
        log.uyaw=u_yaw;

        states_dbtFile->write((char*)(&log),rt_timer_read());
    }

}


/*
** ===================================================================
**     fonctions de saturation
**
** ===================================================================
*/


uint16 sat_pwm(float vel_cons,uint16 min,uint16 max)
{
  uint16 sat_value=(uint16)vel_cons;

  if(vel_cons>((float)sat_value+0.5)) sat_value++;

  if(vel_cons<(float)min) sat_value=min;
  if(vel_cons>(float)max) sat_value=max;

  return sat_value;
}


/*
** ===================================================================
**     fonction d'ecriture des gains en memoire persitante
**
** ===================================================================
*/

uint8 write_gains_memory(void)
{
    ofstream fichier("gains_fo.hex", ios::out | ios::trunc | ios::binary);
    if(fichier.fail()==false)
    {
        fichier.write((char*)&gains, sizeof(gains_all));
        fichier.close();
        return 1;
    }
    else
    {
        fichier.close();
        return 0;
    }
}

/*
** ===================================================================
**     fonction de lecture des gains dans la memoire
**
** ===================================================================
*/

uint8 read_gains_memory(void)
{
    ifstream fichier("gains_fo.hex", ios::in | ios::binary);
    if(fichier.fail()==false)
    {
        fichier.read((char*)&gains, sizeof(gains_all));
        fichier.close();
        return 1;
    }
    else
    {
        fichier.close();
        return 0;
    }
}


/*
** ===================================================================
**     stop des moteurs
**
** ===================================================================
*/
void stop_moteurs(void)
{
	if(sock!=NULL) sock->debug("stop moteurs");

	stop_pwms();
    for(int i=0;i<nb_mot;i++) pwm_16.moteur[i]=0;
    if ((nb_mot == 4) || (nb_mot == 8))
    {
      set_motors(pwm_16.moteur,nb_mot);
    }

	//close dbt files
	delete us_dbtFile;
	delete imu_dbtFile;
	delete states_dbtFile;
	delete of_dbtFile;
	us_dbtFile=NULL;
	imu_dbtFile=NULL;
	states_dbtFile=NULL;
	of_dbtFile=NULL;

	z_state=ARRET;
	if(sock!=NULL) sock->debug("z state: arret");
	navigation_state=ARRET;
	alt_cons.z=0;
	alt_cons.vz=0;

}


/*
** ===================================================================
**     fonction d'envoi des logs
**
** ===================================================================
*/
void send_logs(void)
{
	uint8 buf[256];
	static uint16 log_time=0;
	int32 size=calc_long_log();

	uint8* ptr=&buf[1];

    memcpy(ptr,(&joy_data),sizeof(control));
    ptr+=sizeof(control);

    memcpy(ptr,(&orientation),sizeof(imu_datas));
    ptr+=sizeof(imu_datas);

    memcpy(ptr,(&orientation_cons),sizeof(angles));
    ptr+=sizeof(angles);

    memcpy(ptr,(&pwm_8),sizeof(moteurs_8bits));
    ptr+=sizeof(moteurs_8bits);

    memcpy(ptr,(&alt_fil),sizeof(altitude));
    ptr+=sizeof(altitude);

    memcpy(ptr,(&alt_cons),sizeof(altitude));
    ptr+=sizeof(altitude);

    memcpy(ptr,(&of),sizeof(optical_flow));
    ptr+=sizeof(optical_flow);


    memcpy(ptr,(uint8*)(&log_time),sizeof(uint16));

    log_time++;
    buf[0]=ID_TRAME_LOG;
    if(sock!=NULL) sock->send_sock(buf,size+1);
    if(modem!=NULL) modem->write((char*)&buf[1],size,ID_TRAME_LOG);

}
