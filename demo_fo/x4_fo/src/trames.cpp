#include "trames.h"
#include "structdef.h"
#include <cstring>
#include <stdio.h>

extern struct gains_all gains;
extern struct control joy_data;
extern struct control_trim joy_trim;
extern struct imu_datas orientation;
extern struct angles orientation_cons;
extern struct moteurs_8bits pwm_8;
extern struct altitude alt_fil,alt_cons;
extern struct optical_flow of;


/*
** ===================================================================
**     fonction d'extration de trames dans les buffer rx
**		tester si le buffer est vide avant de l'appeller
**		une seule trame est extraite par appel
**
** ===================================================================
*/
struct extract_result extract_trame_sci(uint8* buf,int32 size)
{

    int32 i;
    uint8 checksum;
    int32 pt_rd=1;//se place sur la premiere donnee
    struct extract_result result;
    signed char value;

    //calcul du checksum
    checksum=0;
    for(i=0;i<size;i++) checksum=(uint8)((checksum+buf[i])& 0xff);

    if(checksum!=255)
    {
        result.api_id=WRONG_CHECKSUM;
        return result;
    }

    result.api_id=RX_PACKET;
    result.trame_id=buf[0];

    switch(result.trame_id)
    {
        case ID_TRAME_LOG:
            extract_buf((uint8*)(&joy_data),sizeof(control),buf,&pt_rd);
            extract_buf((uint8*)(&orientation),sizeof(imu_datas),buf,&pt_rd);
            extract_buf((uint8*)(&orientation_cons),sizeof(angles),buf,&pt_rd);
            extract_buf((uint8*)(&pwm_8),sizeof(moteurs_8bits),buf,&pt_rd);
            extract_buf((uint8*)(&alt_fil),sizeof(altitude),buf,&pt_rd);
            extract_buf((uint8*)(&alt_cons),sizeof(altitude),buf,&pt_rd);
            extract_buf((uint8*)(&of),sizeof(optical_flow),buf,&pt_rd);

            //données envoyées tout le temps
            extract_buf((uint8*)(&result.log_time),sizeof(uint16),buf,&pt_rd);
            break;
        case ID_TRAME_JOYSTICK:
            extract_buf((uint8*)(&joy_data),sizeof(control),buf,&pt_rd);
            break;
        case ID_TRAME_JOYSTICK_TRIM:
            extract_buf((uint8*)(&joy_trim),sizeof(control_trim),buf,&pt_rd);
            break;
        case ID_TRAME_JOYSTICK_TRIM_ROLL:
            extract_buf((uint8*)(&value),sizeof(signed char),buf,&pt_rd);
            if(value==0) {
                joy_trim.trim_roll=0;
            }
            else {
                // prevent trim value from looping
                if(joy_trim.trim_roll<128 && value==1) joy_trim.trim_roll++;
                if(joy_trim.trim_roll>-127 && value==-1) joy_trim.trim_roll--;
            }
            break;
        case ID_TRAME_JOYSTICK_TRIM_PITCH:
            extract_buf((uint8*)(&value),sizeof(signed char),buf,&pt_rd);
            if(value==0) {
                joy_trim.trim_pitch=0;
            }
            else {
                // prevent trim value from looping
                if(joy_trim.trim_pitch<128 && value==1) joy_trim.trim_pitch++;
                if(joy_trim.trim_pitch>-127 && value==-1) joy_trim.trim_pitch--;
            }
            break;
        case ID_TRAME_JOYSTICK_TRIM_GAZ:
            extract_buf((uint8*)(&value),sizeof(signed char),buf,&pt_rd);
            if(value==0) {
                joy_trim.trim_gaz=0;
            }
            else {
                // prevent trim value from looping
                if(joy_trim.trim_gaz<128 && value==1) joy_trim.trim_gaz++;
                if(joy_trim.trim_gaz>-127 && value==-1) joy_trim.trim_gaz--;
            }
            break;
        case ID_TRAME_GAINS:
            extract_buf((uint8*)(&gains),sizeof(gains_all),buf,&pt_rd);
            break;
        case ID_TRAME_DEBUG:
            extract_buf((uint8*)(&result.debug),size-2,buf,&pt_rd);
            break;
    }

    return result;
}

/*
** ===================================================================
**     fonction calculant la taille de la trame log
**		a changer en utilisant le send_trame, et un com virtuel pour
**		avoir la taille
**		donner erreur si taille> max xbee
**		inclure une verif dans la station sol de taille log recu/attendue
**
** ===================================================================
*/

int32 calc_long_log(void)
{
	int32 size=sizeof(uint16);//temps

	size+=sizeof(control);//joy
	size+=sizeof(imu_datas);//gyros+angles
	size+=sizeof(angles);//angles consignes
	size+=sizeof(moteurs_8bits);//pwm
    size+=sizeof(altitude);//altitude
    size+=sizeof(altitude);//altitude consigne
	size+=sizeof(optical_flow);//of

	return size;

}



void extract_buf(uint8* dest,int32 size,uint8* src,int32* pt_rd)
{
    if(size>0)
    {
        memcpy(dest,src+(*pt_rd),size);
        (*pt_rd)+=size;
    }
    else
        printf("prob extract_buf, taille negative\n");
}
