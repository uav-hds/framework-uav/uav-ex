//  created:    2011/05/01
//  filename:   x4.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe permettant de definir un environnement de base pour un x4
//
//
/*********************************************************************/

#include "x4.h"
#include <stdio.h>
#include <PushButton.h>
#include <Pid.h>
#include <SimuAhrs.h>
#include <SimuBldc.h>
#include <FrameworkManager.h>
#include <GridLayout.h>
#include <DoubleSpinBox.h>
#include <TabWidget.h>
#include <Tab.h>
#include <Euler.h>
#include <Quaternion.h>
#include <Vector3D.h>
#include <AhrsData.h>
#include "X2Multiplex.h"

#define TAU 5
#define PI ((float)3.14159265358979323846)

using namespace std;
using namespace framework::core;
using namespace framework::gui;
using namespace framework::actuator;
using namespace framework::sensor;
using namespace framework::filter;

x4::x4(FrameworkManager* parent,string name): Thread(parent,name,50)
{
    //creation remote ui qui servira pour les differents boutons et le log
    Tab *uav_tab=new Tab(parent->GetTabWidget(),ObjectName());

        button_start_log=new PushButton(uav_tab->NewRow(),"start_log");
        button_stop_log=new PushButton(uav_tab->LastRowLastCol(),"stop_log");

    ahrs=new SimuAhrs(parent,"ahrs",0,60);
    ahrs->UseDefaultPlot();
    parent->AddDeviceToLog(ahrs);

    Tab *law_tab=new Tab(parent->GetTabWidget(),"loi de commande");
    TabWidget *tab_widget;
    tab_widget=new TabWidget(law_tab->NewRow(),"loi");
    Tab* reglages_tab;
    reglages_tab=new Tab(tab_widget,"Reglages");
    roll_pid=new Pid(reglages_tab->NewRow(),"uroll");
    consigne=new DoubleSpinBox(reglages_tab->LastRowLastCol(),"consigne "," deg",-90,90,1,1);

    Tab* graphes_tab;
    graphes_tab=new Tab(tab_widget,"Graphes");
    roll_pid->UseDefaultPlot(graphes_tab->NewRow());
    parent->AddDeviceToLog(roll_pid);

    multiplex=new X2Multiplex(parent,"multiplex");
    motors=new SimuBldc(multiplex,multiplex->GetLayout(),"moteurs",2,0);
    multiplex->UseDefaultPlot();
}

x4::~x4()
{
    //on ne fait rien ici
    //il vaut mieux faire la destruction a la fin du thread RT pour rester dans le contexte RT
    //(ce destructeur peut etre apellé depuis du non RT)
}


void x4::Run()
{
    WarnUponSwitches(true);

    ahrs->Start();
    motors->SetEnabled(true);

    //periode a passer an argument (reglable)
    SetPeriodMS(TAU);

    while(1)
    {
        WaitPeriod();

        //boutons
        if(button_start_log->Clicked()==true) getFrameworkManager()->StartLog();
        if(button_stop_log->Clicked()==true) getFrameworkManager()->StopLog();

        Quaternion currentQuaternion;
        Vector3D currentAngularSpeed;

        ahrs->GetDatas()->GetQuaternionAndAngularRates(currentQuaternion,currentAngularSpeed);
        Euler currentEuler=currentQuaternion.ToEuler();

        roll_pid->SetValues(currentEuler.roll-consigne->Value()*PI/180.,currentAngularSpeed.x);
        roll_pid->Update(GetTime());

        multiplex->SetRoll(-roll_pid->Output());
        multiplex->Update(GetTime());

    }
    WarnUponSwitches(false);
}
