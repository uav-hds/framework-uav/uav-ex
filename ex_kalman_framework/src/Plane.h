//  created:    2013/06/27
//  filename:   Plane.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant calculant l'etat de l'avion par un kalman
//
//
/*********************************************************************/

#ifndef PLANE_H
#define PLANE_H

//on met les include nécessaires, ceux dont dérive la classe Sinus
#include <IODevice.h>
#include <cv.h>
#include <kalman/ekfilter.hpp>

//les autres classe peuvent simplement être déclarées (forward declaration)
namespace framework
{
    namespace core
    {
        class FrameworkManager;
        class cvmatrix;
    }
    namespace gui
    {
        class Tab;
        class TabWidget;
    }
}

namespace framework
{
namespace actuator
{
    class Plane : public core::IODevice,public Kalman::EKFilter<double,1,false,true,true>
    {
        typedef Kalman::KVector<double, 1, true> Vector;  //!< Vector type.
        typedef Kalman::KMatrix<double, 1, true> Matrix;  //!< Matrix type.

        public://fonctions publiques visibles par tous
            /*!
            * \brief Constructeur
            *
            * Construit un filtre moyenneur
            *
            * \param parent le IODevice à utiliser
            * \param layout où placer les réglages
            * \param name nom de l'objet
            */
            Plane(const core::FrameworkManager* parent,std::string name);

            /*!
            * \brief Déstructeur
            */
            ~Plane();

             /*!
            * \brief Utilise le graphe par défaut
            *
            * cette fonction placera un graphe du signal dans un onglet Graphe
            *
            */
            void UseDefaultPlot(void);

            /*!
            * \brief Matrice de sortie
            *
            * permet d'accéder à la matrice de sortie;
            * par exemple pour récupérer la valeur du signal (voir aussi la fonction Value),
            * ou pour afficher un graphe (voir aussi la fonction UseDefaultPlot)
            *
            * \return un pointeur vers la matrice de sortie
            */
            core::cvmatrix *Output(void) const;

            /*!
            * \brief Update
            *
            * Mise à jour du filtre de kalman
            *
            * \param theta
            * \param r
            * \param f
            */
            void Update(float theta,float r,float f);

        private://fonctions et variables privées, utilisables uniquement par cette classe
            //fonction UpdateFrom de la classe IODevice
            //elle n'est pas implémentée ici car on ne fait pas de mise à jour auto
            void UpdateFrom(const core::io_data *data){};
            gui::Tab* main_tab;//onglet principal du plane
            gui::TabWidget* tabwidget;//conteneur d'onglets secondaires
            core::cvmatrix *output;//matrice de sortie

        protected:
            //fonctions copiés de l'exemple de la librairie kalman
            void makeBaseA();
            void makeBaseH();
            void makeBaseV();
            void makeBaseR();
            void makeBaseW();
            void makeBaseQ();

            void makeA();
            void makeH();
            void makeProcess();
            void makeMeasure();

            double Period, Mass, Bfriction, Portance, Gravity;
    };
}//end namespace actuator
}//end namespace framework
#endif // PLANE_H
