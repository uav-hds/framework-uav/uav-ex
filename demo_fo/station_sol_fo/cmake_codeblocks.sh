DIR=build_$(uname -m)

#verifie l'existence du lien symbolique build
if [ -d build ];then
	if ! readlink build > /dev/null ; then
		#c'est un répertoire, on quitte pour ne rien effacer
		echo "Erreur: build existe et est un répertoire; c'est sensé être un lien symbolique."
		exit 1
	fi
	if ! readlink build | grep -w $DIR >/dev/null; then
		echo "Attention, build pointait vers un autre répertoire."
	fi
	rm build
fi

#creation du repertoire
mkdir -p $DIR
#creation du lien symbolique
ln -s $DIR build

#creation project x86
cd build

rm -f CMakeCache.txt
rm -rf CMakeFiles
cmake ../ -G "CodeBlocks - Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${OECORE_CMAKE_HOST_TOOLCHAIN}
