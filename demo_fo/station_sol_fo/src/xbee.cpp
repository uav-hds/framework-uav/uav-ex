#include "xbee.h"

#include <stdlib.h>
#include <stdio.h>

//recopie de trames.h, a revoir!!
#define ID_TRAME_JOYSTICK 3

//constantes XBee
#define START_DELIMITER 0x7e
#define TX_REQUEST 0x01
#define TX_STATUS 0x89
#define RX_PACKET 0x81
#define DESTINATION_MSB 0x00
#define DESTINATION_LSB 0x02
#define OPTION 0x00
#define XBEE_MAX_SIZE 99
#define LAST_TRAME 0x80

using namespace std;

xbee::xbee(QString Port)
{
    tmp_wr=0;
    tmp_rd=0;
    tmp_message=(char*)malloc(65535);
    offset_tmp_message=0;

    //QextSerialPort
    port = new QextSerialPort(Port, QextSerialPort::EventDriven);
    //port->setBaudRate(BAUD115200);
    port->setBaudRate(BAUD38400);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);
    port->setDataBits(DATA_8);
    port->setStopBits(STOP_1);
    port->open(QIODevice::ReadWrite);

    connect(port, SIGNAL(readyRead()), this, SLOT(receive()));
}

xbee::~xbee()
{
    port->close();
}


void xbee::receive()
{
    unsigned char data[256];
    uint8 recv_size;
    qint64 bytesRead = port->read((char*)data, 256);
//printf("receive %i\n",bytesRead);
    //ajout au buf de recep
    for(qint64 i=0;i<bytesRead;i++)
    {
  //      printf("%x ",data[i]);
        tmp_buf[tmp_wr]=data[i];
        tmp_wr++;
    }
//printf("\n");

    //cherche entete api
    while(tmp_buf[tmp_rd]!=START_DELIMITER && (uint8)(tmp_rd+1)!=tmp_wr)
    {
        tmp_rd++;
    }
//printf("%i %i %i\n",tmp_rd,tmp_wr,rx_size());
    //verifie que la taille du message tient dans les données presentes dans le buffer
    if(rx_size()>=3 )
    {
        //recev_size contient le nb de donnees a traiter pour le checksum
        recv_size=(uint8)(1+tmp_buf[(uint8)(tmp_rd+2)]);

        if(rx_size()>=recv_size+3)
        {
            //se place au debut des donnees
            tmp_rd+=3;

            //calcul du checksum
            uint8 checksum=0;
            for(uint8 i=0;i<recv_size;i++) checksum=(uint8)((checksum+tmp_buf[(uint8)(tmp_rd+i)])& 0xff);

            if(checksum==255)
            {
                uint8 api_id=tmp_buf[tmp_rd];
                if(api_id==TX_STATUS)
                {

                    //se place au debut des donnees
                    tmp_rd++;
                    printf("status %i %i\n",tmp_buf[tmp_rd],tmp_buf[(uint8)(tmp_rd+1)]);
                    if(tmp_buf[tmp_rd]!=send_bufs.first()[4]) printf("erreur trame inattendue %x %x\n",tmp_buf[tmp_rd],send_bufs.first()[4]);
                    tmp_rd++;
                    //si nack, on renvoie
                    if(tmp_buf[tmp_rd]!=0)
                    {

                       // printf("renv\n");
                    }
                    else//si non on enleve les donnees
                    {
                        free(send_bufs.first());
                        send_sizes.removeFirst();
                        send_bufs.removeFirst();
                    }

                    if(send_bufs.size()!=0) writeData((char*)send_bufs.first(),send_sizes.first());
                }
                //rx packet
                if(api_id==RX_PACKET)
                {
                    tmp_rd+=5;

                    for(int i=0;i<recv_size-7;i++)
                    {

                        tmp_message[1+i+offset_tmp_message]= tmp_buf[(uint8)(tmp_rd+1+i)];
                        //printf("%i %i %i %i\n",tmp_message[1+i+offset_tmp_message], tmp_buf[(uint8)(tmp_rd+1+i)],i,(uint8)(tmp_rd+1+i));
                    }
                    offset_tmp_message+=recv_size-7;
                    if((tmp_buf[tmp_rd]&LAST_TRAME)==LAST_TRAME)
                    {
                        tmp_message[0]=tmp_buf[tmp_rd]&(~LAST_TRAME);

                        unsigned char* buf=(unsigned char*)malloc(offset_tmp_message+2);//id et checksum
                        memcpy(buf,tmp_message,offset_tmp_message+1);
                        buf[offset_tmp_message+1]=calc_checksum(buf,offset_tmp_message+1);
                        recv_bufs.append(buf);
                        recv_sizes.append(offset_tmp_message+2);
/*
                        printf("lu xbee->buf %i\n",offset_tmp_message+2);
                        for(size_t i=0;i<offset_tmp_message+2;i++)
                        {
                            printf("%x ",buf[i]);
                        }
                        printf("\n");
*/
                        emit data_ready();
                        offset_tmp_message=0;
                    }
                    tmp_rd+=recv_size-6;
                }
            }
            else
            {
                printf("erreur trame checksum %i %i %i %i\n",tmp_rd,tmp_wr,rx_size(),recv_size);
                uint8 checksum=0;
                for(uint8 i=0;i<recv_size;i++)
                {
                    checksum=(uint8)((checksum+tmp_buf[(uint8)(tmp_rd+i)])& 0xff);
                    printf("%i %x %x\n",(uint8)(tmp_rd+i),tmp_buf[(uint8)(tmp_rd+i)],checksum);
                }
            }
            tmp_rd++;//passe le checksum
            //printf("prochain lu %x %x\n",tmp_buf[tmp_rd],tmp_buf[(uint8)(tmp_rd-1)]);
        }
    }
    //if(rx_size()>=3 ) receive();
}

qint64 xbee::writeData(const char * data,qint64 maxSize)
{/*
    printf("send:\n");
    for(int i=0;i<maxSize;i++)
        {
            printf("%x ",(unsigned char)data[i]);
        }
        printf("\n");*/
    port->write(data,maxSize);
}

qint64 xbee::readData(char * data, qint64 maxSize)
{
    qint64 read_size;

    if(recv_sizes.size()==0) return 0;

    read_size=recv_sizes.first();
    if(read_size>maxSize) read_size=maxSize;
    memcpy(data,recv_bufs.first(),read_size);

    free(recv_bufs.first());
    recv_sizes.removeFirst();
    recv_bufs.removeFirst();

    return read_size;

}

void xbee::SendTrame(uint8* data,int32 size,uint8 id_trame)
{
    char* data_ptr=(char*)data;
    ssize_t frame_size;

    if(id_trame>127)
    {
        printf("erreur trame incorrect %i\n",id_trame);
    }

    //while(size!=0)
    do
    {
        unsigned char* buf=(unsigned char*)malloc(255);

        if(size>XBEE_MAX_SIZE)
        {
            frame_size=XBEE_MAX_SIZE;
        }
        else
        {
            frame_size=size;
        }
        size-=frame_size;

        buf[0]=START_DELIMITER;
        buf[1]=0;//taille MSB, 0 à priori
        buf[2]=frame_size+10-4;//taille LSB
        //le checksum commence a partir de la
        buf[3]=TX_REQUEST;
        buf[4]=id_trame;
        if(id_trame==ID_TRAME_JOYSTICK ) buf[4]=0;
        buf[5]=DESTINATION_MSB;
        buf[6]=DESTINATION_LSB;
        buf[7]=OPTION;
        buf[8]=id_trame;
        if(size==0) buf[8]|=LAST_TRAME;

        memcpy(&buf[9],data_ptr,frame_size);
        data_ptr+=frame_size;

        buf[9+frame_size]=calc_checksum(&buf[3],frame_size+6);

        if(send_bufs.size()==0)
        {
            writeData((char*)buf,frame_size+10);
        }/*
printf("ecrit\n");
        for(int i=0;i<frame_size+10;i++)
        {
            printf("%x ",buf[i]);
        }
        printf("\n");*/

        if(id_trame!=0 && id_trame!=ID_TRAME_JOYSTICK)//hack, a revoir ave cun flag ack
        {
            send_bufs.append((uint8*)buf);
            send_sizes.append(frame_size+10);
        }
    } while(size!=0);
}

uint8 xbee::calc_checksum(uint8* buf,int32 size)
{
    uint8 checksum=0;
    for(int32 i=0;i<size;i++) checksum+=buf[i];

    return (uint8)(255-checksum);
}

uint8 xbee::rx_size()
{
    uint8 size;
    size=(uint8)(tmp_wr-tmp_rd);

    return size;
}
