//  created:    2013/06/27
//  filename:   Moyenneur.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant le calcul d'une moyenne
//
//
/*********************************************************************/

#ifndef MOYENNEUR_H
#define MOYENNEUR_H

//on fixe le nombre maximum d'elements pour la moyenne
//c'est un exemple simple, pour faire bien cela devrait être dynamique
#define MAX_NB 200

//on met les include nécessaires, ceux dont dérive la classe Sinus
#include <IODevice.h>
#include <cv.h>

//les autres classe peuvent simplement être déclarées (forward declaration)
namespace framework
{
    namespace core
    {
        class cvmatrix;
    }
    namespace gui
    {
        class LayoutPosition;
        class GroupBox;
        class SpinBox;
    }
}

//Moyenneur est une classe calculant une moyenne
// sur un nombre paramétrable d'éléments
// on étend donc le namespace filter
// elle dérive de
// IODevice: car elle possède une entrée et une sortie
namespace framework
{
namespace filter
{
    class Moyenneur : public core::IODevice
    {

        public://fonctions publiques visibles par tous
            /*!
            * \brief Constructeur
            *
            * Construit un filtre moyenneur. \n
            * After calling this function, position will be deleted as it is no longer usefull. \n
            *
            * \param parent le IODevice à utiliser
            * \param position où placer les réglages
            * \param name nom de l'objet
            */
            Moyenneur(const core::IODevice* parent,const gui::LayoutPosition* position,std::string name);

            /*!
            * \brief Déstructeur
            */
            ~Moyenneur();

            /*!
            * \brief Matrice de sortie
            *
            * permet d'accéder à la matrice de sortie;
            * par exemple pour récupérer la valeur du signal (voir aussi la fonction Value),
            * ou pour afficher un graphe
            *
            * \return un pointeur vers la matrice de sortie
            */
            core::cvmatrix *Matrix(void) const;

            /*!
            * \brief Valeur
            *
            * cette fonction est équivalente à Matrix()->Value(0,0)
            *
            * \return valeur actuelle de la moyenne
            */
            float Value(void) const;

        private://fonctions et variables privées, utilisables uniquement par cette classe
            //fonction UpdateFrom de la classe IODevice
            void UpdateFrom(const core::io_data *data);
            gui::GroupBox *groupbox;//groupe de boites pour les réglages
            gui::SpinBox *nb;//nombre d'éléments pour la moyenne
            core::cvmatrix *output;//matrice de sortie
            float previous_values[MAX_NB];//stockage des valeurs précédentes, taille prédefinie de MAX_NB

    };
}// end namespace filter
}// end namespace framework
#endif // MOYENNEUR_H
