#ifndef GRAPHE_H_INCLUDED
#define GRAPHE_H_INCLUDED

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include "scrollbar.h"
//#include <stdint.h>

#define PLOT_SIZE  18000

class DataPlot : public QwtPlot
{
    Q_OBJECT

    public:
        DataPlot(QString title,float ymin, float ymax,float period);
        ~DataPlot();
        void setYAxisScale(float ymin,float ymax);
        void addCurve(QPen pen,QString legend);
        void Draw(void);
        void SetData(double data,int index);

        virtual bool eventFilter(QObject *, QEvent *);

    protected:
         void mousePressEvent(QMouseEvent *event);

    private:
        void alignScales();
        void showCurve(QwtPlotItem *item, bool on);
        float ymin_orig;
        float ymax_orig;
        ScrollBar* sb;

        QList<QwtPlotCurve*> *datas;
        QList<int> *datas_type;
        double d_x[PLOT_SIZE];
        QList<double*> *d_y;
        int data_index;
        int min_data_index;
        int max_data_index;
        bool scrolling;
        double min_scroll;
        double max_scroll;

        double view_size;//en s
        double temps;//temps ecoulé en s
        double refresh_rate;//en s

    private slots:
        void legendChecked( const QVariant &itemInfo, bool on );
        void scrollBarMoved(Qt::Orientation o, double min, double max);

};


#endif // GRAPHE_H_INCLUDED
