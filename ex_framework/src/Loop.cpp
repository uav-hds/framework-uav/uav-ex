//  created:    2013/06/26
//  filename:   Loop.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//
//
/*********************************************************************/

//on inclut le header de la classe
#include "Loop.h"
//on inclut les headers des classes seulement déclarées dans Loop.h (forward declaration)
#include "Sinus.h"
#include "Moyenneur.h"
#include <LowPassFilter.h>
#include <ButterworthLowPass.h>
#include <FrameworkManager.h>
#include <Tab.h>
#include <GridLayout.h>
#include <SpinBox.h>
#include <PushButton.h>
#include <DataPlot1D.h>
#include <cvmatrix.h>


//on utilise le namespace std, ce qui évite d'écrire par exemple std::string
using namespace std;
using namespace framework::core;
using namespace framework::gui;
using namespace framework::filter;
using namespace framework::sensor;

//constructeur, on hérite de Thread
Loop::Loop(FrameworkManager* parent,string name,int priority): Thread(parent,name,priority)
{
    Tab *main_tab=new Tab(parent->GetTabWidget(),ObjectName());//onglet principal
        button_kill=new PushButton(main_tab->NewRow(),"kill");//bouton
        button_start_log=new PushButton(main_tab->NewRow(),"start_log");//bouton
        button_stop_log=new PushButton(main_tab->LastRowLastCol(),"stop_log");//bouton
        period=new SpinBox(main_tab->NewRow(),"period thread:"," ms",10,1000,1);//période du Thread

    sinus=new Sinus(parent,"sinus");//le sinus
    sinus->UseDefaultPlot();//utilise le graphe par défaut du sinus

    //le filtre passe bas 1er ordre sur le signal brut, son parent est le sinus
    pbas=new LowPassFilter(sinus,sinus->SetupLayout()->NewRow(),"Passe bas 1er ordre");
    sinus->Plot()->AddCurve(pbas->Matrix()->Element(0),DataPlot::Blue);//on ajoute la sortie du passe bas au graphe du sinus déjà existant

    //le filtre passe bas 3ieme ordre sur le signal brut, son parent est le sinus
    pbas_butt=new ButterworthLowPass(sinus,sinus->SetupLayout()->NewRow(),"Passe bas 3ieme ordre",3);
    sinus->Plot()->AddCurve(pbas_butt->Matrix()->Element(0),DataPlot::Yellow);//on ajoute la sortie du passe bas au graphe du sinus déjà existant

    //le filtre moyenneur sur le signal brut, son parent est le sinus
    moyenne=new Moyenneur(sinus,sinus->SetupLayout()->NewRow(),"Moyenne");
    sinus->Plot()->AddCurve(moyenne->Matrix()->Element(0),DataPlot::Green);//on ajoute la sortie du moyenneur au graphe du sinus déjà existant

    //le filtre moyenneur sur le signal filtré, son parent est le passe bas
    moyenne_pbas=new Moyenneur(pbas,sinus->SetupLayout()->NewRow(),"Moyenne pbas");
    sinus->Plot()->AddCurve(moyenne_pbas->Matrix()->Element(0),DataPlot::Black);//on ajoute la sortie du moyenneur au graphe du sinus déjà existant

    //on specifie les objets à enregistrer (logs)
    //comme le passe bas et les moyennes sont déjà associés au sinus, ils seront automatiquement enregistrés aussi
    parent->AddDeviceToLog(sinus);
}

Loop::~Loop()
{

}

//la boucle du Thread
void Loop::Run(void)
{
    //permet d'avertir si l'on fait des changements du mode temps réél au mode normal
    WarnUponSwitches(true);

    //on démarre le Thread du sinus
    sinus->Start();

    //on fixe la période du Thread
    SetPeriodMS(period->Value());

    while(1)
    {
        //on synchronise avec la période
        WaitPeriod();

        //si la valeur de la box a changé, on fixe la période du Thread
        if(period->ValueChanged()) SetPeriodMS(period->Value());

        //boutons
        if(button_kill->Clicked()) break;//on quitte la boucle

        if(button_start_log->Clicked()) getFrameworkManager()->StartLog();//enregistrement des données
        if(button_stop_log->Clicked()) getFrameworkManager()->StopLog();//fin d'enregistrement

        //il n'y a rien d'autre à faire
        //ce n'est qu'un exemple...
        //normalement on utiliserait les résultats des filtres pour une loi de commande par exemple
    }

    WarnUponSwitches(false);
}
