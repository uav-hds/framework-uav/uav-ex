#include "udt_socket.h"

//#include <stdio.h>
#include <stdlib.h>
#include <QThread>

using namespace std;

udt_socket::udt_socket(QString add,int port,qint64 max_size)
{
    // use this function to initialize the UDT library
    UDT::startup();

    address=add;
    Port=port;

    is_connected=false;

    recv_buf=(char**)malloc(256*sizeof(char*));
    for(int i=0;i<256;i++) recv_buf[i]=NULL;
    pt_rd=0;
    pt_wr=0;
    buf_max_size=max_size;

    Thread = new QThread(this);
    connect(Thread, SIGNAL(started()), this, SLOT(run()));
    moveToThread(Thread);
    Thread->start();
}

udt_socket::~udt_socket()
{
    is_running=false;
    if(Thread->wait(5000)==false)
    {
        Thread->terminate();
        printf("sock terminate\n");
    }

    for(int i=0;i<256;i++)
        if(recv_buf[i]!=NULL) free(recv_buf[i]);

    free(recv_buf);

    UDT::close(fhandle);

    // use this function to release the UDT library
    UDT::cleanup();
}

void udt_socket::try_connect()
{
    while(is_running==true)
    {
        fhandle = UDT::socket(AF_INET, SOCK_DGRAM, 0);
        bool block = true;
        UDT::setsockopt(fhandle, 0, UDT_SNDSYN, &block, sizeof(bool));
        block = true;
        UDT::setsockopt(fhandle, 0, UDT_RCVSYN, &block, sizeof(bool));
        //configure timeout of blocking receive
        int timeout=100;//ms
        UDT::setsockopt(fhandle, 0, UDT_RCVTIMEO, &timeout, sizeof(int));

        //int buf_len=1024*100;
        //UDT::setsockopt(fhandle, 0, UDT_SNDBUF, &buf_len, sizeof(int));
        //UDT::setsockopt(fhandle, 0, UDT_RCVBUF, &buf_len, sizeof(int));

        sockaddr_in serv_addr;
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(short(Port));
        #ifndef WIN32
        //if (inet_pton(AF_INET, "192.168.6.1", &serv_addr.sin_addr) <= 0)
        if (inet_pton(AF_INET, address.toStdString().c_str(), &serv_addr.sin_addr) <= 0)
        #else
        if (INADDR_NONE == (serv_addr.sin_addr.s_addr = inet_addr("192.168.6.1")))
        #endif
        {
            emit_log("incorrect network address.");
        }
        memset(&(serv_addr.sin_zero), '\0', 8);

        if (UDT::ERROR == UDT::connect(fhandle, (sockaddr*)&serv_addr, sizeof(serv_addr)))
        {
            emit_log(QString( "<strong>udt socket: %1<strong>").arg(UDT::getlasterror().getErrorMessage()));
            is_connected=false;
        }
        else
        {
            emit_log(QString( "<strong>udt socket %1 connected<strong>").arg(Port));
            is_connected=true;
            break;
        }
        UDT::close(fhandle);
    }

}

void udt_socket::run()
{
    is_running=true;
    try_connect();

    while(is_running==true)
    {
        //conection socket
        recv_buf[pt_wr]= (char*)malloc(buf_max_size*sizeof(char));
        bytesRead[pt_wr] = UDT::recvmsg(fhandle,recv_buf[pt_wr], buf_max_size);
        if(bytesRead[pt_wr]==buf_max_size) emit_log("<strong>udt socket message is equal to max size<strong>");

        if(bytesRead[pt_wr]>0)
        {
            pt_wr++;
            if(pt_wr==pt_rd)
            {
                emit_log("<strong>udt socket buffer full<strong>");
                pt_rd++;
                free(recv_buf[pt_wr]);
                recv_buf[pt_wr]=NULL;
            }
            emit data_ready();
        }
        else if(bytesRead[pt_wr]==-1)
        {
            //emit_log(QString( "<strong>udt socket: %1<strong>").arg(UDT::getlasterror().getErrorMessage()));
            free(recv_buf[pt_wr]);
            recv_buf[pt_wr]=NULL;
            if(UDT::getlasterror().getErrorCode()==2001)
            {
               emit_log("<strong>udt socket disconnected<strong>");
               try_connect();
            }
        }
        else if(bytesRead[pt_wr]==0)//timeout
        {
            free(recv_buf[pt_wr]);
            recv_buf[pt_wr]=NULL;
        }
    }
    Thread->exit();
}

qint64 udt_socket::writeData(const char * data,qint64 maxSize)
{
    if(is_connected==true)
    {
        int32 ssize=UDT::sendmsg(fhandle,data,maxSize, -1, true);
        if(ssize!=maxSize)
        {
            emit_log(QString("erreur envoi: %1").arg(UDT::getlasterror().getErrorMessage()));
            if(UDT::getlasterror().getErrorCode()==2001)
            {
               is_connected==false;
               UDT::close(fhandle);
               emit_log("<strong>udt socket disconnected<strong>");
            }
        }
        else
        {
            //for(int i=0;i<maxSize;i++) printf("%x ",data[i]);
            //printf("\n");
        }
    }
}

qint64 udt_socket::readData(char * data, qint64 maxSize)
{
    qint64 read_size;

    if(is_connected==true)
    {
        if(pt_rd==pt_wr)
        {
            return -1;
        }

        if(bytesRead[pt_rd]>maxSize)
            read_size=maxSize;
        else
            read_size=bytesRead[pt_rd];

        memcpy(data,recv_buf[pt_rd],read_size);
        free(recv_buf[pt_rd]);
        recv_buf[pt_rd]=NULL;
        pt_rd++;
        return read_size;
    }
    else
        return -1;
}

void udt_socket::SendTrame(uint8* buf,int32 size,uint8 id_trame)
{
    uint8* buf_tosend=NULL;

    buf_tosend=(uint8*)malloc(size+2);
    if(buf_tosend==NULL)
    {
        emit_log("<strong>SendTrame malloc failed<strong>");
        return;
    }

    buf_tosend[0]=id_trame;
    memcpy(buf_tosend+1,buf,size);
    buf_tosend[size+1]=calc_checksum(buf_tosend,size+1);//checksum

    writeData((char*)buf_tosend,size+2);

    free(buf_tosend);
}
uint8 udt_socket::calc_checksum(uint8* buf,int32 size)
{
    uint8 checksum=0;
    for(int32 i=0;i<size;i++) checksum+=buf[i];

    return (uint8)(255-checksum);
}
