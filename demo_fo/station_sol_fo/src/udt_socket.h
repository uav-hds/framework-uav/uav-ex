#ifndef UDT_SOCKET_H
#define UDT_SOCKET_H

#include <udt.h>
#include <QIODevice>
#include <typedef.h>

#ifndef WIN32
   #include <arpa/inet.h>
#else
   #include <winsock2.h>
   #include <ws2tcpip.h>
#endif

class udt_socket : public QIODevice
{
    Q_OBJECT

    public:
        udt_socket(QString add,int port,qint64 max_size);//max size is the maximum receive buffer size
        ~udt_socket();
        qint64 writeData(const char *data,qint64 maxSize);
        qint64 readData(char *data, qint64 maxSize);
        void SendTrame(uint8* buf,int32 size,uint8 id_trame);

    private:
        void try_connect();
        uint8 calc_checksum(uint8* buf,int32 size);
        UDTSOCKET fhandle;
        bool is_running;
        bool is_connected;
        char** recv_buf;
        qint64 bytesRead[256];
        unsigned char pt_rd;
        unsigned char pt_wr;
        qint64 buf_max_size;
        QString address;
        int Port;
        QThread *Thread;

    signals:
        void data_ready();
        void emit_log(QString);

    public slots:
        void run();
};

#endif // UDT_SOCKET_H
