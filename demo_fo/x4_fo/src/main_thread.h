#include "typedef.h"


/*
** ===================================================================
**     declarations des fonctions
**
** ===================================================================
*/
void main_thread(void * arg);
void control_loop(void);

uint16 sat_pwm(float vel_cons,uint16 min,uint16 max);

uint8 write_gains_memory(void);
uint8 read_gains_memory(void);

void stop_moteurs(void);
void send_logs(void);

