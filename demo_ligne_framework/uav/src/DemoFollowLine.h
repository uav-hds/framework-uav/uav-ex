//  created:    2015/10/19
//  filename:   DemoFollowLine.h
//
//  author:     Gildas Bayard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo follow line
//
//
/*********************************************************************/

#ifndef DEMOFOLLOWLINE_H
#define DEMOFOLLOWLINE_H

#include <DemoOpticalFlow.h>

namespace framework {
    namespace core {
        class AhrsData;
    }
    namespace gui {
        class DoubleSpinBox;
    }
    namespace filter {
        class HoughLines;
    }
}

class DemoFollowLine : public DemoOpticalFlow {

    public:
        DemoFollowLine(framework::meta::Uav* uav);
        ~DemoFollowLine();

    protected:
        framework::gui::DoubleSpinBox *speedStep;
        void SignalEvent(Event_t event);
        void ExtraCheckJoystick(void);
        const framework::core::AhrsData *GetReferenceOrientation(void);
        framework::filter::HoughLines *houghLines;

    private:
        const framework::core::AhrsData *GetOrientation(void) const;
        bool wasLineDetected;
        framework::core::AhrsData *customOrientation;
};

#endif // DEMOFOLLOWLINE_H
