#! /bin/bash

. $IGEP_ROOT/uav_dev/bin/noarch/ubuntu_cgroup_hack.sh

if [ -f /proc/xenomai/version ];then
	EXEC=./simulator_rt
else
	EXEC=./simulator_nrt
fi

$EXEC -a 127.0.0.1 -p 9000 -x simulator.xml
