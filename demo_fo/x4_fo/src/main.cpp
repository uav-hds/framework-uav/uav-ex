#include <typedef.h>
#include "structdef.h"
#include "main_thread.h"
#include <3dmgx3.h>
#include <socket.h>
#include "of_thread.h"
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include "dbt.h"
#include <rtdk.h>
#include <signal.h>
#include <xbee.h>
#include <tclap/CmdLine.h>

#include <native/task.h>
#include <native/timer.h>
#include <native/pipe.h>

using namespace TCLAP;
using namespace std;

RT_PIPE of_pipe;
extern struct gains_all gains;
gx3* imu;
xbee* modem;
udt_socket *sock,*sock_cam;
pthread_t of_thread;
bool continuer;
bool gains_changed;
string i2c_port;
string dsp_file;
string xbee_port;
string machine;

dbt *us_dbtFile,*imu_dbtFile,*states_dbtFile,*of_dbtFile;


void catch_signal(int sig)
{
}

void parseOptions(int argc, char** argv)
{
	try
	{

        CmdLine cmd("Command description message", ' ', "0.1");

        ValueArg<string> i2cArg("i","i2c","port i2c (ultra sons et moteurs)",true,"rti2c3","string");
        cmd.add( i2cArg );

        ValueArg<string> dspArg("d","dsp","executable dsp",true,"./dspcv_dsp.out","string");
        cmd.add( dspArg );

        ValueArg<string> xbeeArg("x","xbee","modem xbee",false,"none","string");
        cmd.add( xbeeArg );

        ValueArg<string> machineArg("m","machine","machine (xair or modulox)",true,"none","string");
        cmd.add( machineArg );

        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        i2c_port = i2cArg.getValue();
        dsp_file = dspArg.getValue();
        xbee_port=xbeeArg.getValue();
        machine=machineArg.getValue();

	} catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
}

int main(int argc, char* argv[])
{
    RT_TASK control_task;
    int status;

	signal(SIGTERM, catch_signal);
	signal(SIGINT, catch_signal);

	// Avoids memory swapping for this program
	mlockall(MCL_CURRENT|MCL_FUTURE);

	parseOptions(argc,argv);

    printf("programme démo x4 flux optique\n");

    if(machine=="xair")
    {
        printf("configuration xair (rotation 180 camera)\n");
    }
    else if(machine=="modulox")
    {
        printf("configuration modulox\n");
    }
    else
    {
        printf("machine inconnue %s\n",machine.c_str());
        exit(0);
    }

	 //initialisation des gains
    if(read_gains_memory()==0)
    {
        printf("fichier de gains vide\n");
        //quelques valeurs par defaut pour faire fonctionner le programme
        gains.log_divider=10;
        gains.fc_alt=10;
        gains.fc_of=10;
    }
    gains_changed=false;

    //creation pipe
    status=rt_pipe_create(&of_pipe, "of_pipe", P_MINOR_AUTO,256);
    if(status!=0) printf("rt_pipe_create failed (%i:%s)\n",status,strerror(status));

    if(xbee_port!="none")
    {
        modem=new xbee(xbee_port);
        sock=NULL;
        sock_cam=NULL;
    }
    else
    {
        sock= new udt_socket("sock",9000);
        sock_cam= new udt_socket("sock_cam",9001);
        modem=NULL;
    }

    imu=new gx3("imu","rtser1");
    imu->start();

    continuer=true;

    //creation et start task controle
	status=rt_task_create(&control_task, "control", 0, 80, T_JOINABLE);
	if(status!=0) printf("erreur creation control task %i\n",status);
	status=rt_task_start(&control_task, &main_thread,  (void*)i2c_port.c_str());
	if(status!=0) printf("erreur start control task %i\n",status);

    // Initialise the rt_print buffer for this task explicitly
    rt_print_init(512, "control");

    //creation et start task vision
    if(pthread_create (&of_thread, NULL, main_of, (void*)dsp_file.c_str()) < 0)
    {
        printf("pthread_create failed\n");
    }

    //attente fin de taches
	status=rt_task_join(&control_task);
	if(status!=0) printf("erreur join control task %i\n",status);
    delete imu;

    if(modem==NULL)
    {
        delete sock;
        delete sock_cam;
    }
    else
    {
        delete modem;
    }

    pthread_join(of_thread, NULL);
}

