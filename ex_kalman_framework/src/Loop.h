//  created:    2013/06/26
//  filename:   Loop.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//
//
/*********************************************************************/

#ifndef LOOP_H
#define LOOP_H

//on met les include nécessaires, ceux dont dérive la classe Loop
#include <Thread.h>
#include <kalman/ekfilter.hpp>

//les autres classe peuvent simplement être déclarées (forward declaration)
namespace framework
{
    namespace core
    {
        class FrameworkManager;
        class Measure;
    }
    namespace gui
    {
        class PushButton;
        class SpinBox;
        class DataPlot2D;
    }
    namespace actuator
    {
        class Plane;
    }
}


//Loop est la classe principale de cet example
//elle permet de créer l'interface et tous les objets
//elle dérive de Thread
class Loop : public framework::core::Thread
{
    typedef Kalman::KVector<double, 1, true> Vector;  //!< Vector type.
    typedef Kalman::KMatrix<double, 1, true> Matrix;  //!< Matrix type.

    public://fonctions publiques visibles par tous
        /*!
        * \brief Constructeur
        *
        * Construit la boucle principale
        *
        * \param parent le FrameworkManager à utiliser
        * \param name nom de l'objet
        * \param priority priorité du Thread, par défaut 51 (1:mini, 99:maxi)
        */
        Loop(framework::core::FrameworkManager* parent,std::string name,int priority=51);

        /*!
        * \brief Déstructeur
        */
        ~Loop();

    private://fonctions et variables privées, utilisables uniquement par cette classe

        //fonction Run de la classe Thread, c'est la boucle de traitement
        void Run(void);

        //variables:
        framework::core::Measure *measure;//le sinus
        framework::actuator::Plane *plane;//la moyenne du signal brut
        framework::gui::PushButton *button_start_log,*button_stop_log,*button_kill;//les boutons
        framework::gui::SpinBox *period;//période du Thread
        framework::gui::DataPlot2D *xy_plot;//graphe
        Kalman::KVector<double, 1, true> *f;
};

#endif // LOOP_H
