#! /bin/bash

. $IGEP_ROOT/uav_dev/bin/noarch/ubuntu_cgroup_hack.sh

if [ -f /proc/xenomai/version ];then
	EXEC=./control_rt
else
	EXEC=./control_nrt
fi

$EXEC -n control -a 127.0.0.1 -p 9000 -l ./ -x setup.xml
