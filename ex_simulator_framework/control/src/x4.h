//  created:    2011/05/01
//  filename:   x4.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe permettant de definir un environnement de base pour un x4
//
//
/*********************************************************************/

#ifndef X4_H
#define X4_H

#include <Thread.h>

namespace framework
{
    namespace core
    {
        class FrameworkManager;
    }
    namespace gui
    {
        class PushButton;
        class DoubleSpinBox;
    }
    namespace filter
    {
        class Pid;
        class SimuAhrs;
        class X2Multiplex;
    }
    namespace actuator
    {
        class SimuBldc;
    }
}

class x4 : public framework::core::Thread
{

    public:
        x4(framework::core::FrameworkManager* parent,std::string name);
        ~x4();

    private:
        void Run(void);

        framework::gui::PushButton *button_start_log,*button_stop_log;
        framework::gui::DoubleSpinBox* consigne;
        framework::filter::Pid *roll_pid;
        framework::filter::SimuAhrs* ahrs;
        framework::actuator::SimuBldc* motors;
        framework::filter::X2Multiplex *multiplex;
};

#endif // X4_H
