//  created:    2010/11/24
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    conversion de fichiers DBT vers matlab
//
//
/*********************************************************************/
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

#include "kernel/io_hdfile.h"
#include "kernel/err.h"
#include "typedef.h"
#include "structdef.h"

using namespace std;


typedef struct tolog{
    float uroll;
    float upitch;
    float uyaw;
    float ugaz;
} tolog;

//////////////////////////////////////////////////////////////////////////
// decode dbt recording, from x4_xenomai
//////////////////////////////////////////////////////////////////////////
void decodedbt(char* filename)
{
    hdfile_t *dbtFile = NULL;
    FILE *rec;
    char mfilename[256];
    bool continuer=true;

    dbtFile=open_hdfile(filename,READ_MODE);
    if (!dbtFile)
    {
        cout << "Error while opening the dbt file\n";
        exit(0);
    }


    sprintf(mfilename,"%s.m",filename);
    rec=fopen(mfilename,"a");

    while (continuer)
    {
        road_time_t time;
        road_timerange_t tr = 0;
        struct imu_datas imu;
        struct altitude us;
        struct optical_flow of;
        struct tolog states;


        switch(dbtFile->h.Type)
        {
            case TRAME_US:
                if(read_hdfile(dbtFile,(void*)&us,&time,&tr)==0)
                    continuer=false;
                else
                    fprintf(rec,"%lld,%i,%f,%f\n",time,tr,us.z,us.vz);
                break;
            case TRAME_IMU:
                if(read_hdfile(dbtFile,(void*)&imu,&time,&tr)==0)
                    continuer=false;
                else
                    fprintf(rec,"%lld,%i,%f,%f,%f,%f,%f,%f\n",time,tr,imu.roll,imu.pitch,imu.yaw,imu.droll,imu.dpitch,imu.dyaw);
                break;
            case TRAME_CAM:
                if(read_hdfile(dbtFile,(void*)&of,&time,&tr)==0)
                    continuer=false;
                else
                    fprintf(rec,"%lld,%i,%f,%f\n",time,tr,of.x,of.y);
                break;
            case TRAME_STATE:
                if(read_hdfile(dbtFile,(void*)&states,&time,&tr)==0)
                    continuer=false;
                else
                    fprintf(rec,"%lld,%i,%f,%f,%f,%f\n",time,tr,states.uroll,states.upitch,states.uyaw,states.ugaz);
                break;
        }



    }

    fclose(rec);
    close_hdfile(dbtFile);
}



//////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv )
{

  if (argc < 2)
  {
    cout << "dbt2matlab Usage:" << endl << "1st parameter: name of the dbt file" << endl;
    exit(0);
  }

  char* dbtFilename = argv[1];

  decodedbt(dbtFilename);

  cout <<"end of conversion from dbt to matlab" << endl;

}
