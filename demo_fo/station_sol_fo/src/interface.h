#ifndef HEADER_INTERFACE
#define HEADER_INTERFACE


#include <QtGui>
#include <QWidget>
#include <QTimer>
#include <qextserialenumerator.h>
#include "ui_interface.h"
#include "typedef.h"
#include "structdef.h"
#include "DataPlot.h"
#include "udt_socket.h"
#include "DS3.h"
#include "address_dialog.h"
#include "xbee.h"

//pour les problemes dus a SDL:
#undef main


class GLWidget;

class Interface : public QWidget, private Ui::Interface
{
    Q_OBJECT

    public:
        Interface(std::string address,QWidget *parent = 0);
        ~Interface();

    private:
        DS3 *joypad;
        udt_socket *socket_fd,*socket_cam;
        address_dialog* remote_ip;

        DataPlot *roll_joy_graphe,*pitch_joy_graphe,*yaw_joy_graphe,*gaz_joy_graphe;
        DataPlot *roll_imu_graphe,*pitch_imu_graphe,*yaw_imu_graphe;
        DataPlot *droll_imu_graphe,*dpitch_imu_graphe,*dyaw_imu_graphe;
        DataPlot *vz_graphe,*z_graphe;
        DataPlot *pwm1_graphe,*pwm2_graphe,*pwm3_graphe,*pwm4_graphe;
        DataPlot *of_x_graphe,*of_y_graphe;
        void RequestGains();
        void spinbox_to_gains();
        void gains_to_spinbox();
        void send_sock(uint8* buf,int32 size,uint8 id_trame);
        uint8 calc_checksum(uint8* buf,int32 size);
        bool is_connected;
        bool x4_flying;
        bool save_picture_flag;
        uint16 total_bytesRead;
        struct control joy_local;
        QVector<QRgb> color_table;
        QList<QextPortInfo> ports;
        xbee* com_xbee;

    private slots:
        /* InsÚrez les prototypes de vos slots personnalisÚs ici */
        void on_ClearLogButton_clicked ();
        void on_KillButton_clicked ();
        void on_main_tab_currentChanged (int index);
        void on_SendButton_clicked();
        void on_StartButton_clicked();
        void on_DecollageButton_clicked();
        void on_StopButton_clicked();
        void on_AtterrissageButton_clicked();
        void on_WriteButton_clicked();
        void on_LoadButton_clicked();
        void on_SaveButton_clicked();
        void on_StartOFButton_clicked();
        void on_StopOFButton_clicked();
        void receive();
        void receive_cam();
        void joyaxisValueChanged(int value0,int value1,int value2,int value3);
        void joybuttonValueChanged(int button, bool value);
        void on_ResetRollButton_clicked();
        void on_ResetPitchButton_clicked();
        void on_ResetGazButton_clicked();
        void print_log(QString log);
        void startup(QString ip);
        void on_connect_xbee_clicked();
        void on_save_picture_clicked();

    signals:
        void kill_joypad();
        void kill_udt_socket();
        void ask_rumble(unsigned char);

    protected:
        virtual void timerEvent(QTimerEvent *e);

};


#endif

