#ifndef XBEE_H
#define XBEE_H

#include <QIODevice>
#include <typedef.h>
#include <qextserialport.h>

class xbee : public QIODevice
{
    Q_OBJECT

    public:
        xbee(QString Port);//max size is the maximum receive buffer size
        ~xbee();
        qint64 writeData(const char *data,qint64 maxSize);
        qint64 readData(char *data, qint64 maxSize);
        void SendTrame(uint8* data,int32 size,uint8 id_trame);

    private:
        unsigned char tmp_buf[256];
        unsigned char tmp_wr,tmp_rd;
        char *tmp_message;
        size_t offset_tmp_message;
        QextSerialPort *port;
        QList<uint8*> send_bufs;
        QList<qint64> send_sizes;
        QList<uint8*> recv_bufs;
        QList<qint64> recv_sizes;

        uint8 calc_checksum(uint8* buf,int32 size);
        uint8 rx_size();

    signals:
        void data_ready();
        void emit_log(QString);

    private slots:
        void receive();
};

#endif // XBEE_H

